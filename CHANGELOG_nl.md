|| [English](CHANGELOG.md) || [Nederlands](CHANGELOG_nl.md) || [français](CHANGELOG_fr.md) ||

# Change Log

## 0.1.1 (2018-02-23)

- Response met wachtwoord voor emailloze klanten
- Telefoonnummer voor klantaccount
- Firma-afbeelding op klantenportaal schalen op kleine schermen
- Email layout en tekst aangepast

## 0.1.0 (2018-02-21)

- Bugfix: Chrome popup verschijnt niet met slechte inloggegevens
- handmatig toegewezen punten kunnen worden opgevraagd
- ticket-gegevens kunnen worden opgevraagd
- puntenhistorie kan worden opgevraagd

## 0.0.1 Eerste werkende versie
