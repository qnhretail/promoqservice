|| [English](CHANGELOG.md) || [Nederlands](CHANGELOG_nl.md) || [français](CHANGELOG_fr.md) ||

# Change Log

## 0.1.0 (2018-02-21)

- Réponse avec mot de passe pour les clients sans email
- Numéro de téléphone du compte client
- Mettre à l'échelle l'image de l'entreprise sur le portail client sur les petits écrans
- Mise en page e-mail et texte ajusté

### Bugfixes
- La fenêtre contextuelle chrome ne s'affichera pas avec des informations d'identification incorrectes

### Fonctionnalité
- les points attribués manuellement peuvent être demandés
- Les détails du ticket peuvent être demandés
- l'historique des points peut être demandé

## 0.0.1 Version de travail initiale