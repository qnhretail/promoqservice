package be.qnh.promoq.security.controllers;

import be.qnh.promoq.security.dataAccessObjects.OauthClientDetailsDaoImpl;
import be.qnh.promoq.security.models.OauthClientDetails;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/admin/oauth/")
@Api(value="oAuth2 management", description = "api for oAuth2 token management")
public class OauthController {

    @Autowired
    OauthClientDetailsDaoImpl OauthClientDetailsDao;

    @RequestMapping(value = "/addClient", method = RequestMethod.POST)
    public String addOauthClient(@RequestBody OauthClientDetails clientDetails) {
        OauthClientDetailsDao.save(clientDetails);
        return "ok";
    }
    //   @RequestMapping(
    //    value = {"/token"},
    //    method = {RequestMethod.POST}
    //)
    //public ResponseEntity<OAuth2AccessToken> postAccessToken(Principal principal, @RequestParam Map<String, String> parameters) throws HttpRequestMethodNotSupportedException {
    //       Principal oAuthPrincipal = new UserPrincipal(parameters.get("client_id"));
    //}
}
