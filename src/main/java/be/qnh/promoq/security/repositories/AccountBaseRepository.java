package be.qnh.promoq.security.repositories;

import be.qnh.promoq.security.models.Account;
import be.qnh.promoq.security.models.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface AccountBaseRepository<T extends Account> extends CrudRepository<T, Long> {
    T findByUsername(String username);
    Iterable<T> findAccountsByRolesContains(Role role);

    T findById(long id);
}
