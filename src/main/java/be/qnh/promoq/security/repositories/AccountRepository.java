package be.qnh.promoq.security.repositories;

import be.qnh.promoq.security.models.Account;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface AccountRepository extends AccountBaseRepository<Account> {
}

