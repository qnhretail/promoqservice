package be.qnh.promoq.security.models;


import be.qnh.promoq.main.models.Client;
import be.qnh.promoq.security.config.CustomPasswordEncoder;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Entity
//@Inheritance(strategy = InheritanceType.JOINED)
//@MappedSuperclass
@Component
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String username;
    private String password;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name="accounts_roles",
    joinColumns = @JoinColumn(
            name= "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "role_id", referencedColumnName = "id"))
    private Collection<Role> roles;
    @OneToMany(mappedBy = "account")
    private Collection<Client> clients;

    private Date created;
    private Date updated;

    public Account(){}
    public Account(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public Account(String username, String password, ArrayList<Role> roles) {
        setUsername(username);
        setPassword(password);
        setRoles(roles);
    }

    @PrePersist
    protected void onCreate() {
        created = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        updated = new Date();
    }

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        if (password != null) {
            CustomPasswordEncoder customPasswordEncoder = new CustomPasswordEncoder();
            this.password = customPasswordEncoder.encode(password);
        } else this.password = null;
    }

    public Collection<Role> getRoles() {
        return roles;
    }

    public void setRoles(Collection<Role> roles) {
        this.roles = roles;
    }

    public Collection<Client> getClients() {
        return clients;
    }

    public void setClients(Collection<Client> clients) {
        this.clients = clients;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }
}
