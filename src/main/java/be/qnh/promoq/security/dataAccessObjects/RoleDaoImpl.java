package be.qnh.promoq.security.dataAccessObjects;

import be.qnh.promoq.security.models.Role;
import be.qnh.promoq.security.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;

@Component
public class RoleDaoImpl implements RoleDao {
    @Autowired
    RoleRepository roleRepository;

    public Role findByName(String name) {
        return roleRepository.findByName(name);
    }

    public ArrayList<Role> findByName(ArrayList<String> names) {
        ArrayList<Role> result = new ArrayList<>();
        names.forEach((String role) -> result.add(findByName(role)));
        return result;
    }

    public long count() {
        return roleRepository.count();
    }

    public Iterable<Role> save(Collection<Role> newRoles) {
        return roleRepository.save(newRoles);
    }
}
