package be.qnh.promoq.security.dataAccessObjects;

import be.qnh.promoq.security.models.OauthClientDetails;
import be.qnh.promoq.security.repositories.OauthClientDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OauthClientDetailsDaoImpl implements OauthClientDetailsDao {
    @Autowired
    OauthClientDetailsRepository ocdRepository;

    @Override
    public OauthClientDetails save(OauthClientDetails clientDetails) {
        return ocdRepository.save(clientDetails);
    }
}
