package be.qnh.promoq.security.dataAccessObjects;

import be.qnh.promoq.security.models.OauthClientDetails;

public interface OauthClientDetailsDao {
    OauthClientDetails save(OauthClientDetails clientDetails);
}
