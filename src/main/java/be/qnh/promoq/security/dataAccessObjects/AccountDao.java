package be.qnh.promoq.security.dataAccessObjects;

import be.qnh.promoq.security.models.Account;

public interface AccountDao<T extends Account> {
    T save(Account account);

    T findByUsername(String username);

    T findById(long id);
}
