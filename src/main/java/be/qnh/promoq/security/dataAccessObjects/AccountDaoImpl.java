package be.qnh.promoq.security.dataAccessObjects;

import be.qnh.promoq.api.dataTransferObjects.ClientAccountDto;
import be.qnh.promoq.api.dataTransferObjects.NewAccountDto;
import be.qnh.promoq.api.dataTransferObjects.NewOwnerDto;
import be.qnh.promoq.api.dataTransferObjects.NewStaffAccountDto;
import be.qnh.promoq.main.Constants;
import be.qnh.promoq.main.dataAccessObjects.FirmDaoImpl;
import be.qnh.promoq.main.models.*;
import be.qnh.promoq.main.repositories.ClientAccountRepository;
import be.qnh.promoq.main.repositories.ClientAccountsFirmsRepository;
import be.qnh.promoq.main.repositories.OwnerAccountRepository;
import be.qnh.promoq.main.repositories.StaffAccountRepository;
import be.qnh.promoq.security.models.Account;
import be.qnh.promoq.security.models.Role;
import be.qnh.promoq.security.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class AccountDaoImpl implements AccountDao {
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private OwnerAccountRepository ownerAccountRepository;
    @Autowired
    private StaffAccountRepository staffAccountRepository;
    @Autowired
    private ClientAccountRepository clientAccountRepository;
    @Autowired
    private RoleDaoImpl roleDao;
    @Autowired
    private ClientAccountsFirmsRepository clientAccountsFirmsRepository;
    @Autowired
    private FirmDaoImpl firmDao;

    @Override
    public Account save(Account account) {
        return accountRepository.save(account);
    }

    @Override
    public Account findById(long id) {
        return accountRepository.findById(id);
    }

    public Iterable<Account> findAll() {
        return accountRepository.findAll();
    }

    // todo: refactor
    public Iterable<Firm> getFirmsForAccount(String name) {
        Account account = accountRepository.findByUsername(name);
        if (account instanceof OwnerAccount) {
            return ((OwnerAccount) account).getFirms();
        }
        return null;
    }

    public Iterable<OwnerAccount> findAllOwnerAccountsByFirm(Firm firm) {
        return ownerAccountRepository.findOwnerAccountsByFirmsContains(firm);
    }

    public Iterable<Account> findAllByRole(String role) {
        return accountRepository.findAccountsByRolesContains(roleDao.findByName(role));
    }

    public Account save(NewAccountDto newAccount) {
        if (accountRepository.findByUsername(newAccount.getUsername()) == null) {
            ArrayList<Role> roles = new ArrayList<>();
            for (String role : newAccount.getRoles()) {
                roles.add(roleDao.findByName(role));
            }
            if (newAccount instanceof NewOwnerDto) {
                ownerAccountRepository.save(new OwnerAccount(
                        newAccount.getUsername(),
                        newAccount.getPassword(),
                        roles,
                        ((NewOwnerDto) newAccount).getMaxFirms(),
                        ((NewOwnerDto) newAccount).getMaxAdmins(),
                        ((NewOwnerDto) newAccount).getMaxAdmins()));
            }
            if (newAccount instanceof NewStaffAccountDto)
                staffAccountRepository.save(new StaffAccount((NewStaffAccountDto) newAccount));
            else { // ClientAccount
                ClientAccountDto clientAccountDto = (ClientAccountDto) newAccount;
                ClientAccount clientAccount = new ClientAccount(clientAccountDto, roles);
                clientAccountRepository.save(clientAccount);
            }
        }
        return accountRepository.findByUsername(newAccount.getUsername());
    }

    public void delete(long id) {
        accountRepository.delete(id);
    }

    public Account findByUsername(String userName) {
        return accountRepository.findByUsername(userName);
    }

    public Iterable<StaffAccount> findStaffAccountsByBranch(Branch branch) {
        return staffAccountRepository.findStaffAccountByBranchesContains(branch);
    }

    public StaffAccount findStaffAccountById(long id) {
        return staffAccountRepository.findStaffAccountById(id);
    }

    public Collection<ClientAccount> findAllClientAccountsByFirm(Firm firm) {
        List<ClientAccount> result = new ArrayList<>();
        clientAccountsFirmsRepository.findAllByFirm(firm).forEach(caf -> result.add(caf.getClientAccount()));
        return result;
    }

    public ClientAccount save(ClientAccountDto clientAccountDto) {
        ClientAccount clientAccount = (ClientAccount) save(
                new ClientAccount(clientAccountDto, new ArrayList<Role>() {{
                    add(roleDao.findByName(Constants.ROLE_CLIENT));
                }}));
        saveClientAccountFirms(clientAccount, clientAccountDto.getFirms());
        return clientAccount;
    }

    private void saveClientAccountFirms(ClientAccount clientAccount, Collection<Long> firmIds) {
        clientAccountsFirmsRepository.save(
                new ArrayList<ClientAccountFirm>() {{
                    Collection<Firm> firms = new ArrayList<Firm>() {{
                        firmIds.forEach(firmId -> add(firmDao.findById(firmId)));
                    }};
                    firms.forEach(firm ->
                            add(new ClientAccountFirm(clientAccount, firm, false, false))
                    );
                }});
    }

    public ClientAccount findClientAccountById(long id) {
        return clientAccountRepository.findById(id);
    }

    public RegisterAccount findRegisterAccountBySerialNr(String registerSerialNr) {
        return (RegisterAccount) accountRepository.findByUsername(registerSerialNr);
    }

    public ClientAccount findByEmail(String email) {
        return clientAccountRepository.findByEmail(email);
    }

    public ClientAccount update(ClientAccountDto clientAccountDto) {
        ClientAccount clientAccount = clientAccountRepository.findById(clientAccountDto.getId());
        if (clientAccountDto.getEmail() != null) clientAccount.setEmail(clientAccountDto.getEmail());
        if (clientAccountDto.getUsername() != null) clientAccount.setUsername(clientAccountDto.getUsername());
        clientAccount.setAddress(clientAccountDto.getAddress());
        clientAccount.setZipcode(clientAccountDto.getZipCode());
        clientAccount.setTown(clientAccountDto.getTown());
        clientAccount.setNote(clientAccountDto.getNote());
        return clientAccountRepository.save(clientAccount);
    }

    public long count() {
        return clientAccountRepository.count();
    }

    public Iterable<Account> save(List<Account> accounts) {
        return accountRepository.save(accounts);
    }
}
