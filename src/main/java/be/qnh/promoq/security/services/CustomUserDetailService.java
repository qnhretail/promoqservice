package be.qnh.promoq.security.services;

import be.qnh.promoq.security.config.CustomPasswordEncoder;
import be.qnh.promoq.security.dataAccessObjects.AccountDaoImpl;
import be.qnh.promoq.security.models.Account;
import be.qnh.promoq.security.models.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class CustomUserDetailService implements UserDetailsService {
    @Autowired
    AccountDaoImpl accountDao;
    @Autowired
    CustomPasswordEncoder passwordEncoder;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Account account = accountDao.findByUsername(username);
        if(account != null) {
            return new User(account.getUsername(), account.getPassword(), true, true,
                    true, true,
                    getGrantedAuthorities(account));
        } else {
            System.out.println("Acount not found");
            throw new UsernameNotFoundException("could not find the user '" + username + "'");
        }
    }

    private Collection<GrantedAuthority> getGrantedAuthorities(Account account) {
        Collection<GrantedAuthority> ga = new ArrayList<GrantedAuthority>();
        if (account.getRoles().isEmpty()) ga.add(new SimpleGrantedAuthority("ROLE_NONE"));
        else for (Role role : account.getRoles()) {
                ga.add(new SimpleGrantedAuthority(role.getName()));
            }
        return ga;
    }
}
