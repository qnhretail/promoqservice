package be.qnh.promoq.mail;

import be.qnh.promoq.main.dataAccessObjects.SmtpServerConfigurationDao;
import be.qnh.promoq.main.models.ClientAccount;
import be.qnh.promoq.main.models.OwnerAccount;
import be.qnh.promoq.main.models.SmtpServerConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Service
public class EmailService {

    @Autowired
    private JavaMailSender emailSender;

    @Autowired
    private SpringTemplateEngine templateEngine;

    @Autowired
    private SmtpServerConfigurationDao smtpServerConfigurationDao;

    @Autowired
    private MessageSource messageSource;


    public void sendSimpleMessage(Mail mail, String template) throws MessagingException, IOException {
        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message,
                MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                StandardCharsets.UTF_8.name());

        Context context = new Context();
        context.setVariables(mail.getModel());
        String html = templateEngine.process(template, context);

        helper.setTo(mail.getTo());
        helper.setText(html, true);
        helper.setSubject(mail.getSubject());
        helper.setFrom(mail.getFrom());

        emailSender.send(message);
    }

    public void sendNewAccount(OwnerAccount sender, ClientAccount receiver, String password, Locale locale) throws IOException, MessagingException {
        SmtpServerConfiguration smtpServerConfiguration = smtpServerConfigurationDao.findByOwnerAccount(sender);
        String subject = messageSource.getMessage("welkom.subject",
                new Object[]{}, locale);
        String salutation = messageSource.getMessage("email.salutation",
                new Object[]{receiver.getName()}, locale);
        String content = messageSource.getMessage("welkom.content",
                new Object[]{receiver.getUsername(), password}, locale);

        Mail mail = new Mail();
        mail.setFrom("test@mail.be");
        mail.setTo(receiver.getEmail());
        mail.setSubject(subject);
        Map model = new HashMap();
        model.put("salutation", salutation);
        model.put("content", content);
        mail.setModel(model);
        sendSimpleMessage(mail, "new-account");
    }

    public void sendNewPassword(OwnerAccount sender, ClientAccount receiver, String password, Locale locale)
            throws IOException, MessagingException {

        SmtpServerConfiguration smtpServerConfiguration =
                smtpServerConfigurationDao.findByOwnerAccount(sender);

        String subject = messageSource.getMessage("newPassword.subject",
                new Object[]{}, locale);
        String salutation = messageSource.getMessage("email.salutation",
                new Object[]{receiver.getName()}, locale);
        String content = messageSource.getMessage("newPassword.content",
                new Object[]{receiver.getUsername(), password}, locale);

        Mail mail = new Mail();
        mail.setFrom("PromoQ");
        mail.setTo(receiver.getEmail());
        mail.setSubject(subject);
        Map model = new HashMap();
        model.put("salutation", salutation);
        model.put("content", content);
        mail.setModel(model);
        sendSimpleMessage(mail, "reset-password");
    }

}
