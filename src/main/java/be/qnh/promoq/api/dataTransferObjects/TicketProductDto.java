package be.qnh.promoq.api.dataTransferObjects;

import be.qnh.promoq.main.models.TicketProduct;

public class TicketProductDto {
    private long id;
    private String plu;
    private String description;
    private long articleGroup;
    private float price;
    private float priceInclDiscount;

    public TicketProductDto() {
    }

    public TicketProductDto(long id, String plu, String description, long articleGroup, float price, float priceInclDiscount) {
        this.id = id;
        this.plu = plu;
        this.description = description;
        this.articleGroup = articleGroup;
        this.price = price;
        this.priceInclDiscount = priceInclDiscount;
    }

    public TicketProductDto(TicketProduct ticketProduct) {
        this.id = id;
        this.plu = ticketProduct.getProduct() != null ? ticketProduct.getProduct().getProductCode() :
                null;
        this.description = ticketProduct.getDescription();
        this.articleGroup = ticketProduct.getArticleGroup();
        this.price = ticketProduct.getPrice();
        this.priceInclDiscount = ticketProduct.getPriceInclDiscount();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPlu() {
        return plu;
    }

    public void setPlu(String plu) {
        this.plu = plu;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getArticleGroup() {
        return articleGroup;
    }

    public void setArticleGroup(long articleGroup) {
        this.articleGroup = articleGroup;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getPriceInclDiscount() {
        return priceInclDiscount;
    }

    public void setPriceInclDiscount(float priceInclDiscount) {
        this.priceInclDiscount = priceInclDiscount;
    }
}
