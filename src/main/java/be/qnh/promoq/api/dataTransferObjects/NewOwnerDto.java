package be.qnh.promoq.api.dataTransferObjects;

import be.qnh.promoq.main.Constants;

import java.util.ArrayList;

public class NewOwnerDto implements NewAccountDto {

    private String username;
    private String password;
    private ArrayList<String> roles = new ArrayList<String>() {{
        add(Constants.ROLE_OWNER);
    }};
    private int maxFirms;
    private int maxAdmins;
    private int maxClients;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getMaxFirms() {
        return maxFirms;
    }

    public void setMaxFirms(int maxFirms) {
        this.maxFirms = maxFirms;
    }

    public int getMaxAdmins() {
        return maxAdmins;
    }

    public void setMaxAdmins(int maxAdmins) {
        this.maxAdmins = maxAdmins;
    }

    public int getMaxClients() {
        return maxClients;
    }

    public void setMaxClients(int maxClients) {
        this.maxClients = maxClients;
    }

    public ArrayList<String> getRoles() {
        return roles;
    }
}
