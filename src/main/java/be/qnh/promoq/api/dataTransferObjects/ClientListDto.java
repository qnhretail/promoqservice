package be.qnh.promoq.api.dataTransferObjects;

import be.qnh.promoq.main.models.Client;

import java.util.ArrayList;
import java.util.Collection;

public class ClientListDto {
    private Collection<ClientDto> clients;

    public ClientListDto(Iterable<Client> clients) {
        this.clients = new ArrayList<>();
        clients.forEach(client -> this.clients.add(new ClientDto(client)));
    }

    public Collection<ClientDto> getClients() {
        return clients;
    }

    public void setClients(Collection<ClientDto> clients) {
        this.clients = clients;
    }
}
