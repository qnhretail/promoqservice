package be.qnh.promoq.api.dataTransferObjects;

public class DecimalAmountDto extends AmountBaseDto {
    private Float amount;

    public DecimalAmountDto(Float amount, String reason) {
        this.amount = amount;
        this.reason = reason;
    }

    public DecimalAmountDto() {
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }
}
