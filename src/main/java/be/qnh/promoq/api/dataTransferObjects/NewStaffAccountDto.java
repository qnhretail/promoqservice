package be.qnh.promoq.api.dataTransferObjects;

import be.qnh.promoq.main.Constants;
import be.qnh.promoq.main.models.StaffAccount;

import java.util.ArrayList;

public class NewStaffAccountDto implements NewAccountDto {
    private long firmId;
    private long id;
    private String username;
    private String name;
    private String surname;
    private String email;
    private ArrayList<String> roles = new ArrayList<String>() {{
        add(Constants.ROLE_STAFF);
    }};
    private String password = "password";

    public NewStaffAccountDto(StaffAccount account) {
        this.id = account.getId();
        this.username = account.getUsername();
        this.name = account.getName();
        this.surname = account.getSurname();
        this.email = account.getEmail();
        this.firmId = account.getFirm().getId();
    }

    public NewStaffAccountDto() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getFirmId() {
        return firmId;
    }

    public void setFirmId(long firmId) {
        this.firmId = firmId;
    }

    @Override
    public ArrayList<String> getRoles() {
        return roles;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
