package be.qnh.promoq.api.dataTransferObjects;

public class ValidateGiftArticleDto {
    private long id;
    private String staffMember;
    private String clientCardNr;
    private long quantity;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getClientCardNr() {
        return clientCardNr;
    }

    public void setClientCardNr(String clientCardNr) {
        this.clientCardNr = clientCardNr;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public String getStaffMember() {
        return staffMember;
    }

    public void setStaffMember(String staffMember) {
        this.staffMember = staffMember;
    }
}
