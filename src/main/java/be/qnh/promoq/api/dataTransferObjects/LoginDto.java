package be.qnh.promoq.api.dataTransferObjects;

import be.qnh.promoq.security.models.Role;

import java.util.Collection;

public class LoginDto {
    private String username;
    private Collection<Role> roles;

    public LoginDto(String username, Collection<Role> roles) {
        this.username = username;
        this.roles = roles;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Collection<Role> getRoles() {
        return roles;
    }
}
