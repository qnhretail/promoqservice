package be.qnh.promoq.api.dataTransferObjects;

import be.qnh.promoq.main.models.TicketPromotion;

import java.util.ArrayList;
import java.util.Date;

public class TicketPromotionDto {
    private long id;
    private long promotionId;
    private long pointsRewarded;
    private long totalPoints;
    private Iterable<GiftArticleDto> giftArticles;
    private Date validUntil;
    private Date validateUntil;

    public TicketPromotionDto(long id, long promotionId, long pointsRewarded, long totalPoints,
                              Iterable<GiftArticleDto> giftArticles, Date validUntil, Date validateUntil) {
        this.id = id;
        this.promotionId = promotionId;
        this.pointsRewarded = pointsRewarded;
        this.totalPoints = totalPoints;
        this.giftArticles = giftArticles;
        this.validUntil = validUntil;
        this.validateUntil = validateUntil;
    }

    public TicketPromotionDto(long id, long l, long pointsRewarded, long points, ArrayList<GiftArticleDto> giftArticleDtos, Date validFrom, Date validUntil, Date validateUntil) {
    }

    public TicketPromotionDto(TicketPromotion ticketPromotion) {
        this.id = ticketPromotion.getId();
        this.promotionId = ticketPromotion.getPromotion().getId();
        this.pointsRewarded = ticketPromotion.getPointsRewarded();
        this.validUntil = ticketPromotion.getPromotion().getValidUntil();
        this.validateUntil = ticketPromotion.getPromotion().getValidateUntil();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(long promotionId) {
        this.promotionId = promotionId;
    }

    public long getPointsRewarded() {
        return pointsRewarded;
    }

    public void setPointsRewarded(int pointsRewarded) {
        this.pointsRewarded = pointsRewarded;
    }

    public long getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(int totalPoints) {
        this.totalPoints = totalPoints;
    }

    public Iterable<GiftArticleDto> getGiftArticles() {
        return giftArticles;
    }

    public void setGiftArticles(Iterable<GiftArticleDto> giftArticles) {
        this.giftArticles = giftArticles;
    }

    public Date getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(Date validUntil) {
        this.validUntil = validUntil;
    }

    public Date getValidateUntil() {
        return validateUntil;
    }

    public void setValidateUntil(Date validateUntil) {
        this.validateUntil = validateUntil;
    }
}
