package be.qnh.promoq.api.dataTransferObjects;

public class AmountDto extends AmountBaseDto {
    private Long amount;

    public AmountDto(long amount, String reason) {
        this.amount = amount;
        this.reason = reason;
    }

    public AmountDto() {
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

}
