package be.qnh.promoq.api.dataTransferObjects.helpers;

import be.qnh.promoq.api.dataTransferObjects.ClientAccountValuePromotionDto;
import be.qnh.promoq.main.dataAccessObjects.ClientAccountPromotionDaoImpl;
import be.qnh.promoq.main.models.ClientAccount;
import be.qnh.promoq.main.models.Ticket;

import java.util.ArrayList;
import java.util.Date;

public class ClientAccountValuePromotionDtoHelper {
    public static Iterable<ClientAccountValuePromotionDto> getClientAccountValuePromotionDtoArrayForTicket(Ticket ticket) {
        return getClientAccountValuePromotionDtoArrayForClientAccount(ticket.getClient().getAccount());
    }

    private static Iterable<ClientAccountValuePromotionDto> getClientAccountValuePromotionDtoArrayForClientAccount(ClientAccount account) {
        ClientAccountPromotionDaoImpl clientAccountPromotionDao = new ClientAccountPromotionDaoImpl();
        return new ArrayList<ClientAccountValuePromotionDto>() {{
            clientAccountPromotionDao.findValuePromotionsByClientAccount(account).forEach(
                    clientAccountPromotion -> {
                        if (clientAccountPromotion.getPromotion().getValidUntil().after(new Date())) {
                            add(new ClientAccountValuePromotionDto(clientAccountPromotion));
                        }
                    }
            );
        }};
    }
}
