package be.qnh.promoq.api.dataTransferObjects;

import be.qnh.promoq.main.models.Product;

public class ProductDto {
    private long id;
    private String productCode;
    private String name;
    private Long articleGroup;
    private Long firmId;

    public ProductDto(Product product) {
        this.id = product.getId();
        this.productCode = product.getProductCode();
        this.name = product.getName();
        this.articleGroup = product.getArticleGroup();
        this.firmId = product.getFirm().getId();
    }

    public ProductDto() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getArticleGroup() {
        return articleGroup;
    }

    public void setArticleGroup(Long articleGroup) {
        this.articleGroup = articleGroup;
    }

    public Long getFirmId() {
        return firmId;
    }

    public void setFirmId(Long firmId) {
        this.firmId = firmId;
    }
}
