package be.qnh.promoq.api.dataTransferObjects;

import be.qnh.promoq.main.models.ClientAccount;

import java.util.ArrayList;
import java.util.Collection;

public class ClientAccountListDto {
    private final Collection<ClientAccountDto> clientAccounts;

    public ClientAccountListDto(Iterable<ClientAccount> clientAccounts) {
        this.clientAccounts = new ArrayList<>();
        for (ClientAccount clientAccount : clientAccounts) {
            this.clientAccounts.add(new ClientAccountDto(clientAccount));
        }
    }

    public Collection<ClientAccountDto> getClientAccounts() {
        return clientAccounts;
    }
}
