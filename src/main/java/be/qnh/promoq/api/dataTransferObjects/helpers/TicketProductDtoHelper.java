package be.qnh.promoq.api.dataTransferObjects.helpers;

import be.qnh.promoq.api.dataTransferObjects.TicketProductDto;
import be.qnh.promoq.main.dataAccessObjects.TicketProductDaoImpl;
import be.qnh.promoq.main.models.Ticket;

import java.util.ArrayList;
import java.util.Collection;

public class TicketProductDtoHelper {
    public static Collection<TicketProductDto> getTicketProductDtoArray(Ticket ticket) {
        TicketProductDaoImpl ticketProductDao = new TicketProductDaoImpl();
        return new ArrayList<TicketProductDto>() {{
            ticketProductDao.findByticket(ticket).forEach(ticketProduct -> add(new TicketProductDto(ticketProduct)));
        }};
    }
}
