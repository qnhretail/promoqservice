package be.qnh.promoq.api.dataTransferObjects;

public class AmountBaseDto {
    protected String reason;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
