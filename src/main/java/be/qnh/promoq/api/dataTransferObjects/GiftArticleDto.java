package be.qnh.promoq.api.dataTransferObjects;

import be.qnh.promoq.main.models.GiftArticle;

import java.util.ArrayList;
import java.util.Collection;

public class GiftArticleDto {
    private long id;
    private long firmId;
    private String name;
    private Collection<Long> productPlus;

    public GiftArticleDto(GiftArticle giftArticle) {
        this.id = giftArticle.getId();
        this.firmId = giftArticle.getFirm().getId();
        this.name = giftArticle.getName();
        this.productPlus = new ArrayList<>();
        giftArticle.getProducts().forEach(p -> this.productPlus.add(p.getId()));
    }

    public GiftArticleDto(long id, long firmId, String name, Collection<Long> productPlus) {
        this.id = id;
        this.firmId = firmId;
        this.name = name;
        this.productPlus = productPlus;
    }

    public GiftArticleDto() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getFirmId() {
        return firmId;
    }

    public void setFirmId(long firmId) {
        this.firmId = firmId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<Long> getProductPlus() {
        return productPlus;
    }

    public void setProductPlus(Collection<Long> productPlus) {
        this.productPlus = productPlus;
    }
}
