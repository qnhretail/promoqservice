package be.qnh.promoq.api.dataTransferObjects;

import be.qnh.promoq.security.models.Account;

import java.util.ArrayList;

public class BaseAccountDto implements AccountDto {
    private String username;
    private ArrayList<String> roles;

    public BaseAccountDto(Account account) {
        this.username = account.getUsername();
        this.roles = new ArrayList<String>() {{
            account.getRoles().forEach(role -> add(role.getName()));
        }};
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public ArrayList<String> getRoles() {
        return this.roles;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setRoles(ArrayList<String> roles) {
        this.roles = roles;
    }
}
