package be.qnh.promoq.api.dataTransferObjects.helpers;

import be.qnh.promoq.api.dataTransferObjects.TicketPromotionDto;
import be.qnh.promoq.main.dataAccessObjects.TicketPromotionDaoImpl;
import be.qnh.promoq.main.models.Ticket;

import java.util.ArrayList;
import java.util.Collection;

public class TicketPromotionDtoHelper {
    public static Collection<TicketPromotionDto> getTicketPromotionDtoArray(Ticket ticket) {
        TicketPromotionDaoImpl ticketPromotionDao = new TicketPromotionDaoImpl();
        return new ArrayList<TicketPromotionDto>() {{
            ticketPromotionDao.findByTicket(ticket).forEach(ticketPromotion ->
                    add(new TicketPromotionDto(ticketPromotion)
                    )
            );
        }};
    }
}
