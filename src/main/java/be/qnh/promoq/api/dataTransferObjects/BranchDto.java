package be.qnh.promoq.api.dataTransferObjects;

import be.qnh.promoq.main.models.Branch;

public class BranchDto {

    private String site;
    private String phone;
    private String email;
    private String address;
    private long id;
    private String name;
    private long firmId;

    public BranchDto(long id, String name, long firmId) {
        this.id = id;
        this.name = name;
        this.firmId = firmId;
    }

    public BranchDto(Branch branch) {
        id = branch.getId();
        name = branch.getName();
        firmId = branch.getFirm().getId();
        address = branch.getAddress();
        email = branch.getEmail();
        phone = branch.getPhone();
        site = branch.getSite();
    }

    public BranchDto() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getFirmId() {
        return firmId;
    }

    public void setFirmId(long firmId) {
        this.firmId = firmId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }
}
