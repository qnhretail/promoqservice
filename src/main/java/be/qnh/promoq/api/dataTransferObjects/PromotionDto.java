package be.qnh.promoq.api.dataTransferObjects;

import be.qnh.promoq.main.models.Promotion;
import be.qnh.promoq.main.models.ValuePromotion;
import be.qnh.promoq.main.models.ValuePromotionDirectDisc;
import be.qnh.promoq.main.models.ValuePromotionWithGiftArticle;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * PromotionTypes:
 * 0: ValuePromotion
 *
 * ValuePromotionTypes:
 * 0: ValuePromotionWithGiftArticle
 * 1: ValuePromotionWithDirectDiscount
 */
public class PromotionDto {
    private long id;
    private String name;
    private String description;
    private Long firmId;
    private Collection<Long> branches;
    private int type;
    private boolean firmPromotion;
    private Long valueOfPoint;
    private Long valueGift;
    private Date validFrom;
    private Date validUntil;
    private Date validateUntil;
    private Long giftArticleId;
    private int valuePromotionType;
    private Float giftAmount;

    public PromotionDto(Promotion p) {
        this.id = p.getId();
        this.name = p.getName();
        this.description = p.getDescription();
        this.firmId = p.getFirm().getId();
        this.branches = new ArrayList<>();
        this.firmPromotion = p.getFirmPromotion();
        if (!this.firmPromotion) {
            p.getBranches().forEach(b -> this.branches.add(b.getId()));
        }
        this.validFrom = p.getValidFrom();
        this.validUntil = p.getValidUntil();
        this.validateUntil = p.getValidateUntil();

        if (p instanceof ValuePromotion) {
            this.type = 0;
            this.valueOfPoint = ((ValuePromotion) p).getValueOfPoint();
            this.valueGift = ((ValuePromotion) p).getValueGift();
            if (p instanceof ValuePromotionWithGiftArticle) {
                this.giftArticleId = ((ValuePromotionWithGiftArticle) p).getGiftArticle().getId();
                this.valuePromotionType = 0;
            }
            if (p instanceof ValuePromotionDirectDisc) {
                this.giftAmount = (((ValuePromotionDirectDisc) p).getGiftAmount());
                this.valuePromotionType = 1;
            }
        }

    }

    public PromotionDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getFirmId() {
        return firmId;
    }

    public void setFirmId(Long firmId) {
        this.firmId = firmId;
    }

    public Collection<Long> getBranches() {
        return branches;
    }

    public void setBranches(Collection<Long> branches) {
        this.branches = branches;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public boolean isFirmPromotion() {
        return firmPromotion;
    }

    public void setFirmPromotion(boolean firmPromotion) {
        this.firmPromotion = firmPromotion;
    }

    public Long getValueOfPoint() {
        return valueOfPoint;
    }

    public void setValueOfPoint(Long valueOfPoint) {
        this.valueOfPoint = valueOfPoint;
    }

    public Long getValueGift() {
        return valueGift;
    }

    public void setValueGift(Long valueGift) {
        this.valueGift = valueGift;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(Date validUntil) {
        this.validUntil = validUntil;
    }

    public Date getValidateUntil() {
        return validateUntil;
    }

    public void setValidateUntil(Date validateUntil) {
        this.validateUntil = validateUntil;
    }

    public Long getGiftArticleId() {
        return giftArticleId;
    }

    public void setGiftArticleId(Long giftArticleId) {
        this.giftArticleId = giftArticleId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Float getGiftAmount() {
        return giftAmount;
    }

    public void setGiftAmount(Float giftAmount) {
        this.giftAmount = giftAmount;
    }

    public int getValuePromotionType() {
        return valuePromotionType;
    }

    public void setValuePromotionType(int valuePromotionType) {
        this.valuePromotionType = valuePromotionType;
    }
}
