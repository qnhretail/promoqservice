package be.qnh.promoq.api.dataTransferObjects;

import be.qnh.promoq.main.models.PointTransfer;

public class PointTransferDto extends TransferDto {
    private long quantity;
    private long promotion;

    public PointTransferDto(PointTransfer pointTransfer) {
        super(
                pointTransfer.getId(),
                pointTransfer.getDateTime(),
                pointTransfer.getReason(),
                pointTransfer.getAssignedBy().getId(),
                pointTransfer.getClientAccount().getId());
        this.quantity = pointTransfer.getQuantity();
        this.promotion = pointTransfer.getPromotion().getId();
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public long getPromotion() {
        return promotion;
    }

    public void setPromotion(long promotion) {
        this.promotion = promotion;
    }
}
