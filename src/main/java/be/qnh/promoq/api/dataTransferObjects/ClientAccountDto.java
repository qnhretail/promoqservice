package be.qnh.promoq.api.dataTransferObjects;

import be.qnh.promoq.main.models.ClientAccount;

import java.util.ArrayList;
import java.util.Collection;

public class ClientAccountDto implements AccountDto {
    private String phone;
    private String username;
    private String name;
    private String password;
    private ArrayList<String> roles;
    private String email;
    private String address;
    private String zipCode;
    private String town;
    private String note;
    private Collection<Long> firms;
    private long id;

    public ClientAccountDto(ClientAccount account) {
        this.username = account.getUsername();
        this.name = account.getName();
        this.roles = new ArrayList<>();
        account.getRoles().forEach(r -> this.roles.add(r.getName()));
        this.email = account.getEmail();
        this.phone = account.getPhone();
        this.address = account.getAddress();
        this.zipCode = account.getZipcode();
        this.town = account.getTown();
        this.note = account.getNote();
        this.id = account.getId();
    }

    public ClientAccountDto() {
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public ArrayList<String> getRoles() {
        return roles;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Collection<Long> getFirms() {
        return firms;
    }

    public void setFirms(Collection<Long> firms) {
        this.firms = firms;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getNote() {
        return this.note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
