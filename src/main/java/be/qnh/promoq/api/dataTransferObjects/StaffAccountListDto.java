package be.qnh.promoq.api.dataTransferObjects;

import be.qnh.promoq.main.models.StaffAccount;

import java.util.ArrayList;
import java.util.Collection;

public class StaffAccountListDto {
    private Collection<StaffAccountDto> staffAccounts;

    public StaffAccountListDto(Iterable<StaffAccount> accounts) {
        this.staffAccounts = new ArrayList<>();
        for (StaffAccount account : accounts) {
            this.staffAccounts.add(new StaffAccountDto(account));
        }
    }

    public Collection<StaffAccountDto> getStaffAccounts() {
        return staffAccounts;
    }

    public void setStaffAccounts(Collection<StaffAccountDto> staffAccounts) {
        this.staffAccounts = staffAccounts;
    }
}
