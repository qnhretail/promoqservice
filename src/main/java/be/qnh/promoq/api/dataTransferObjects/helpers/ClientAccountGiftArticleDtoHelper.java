package be.qnh.promoq.api.dataTransferObjects.helpers;

import be.qnh.promoq.api.dataTransferObjects.ClientAccountGiftArticleDto;
import be.qnh.promoq.main.dataAccessObjects.ClientAccountGiftArticleDaoImpl;
import be.qnh.promoq.main.models.ClientAccount;
import be.qnh.promoq.main.models.Ticket;

import java.util.ArrayList;

public class ClientAccountGiftArticleDtoHelper {
    public static Iterable<ClientAccountGiftArticleDto> getClientAccountGiftArticleDtoArrayForTicket(Ticket ticket) {
        return getClientAccountGiftArticleDtoArrayForClientAccount(ticket.getClient().getAccount());
    }

    public static Iterable<ClientAccountGiftArticleDto> getClientAccountGiftArticleDtoArrayForClientAccount(ClientAccount account) {
        ClientAccountGiftArticleDaoImpl clientAccountGiftArticleDao = new ClientAccountGiftArticleDaoImpl();
        return new ArrayList<ClientAccountGiftArticleDto>() {
            {
                clientAccountGiftArticleDao.findByClientAccount(account).forEach(
                        clientAccountGiftArticle -> add(new ClientAccountGiftArticleDto(clientAccountGiftArticle))
                );
            }
        };
    }
}
