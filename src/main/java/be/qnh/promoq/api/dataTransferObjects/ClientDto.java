package be.qnh.promoq.api.dataTransferObjects;

import be.qnh.promoq.main.models.Client;

import java.util.Date;

public class ClientDto {
    private long id;
    private String name;
    private String surname;
    private Date birthday;
    private String cardId;
    private long accountId;
    private Date created;
    private Date updated;

    public ClientDto(Client client) {
        this.id = client.getId();
        this.name = client.getName();
        this.surname = client.getSurname();
        this.birthday = client.getBirthday();
        this.cardId = client.getCardId();
        this.accountId = client.getAccount().getId();
        this.created = client.getCreated();
        this.updated = client.getUpdated();
    }

    public ClientDto() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }
}
