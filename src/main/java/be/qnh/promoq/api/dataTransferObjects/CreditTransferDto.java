package be.qnh.promoq.api.dataTransferObjects;

import be.qnh.promoq.main.models.CreditTransfer;

public class CreditTransferDto extends TransferDto {
    private Float quantity;
    private long firm;

    public CreditTransferDto(CreditTransfer creditTransfer) {
        super(
                creditTransfer.getId(),
                creditTransfer.getDateTime(),
                creditTransfer.getReason(),
                creditTransfer.getAssignedBy().getId(),
                creditTransfer.getClientAccount().getId()
        );
        this.quantity = creditTransfer.getQuantity();
        this.firm = creditTransfer.getFirm().getId();
    }

    public Float getQuantity() {
        return quantity;
    }

    public void setQuantity(Float quantity) {
        this.quantity = quantity;
    }

    public long getFirm() {
        return firm;
    }

    public void setFirm(long firm) {
        this.firm = firm;
    }
}
