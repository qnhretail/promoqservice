package be.qnh.promoq.api.dataTransferObjects;

import be.qnh.promoq.main.models.Firm;

import java.util.ArrayList;

public class FirmListDto {
    private ArrayList<FirmDto> firms;

    public FirmListDto(Iterable<Firm> firms) {
        this.firms = new ArrayList<>();
        for (Firm firm : firms) {
            this.firms.add(new FirmDto(firm));
        }
    }

    public ArrayList<FirmDto> getFirms() {
        return firms;
    }

    public void setFirms(ArrayList<FirmDto> firms) {
        this.firms = firms;
    }
}
