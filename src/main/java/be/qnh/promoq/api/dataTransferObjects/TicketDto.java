package be.qnh.promoq.api.dataTransferObjects;

import be.qnh.promoq.api.dataTransferObjects.helpers.ClientAccountGiftArticleDtoHelper;
import be.qnh.promoq.api.dataTransferObjects.helpers.ClientAccountValuePromotionDtoHelper;
import be.qnh.promoq.api.dataTransferObjects.helpers.TicketProductDtoHelper;
import be.qnh.promoq.api.dataTransferObjects.helpers.TicketPromotionDtoHelper;
import be.qnh.promoq.main.enums.TicketStatus;
import be.qnh.promoq.main.models.Ticket;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Date;

@Component
public class TicketDto {
    private TicketStatus ticketStatus;
    private long id;
    private String registerSerialNr;
    private String clientCardNr;
    private String staffMember;
    private long ticketNr;
    private Date dateTime;
    private Iterable<TicketProductDto> articles;
    private Iterable<TicketPromotionDto> promotions;
    private Iterable<ClientAccountGiftArticleDto> giftArticles;
    private Iterable<ClientAccountValuePromotionDto> valuePromotions;

    public TicketDto(long id, String registerSerialNr, String clientCardNr, String staffMember, long ticketNr,
                     Date dateTime, Iterable<TicketProductDto> articles,
                     Iterable<TicketPromotionDto> ticketPromotionDtos,
                     Iterable<ClientAccountGiftArticleDto> clientAccountGiftArticleDtos,
                     Iterable<ClientAccountValuePromotionDto> valuePromotions) {
        this.id = id;
        this.registerSerialNr = registerSerialNr;
        this.clientCardNr = clientCardNr;
        this.staffMember = staffMember;
        this.ticketNr = ticketNr;
        this.dateTime = dateTime;
        this.articles = articles;
        this.promotions = ticketPromotionDtos;
        this.giftArticles = clientAccountGiftArticleDtos;
        this.valuePromotions = valuePromotions;
    }

    public TicketDto(long id, TicketStatus ticketStatus) {
        this.id = id;
        this.ticketStatus = ticketStatus;
    }

    public TicketDto() {
    }

    public TicketDto(Ticket ticket) {
        this(
                ticket.getId(),
                ticket.getRegisterAccount().getSerialNr(),
                ticket.getClient().getCardId(),
                ticket.getStaffMemeber(),
                ticket.getTicketNumber(),
                ticket.getDateTime(),
                TicketProductDtoHelper.getTicketProductDtoArray(ticket),
                TicketPromotionDtoHelper.getTicketPromotionDtoArray(ticket),
                ClientAccountGiftArticleDtoHelper.getClientAccountGiftArticleDtoArrayForTicket(ticket),
                ClientAccountValuePromotionDtoHelper.getClientAccountValuePromotionDtoArrayForTicket(ticket)
        );
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRegisterSerialNr() {
        return registerSerialNr;
    }

    public void setRegisterSerialNr(String registerSerialNr) {
        this.registerSerialNr = registerSerialNr;
    }

    public String getClientCardNr() {
        return clientCardNr;
    }

    public void setClientCardNr(String clientCardNr) {
        this.clientCardNr = clientCardNr;
    }

    public String getStaffMember() {
        return staffMember;
    }

    public void setStaffMember(String staffMember) {
        this.staffMember = staffMember;
    }

    public long getTicketNr() {
        return ticketNr;
    }

    public void setTicketNr(long ticketNr) {
        this.ticketNr = ticketNr;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public Iterable<TicketProductDto> getArticles() {
        return articles;
    }

    public void setArticles(Collection<TicketProductDto> articles) {
        this.articles = articles;
    }

    public Iterable<TicketPromotionDto> getPromotions() {
        return promotions;
    }

    public void setPromotions(Collection<TicketPromotionDto> promotions) {
        this.promotions = promotions;
    }

    public TicketStatus getTicketStatus() {
        return ticketStatus;
    }

    public void setTicketStatus(TicketStatus ticketStatus) {
        this.ticketStatus = ticketStatus;
    }

    public Iterable<ClientAccountGiftArticleDto> getGiftArticles() {
        return giftArticles;
    }

    public void setGiftArticles(Iterable<ClientAccountGiftArticleDto> giftArticles) {
        this.giftArticles = giftArticles;
    }

    public Iterable<ClientAccountValuePromotionDto> getValuePromotions() {
        return valuePromotions;
    }

    public void setValuePromotions(Iterable<ClientAccountValuePromotionDto> valuePromotions) {
        this.valuePromotions = valuePromotions;
    }

    public void populateWithTicket(Ticket ticket) {
    }
}
