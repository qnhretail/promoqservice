package be.qnh.promoq.api.dataTransferObjects;

import be.qnh.promoq.main.models.Firm;

import java.util.Collection;

public class FirmDto {
    private long id;
    private String name;
    private Collection<BranchDto> branches;

    public FirmDto(Firm firm) {
        this.id = firm.getId();
        this.name = firm.getName();
        //this.branches = new ArrayList<>();
        //Collection<Branch> branchesTemp = firm.getBranches();
        //for (Branch branch : branchesTemp) {
        //    branches.save(new BranchDto(branch.getId(),branch.getName(),this.id));
        //}
    }

    public FirmDto() {
    }

    //public Collection<BranchDto> getBranches() {
    //    return branches;
    //}

    //public void setBranches(Collection<BranchDto> branches) {
    //    this.branches = branches;
    //}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
