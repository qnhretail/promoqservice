package be.qnh.promoq.api.dataTransferObjects;

import be.qnh.promoq.main.models.RegisterAccount;

public class RegisterDto {
    private long id;
    private String description;
    private String serialNr;
    private long branchId;
    private String password;

    public RegisterDto(RegisterAccount registerAccount) {
        id = registerAccount.getId();
        description = registerAccount.getDescription();
        serialNr = registerAccount.getSerialNr();
        branchId = registerAccount.getBranch().getId();
    }

    public RegisterDto() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSerialNr() {
        return serialNr;
    }

    public void setSerialNr(String serialNr) {
        this.serialNr = serialNr;
    }

    public long getBranchId() {
        return branchId;
    }

    public void setBranchId(long branchId) {
        this.branchId = branchId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
