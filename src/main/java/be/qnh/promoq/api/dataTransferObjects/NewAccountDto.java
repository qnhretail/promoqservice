package be.qnh.promoq.api.dataTransferObjects;

import java.util.ArrayList;

public interface NewAccountDto {
    String getUsername();

    ArrayList<String> getRoles();

    String getPassword();
}
