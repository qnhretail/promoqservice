package be.qnh.promoq.api.dataTransferObjects;

import be.qnh.promoq.main.Constants;

import java.util.ArrayList;

public class NewClientDto implements NewAccountDto {
    private String username;
    private String password;
    private ArrayList<String> roles = new ArrayList<String>() {{
        add(Constants.ROLE_CLIENT);
    }};
    private String email;

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public ArrayList<String> getRoles() {
        return roles;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
