package be.qnh.promoq.api.dataTransferObjects;

public class NewPasswordDto {
    private String password;

    public NewPasswordDto() {
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
