package be.qnh.promoq.api.dataTransferObjects;

import be.qnh.promoq.main.models.SmtpServerConfiguration;

public class SmtpServerConfigurationDto {
    private long id;
    private boolean startTls;
    private String host;
    private short port;
    private boolean auth;
    private long ownerAccountId;
    private String username;

    public SmtpServerConfigurationDto(SmtpServerConfiguration smtpServerConfiguration) {
        id = smtpServerConfiguration.getId();
        startTls = smtpServerConfiguration.isStartTls();
        host = smtpServerConfiguration.getHost();
        port = smtpServerConfiguration.getPort();
        auth = smtpServerConfiguration.isAuth();
        if (auth) {
            username = smtpServerConfiguration.getUsername();
        }
        ownerAccountId = smtpServerConfiguration.getOwnerAccount().getId();
    }

    public SmtpServerConfigurationDto() {
    }

    public String getHost() {
        return host;
    }

    public short getPort() {
        return port;
    }

    public boolean isAuth() {
        return auth;
    }

    public boolean isStartTls() {
        return startTls;
    }

    public void setStartTls(boolean startTls) {
        this.startTls = startTls;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setPort(short port) {
        this.port = port;
    }

    public void setAuth(boolean auth) {
        this.auth = auth;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getOwnerAccountId() {
        return ownerAccountId;
    }

    public void setOwnerAccountId(long ownerAccountId) {
        this.ownerAccountId = ownerAccountId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
