package be.qnh.promoq.api.dataTransferObjects;

import be.qnh.promoq.main.models.ClientAccountFirm;

public class ClientAccountFirmDto {
    private Float credit;
    private long clientId;
    private long firmId;
    private String firmName;
    private boolean receivePromotions;
    private boolean receiveNewsletter;

    public ClientAccountFirmDto(ClientAccountFirm clientAccountFirm) {
        this.clientId = clientAccountFirm.getClientAccount().getId();
        this.firmId = clientAccountFirm.getFirm().getId();
        this.firmName = clientAccountFirm.getFirm().getName();
        this.receivePromotions = clientAccountFirm.isReceivePromotions();
        this.receiveNewsletter = clientAccountFirm.isReceiveNewsletter();
        this.credit = clientAccountFirm.getCredit();
    }

    public ClientAccountFirmDto() {
    }

    public long getClientId() {
        return clientId;
    }

    public void setClientId(long clientId) {
        this.clientId = clientId;
    }

    public long getFirmId() {
        return firmId;
    }

    public void setFirmId(long firmId) {
        this.firmId = firmId;
    }

    public String getFirmName() {
        return firmName;
    }

    public void setFirmName(String firmName) {
        this.firmName = firmName;
    }

    public boolean isReceivePromotions() {
        return receivePromotions;
    }

    public void setReceivePromotions(boolean receivePromotions) {
        this.receivePromotions = receivePromotions;
    }

    public boolean isReceiveNewsletter() {
        return receiveNewsletter;
    }

    public void setReceiveNewsletter(boolean receiveNewsletter) {
        this.receiveNewsletter = receiveNewsletter;
    }

    public Float getCredit() {
        return credit;
    }

    public void setCredit(Float credit) {
        this.credit = credit;
    }
}
