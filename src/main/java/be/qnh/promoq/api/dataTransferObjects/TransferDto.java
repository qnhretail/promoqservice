package be.qnh.promoq.api.dataTransferObjects;

import java.util.Date;

public class TransferDto {
    private long id;
    private Date dateTime;
    private String reason;
    private long assignedBy;
    private long clientAccount;

    public TransferDto(long id, Date dateTime, String reason, long assignedBy, long clientAccount) {
        this.id = id;
        this.dateTime = dateTime;
        this.reason = reason;
        this.assignedBy = assignedBy;
        this.clientAccount = clientAccount;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public long getAssignedBy() {
        return assignedBy;
    }

    public void setAssignedBy(long assignedBy) {
        this.assignedBy = assignedBy;
    }

    public long getClientAccount() {
        return clientAccount;
    }

    public void setClientAccount(long clientAccount) {
        this.clientAccount = clientAccount;
    }
}
