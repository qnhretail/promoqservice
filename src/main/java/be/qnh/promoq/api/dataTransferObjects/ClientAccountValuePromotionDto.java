package be.qnh.promoq.api.dataTransferObjects;

import be.qnh.promoq.main.models.ClientAccountPromotion;

public class ClientAccountValuePromotionDto {
    private long promotionId;
    private String promotionName;
    private long points;

    public ClientAccountValuePromotionDto(long id, String name, long points) {
        this.promotionId = id;
        this.promotionName = name;
        this.points = points;
    }

    public ClientAccountValuePromotionDto(ClientAccountPromotion clientAccountPromotion) {
        this(clientAccountPromotion.getPromotion().getId(),
                clientAccountPromotion.getPromotion().getName(),
                clientAccountPromotion.getPoints());
    }

    public long getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(long promotionId) {
        this.promotionId = promotionId;
    }

    public String getPromotionName() {
        return promotionName;
    }

    public void setPromotionName(String promotionName) {
        this.promotionName = promotionName;
    }

    public long getPoints() {
        return points;
    }

    public void setPoints(long points) {
        this.points = points;
    }
}
