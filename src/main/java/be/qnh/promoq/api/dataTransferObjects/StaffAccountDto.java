package be.qnh.promoq.api.dataTransferObjects;

import be.qnh.promoq.main.models.StaffAccount;

public class StaffAccountDto {
    private long firmId;
    private long id;
    private String username;
    private String name;
    private String surname;
    private String email;

    public StaffAccountDto(StaffAccount account) {
        this.id = account.getId();
        this.username = account.getUsername();
        this.name = account.getName();
        this.surname = account.getSurname();
        this.email = account.getEmail();
        this.firmId = account.getFirm().getId();
    }

    public StaffAccountDto() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getFirmId() {
        return firmId;
    }

    public void setFirmId(long firmId) {
        this.firmId = firmId;
    }
}
