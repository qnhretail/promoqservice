package be.qnh.promoq.api.dataTransferObjects;

import java.util.ArrayList;

public interface AccountDto {
    String getUsername();
    ArrayList<String> getRoles();
}
