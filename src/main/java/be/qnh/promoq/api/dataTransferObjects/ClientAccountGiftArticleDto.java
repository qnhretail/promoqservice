package be.qnh.promoq.api.dataTransferObjects;

import be.qnh.promoq.main.models.ClientAccountGiftArticle;

import java.util.ArrayList;

public class ClientAccountGiftArticleDto {
    private long giftArticleId;
    private String name;
    private Iterable<String> productPlus;
    private long quantity;

    public ClientAccountGiftArticleDto(long giftArticleId, String name, Iterable<String> productPlus, long quantity) {
        this.giftArticleId = giftArticleId;
        this.name = name;
        this.productPlus = productPlus;
        this.quantity = quantity;
    }

    public ClientAccountGiftArticleDto(ClientAccountGiftArticle clientAccountGiftArticle) {
        this.giftArticleId = clientAccountGiftArticle.getGiftArticle().getId();
        this.name = clientAccountGiftArticle.getGiftArticle().getName();
        this.productPlus = new ArrayList<String>() {{
            clientAccountGiftArticle.getGiftArticle().getProducts().forEach(
                    product -> add(product.getProductCode()));
        }};
        this.quantity = clientAccountGiftArticle.getQuantity();
    }

    public long getGiftArticleId() {
        return giftArticleId;
    }

    public void setGiftArticleId(long giftArticleId) {
        this.giftArticleId = giftArticleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public Iterable<String> getProductPlus() {
        return productPlus;
    }

    public void setProductPlus(Iterable<String> productPlus) {
        this.productPlus = productPlus;
    }
}
