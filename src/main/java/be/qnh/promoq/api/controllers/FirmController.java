package be.qnh.promoq.api.controllers;

import be.qnh.promoq.api.dataTransferObjects.FirmDto;
import be.qnh.promoq.api.dataTransferObjects.FirmListDto;
import be.qnh.promoq.main.Constants;
import be.qnh.promoq.main.dataAccessObjects.FirmDaoImpl;
import be.qnh.promoq.main.models.ClientAccount;
import be.qnh.promoq.main.models.Firm;
import be.qnh.promoq.main.models.OwnerAccount;
import be.qnh.promoq.main.models.StaffAccount;
import be.qnh.promoq.security.models.Account;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequestMapping(Constants.API_ROOT)
@Api(value = "firm management", description = "api for firm management")
class FirmController {
    @Autowired
    FirmDaoImpl firmDao;
    @Autowired
    private RequestHelper requestHelper;

    @RequestMapping(path = {
            Constants.API_ROLE_ROOT + "/firms/",
            Constants.API_ROLE_OWNER + "/firms/"
    }, method = RequestMethod.POST)
    public ResponseEntity<?> addFirm(@RequestBody Firm firm, HttpServletRequest request) {
        OwnerAccount account = (OwnerAccount) requestHelper.getReqAccount(request);
        Firm newFirm;
        if (requestHelper.isRoot(request)) {
            newFirm = firmDao.save(firm);
        } else {
            if (account.getMaxFirms() > account.getFirms().size()) {
                newFirm = firmDao.save(firm, account);
            } else return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
        }
        return ResponseEntity.ok(new FirmDto(newFirm));
    }

    @RequestMapping(path = {
            Constants.API_ROLE_ROOT + "/firms/{id}",
            Constants.API_ROLE_OWNER + "/{ownerId}"
    }, method = RequestMethod.POST)
    public void addOwnerToFirm (@PathVariable("id") long id,
                                @PathVariable("ownerId") long ownerId,
                                HttpServletRequest request) {
        firmDao.setOwner(id, ownerId, request.getUserPrincipal().getName());
    }

    @RequestMapping(path = {
            Constants.API_ROLE_ROOT + "/firms/{id}",
            Constants.API_ROLE_OWNER + "/firms/{id}"
    }, method = RequestMethod.GET)
    public FirmDto getFirmById(@PathVariable long id, HttpServletRequest request) {
        Firm firm = firmDao.findById(id);
        if (requestHelper.isRoot(request))
            return new FirmDto(firm);
        else {
            Account requestAccount = requestHelper.getReqAccount(request);
            ArrayList<Account> accounts = new ArrayList<Account>()
            {{
                        add(firm.getCreatedBy());
                        add(firm.getAssignedBy());
                        add(firm.getAssignedTo());
            }};
            if (accounts.contains(requestAccount)) {
                return new FirmDto(firm);
            }
            return null;
        }

    }

    private Collection<Firm> getAllFirmsForRequest(HttpServletRequest request) {
        Account requestAccount = requestHelper.getReqAccount(request);
        if (requestHelper.isRoot(request))
            return firmDao.findAll();
        else if (request.isUserInRole(Constants.ROLE_OWNER)) {
            return firmDao.getAllCreatedByOrAssignedByOrAssignedTo(requestAccount);
        } else if (request.isUserInRole(Constants.ROLE_CLIENT)) {
            return ((ClientAccount) requestAccount).getFirms();
        } else if (request.isUserInRole(Constants.ROLE_STAFF)) {
            return new ArrayList<Firm>() {{
                add(((StaffAccount) requestAccount).getFirm());
            }};
        }
        return null;
    }

    @RequestMapping(path = {
            Constants.API_ROLE_ROOT + "/firms/with_promotions",
            Constants.API_ROLE_OWNER + "/firms/with_promotions",
            Constants.API_ROLE_CLIENT + "/firms/with_promotions"
    }, method = RequestMethod.GET)
    public ArrayList<FirmDto> getAllFirmsWithPromotion(HttpServletRequest request) {
        return (new FirmListDto(
                getAllFirmsForRequest(request).stream()
                        .filter(firm -> firm.getPromotions().size() > 0).collect(Collectors.toList())
        )
        ).getFirms();
    }

    @RequestMapping(path = {
            Constants.API_ROLE_ROOT + "/firms",
            Constants.API_ROLE_OWNER + "/firms",
            Constants.API_ROLE_CLIENT + "/firms",
            Constants.API_ROLE_STAFF + "/firms"
    }, method = RequestMethod.GET)
    public ArrayList<FirmDto> getAllFirms(HttpServletRequest request) {
        return (new FirmListDto(getAllFirmsForRequest(request))).getFirms();
    }

    @RequestMapping(path = {Constants.API_ROLE_ROOT + "/firms/orphans", Constants.API_ROLE_OWNER + "/firms/orphans"}, method = RequestMethod.GET)
    public ArrayList<FirmDto> getAllOrphanFirms(HttpServletRequest request) {
        ArrayList<Firm> firmList;
        if (requestHelper.isRoot(request))
            firmList = (ArrayList<Firm>) firmDao.getAllOrphan();
        else {
            firmList = (ArrayList<Firm>) firmDao.getAllOrphanCreatedBy(requestHelper.getReqAccount(request));
        }
        return new FirmListDto(firmList).getFirms();
    }

    @RequestMapping(path = {
            Constants.API_ROLE_ROOT + "/firms/{id}",
            Constants.API_ROLE_OWNER + "/firms/{id}"
    }, method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteFirmById(@PathVariable long id, HttpServletRequest request) {
        Firm firm = firmDao.findById(id);
        Account account = requestHelper.getReqAccount(request);
        if (!firm.getAssignedToCreatedByAssignedBy().contains(account)) {
            return new ResponseEntity<Object>(
                    HttpStatus.FORBIDDEN);
        }
        firmDao.deactivate(firm);
        return new ResponseEntity<Object>(HttpStatus.OK);
    }

    ////////////////
    // Firm Owner //
    ////////////////

    @RequestMapping(path = {
            Constants.API_ROLE_ROOT + "/firms/{id}/owner/",
            Constants.API_ROLE_OWNER + "/firms/{id}/owner/"
    }, method = RequestMethod.POST)
    public Firm assignOwnerToFirm(@PathVariable("id") long id,
                                  @RequestParam("ownerId") long ownerId,
                                  HttpServletRequest request) {
        String requestUsername = request.getUserPrincipal().getName();
        if (requestHelper.isRoot(request)) {
            Account requestAccount = requestHelper.getReqAccount(request);
            if (requestAccount instanceof OwnerAccount)
                return firmDao.addOwner(id,
                        (OwnerAccount) requestAccount,
                        requestUsername);
        } else if (Objects.equals(firmDao.findById(id).getCreatedBy().getUsername(), requestUsername)) {
            return firmDao.addOwner(id,
                    (OwnerAccount) requestHelper.getReqAccount(request),
                    requestUsername);
        }
        return null;
    }

    @RequestMapping(path = {
            Constants.API_ROLE_ROOT + "/firms/{id}/owner/",
            Constants.API_ROLE_OWNER + "/firms/{id}/owner/"
    }, method = RequestMethod.DELETE)
    public Firm deleteOwnerFromFirm(@PathVariable("id") long id, HttpServletRequest request) {
        if (requestHelper.isRoot(request) || firmDao.findById(id).getAssignedBy().equals(requestHelper.getReqAccount(request)))
            return firmDao.deleteOwner(id);
        return null;
    }

    //////////////////
    // Firm Clients //
    //////////////////

    @RequestMapping(path = {
            Constants.API_ROLE_ROOT + "/firms/{id}/clients/",
            Constants.API_ROLE_OWNER + "/firms/{id}/clients/"
    }, method = RequestMethod.POST)
    public Firm addClientToFirm(@PathVariable("id") long id,
                                @RequestParam("clientIds") ArrayList<Integer> clientIds,
                                HttpServletRequest request) {
        if (requestHelper.isRoot(request) || firmDao.findById(id).getAssignedTo().equals(requestHelper.getReqAccount(request))) {
            return firmDao.addClientAccount(id, clientIds);
        }
        return null;
    }

    @RequestMapping(path = {
            Constants.API_ROLE_ROOT + "/firm/{id}/clients/",
            Constants.API_ROLE_OWNER + "/firms/{id}/clients/"
    }, method = RequestMethod.DELETE)
    public Firm deleteClientFromFirm(@PathVariable("id") long id,
                                     @RequestParam("clientIds") ArrayList<Integer> clientIds,
                                     HttpServletRequest request) {
        if (requestHelper.isRoot(request) || firmDao.findById(id).getAssignedTo().equals(requestHelper.getReqAccount(request))) {
            return firmDao.deleteClientAccounts(id, clientIds);
        }
        return null;
    }

}
