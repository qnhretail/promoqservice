package be.qnh.promoq.api.controllers;

import be.qnh.promoq.api.dataTransferObjects.SmtpServerConfigurationDto;
import be.qnh.promoq.main.Constants;
import be.qnh.promoq.main.dataAccessObjects.SmtpServerConfigurationDao;
import be.qnh.promoq.main.models.OwnerAccount;
import be.qnh.promoq.main.models.SmtpServerConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@CrossOrigin
@RequestMapping(Constants.API_ROOT)
public class AccountConfigurationController {
    @Autowired
    private RequestHelper requestHelper;
    @Autowired
    private SmtpServerConfigurationDao smtpServerConfigurationDao;

    @RequestMapping(path = {
            Constants.API_ROLE_OWNER + "/accounts/configuration/smtpserver"},
            method = RequestMethod.POST)
    public ResponseEntity<SmtpServerConfigurationDto> setSmtpServerConfiguration(
            @RequestBody SmtpServerConfigurationDto smtpServerConfigurationDto,
            HttpServletRequest request) {
        OwnerAccount ownerAccount = (OwnerAccount) requestHelper.getReqAccount(request);
        SmtpServerConfiguration smtpServerConfiguration = smtpServerConfigurationDao.findByOwnerAccount(ownerAccount);
        if (smtpServerConfiguration != null) {
            updateSmtpServerConfiguration(smtpServerConfiguration, smtpServerConfigurationDto);
        } else {
            smtpServerConfiguration = smtpServerConfigurationDao.save(
                    new SmtpServerConfiguration(smtpServerConfigurationDto, ownerAccount));
        }
        return ResponseEntity.ok(new SmtpServerConfigurationDto(smtpServerConfiguration));
    }

    private void updateSmtpServerConfiguration(
            SmtpServerConfiguration smtpServerConfiguration,
            SmtpServerConfigurationDto smtpServerConfigurationDto) {
        smtpServerConfiguration.setHost(smtpServerConfigurationDto.getHost());
        smtpServerConfiguration.setPort(smtpServerConfigurationDto.getPort());
        smtpServerConfiguration.setAuth(smtpServerConfigurationDto.isAuth());
        if (smtpServerConfiguration.isAuth())
            smtpServerConfiguration.setUsername(smtpServerConfigurationDto.getUsername());
        smtpServerConfiguration.setStartTls(smtpServerConfigurationDto.isStartTls());
        smtpServerConfigurationDao.save(smtpServerConfiguration);
    }

    @RequestMapping(path = {
            Constants.API_ROLE_OWNER + "/accounts/configuration/smtpserver"},
            method = RequestMethod.GET)
    public ResponseEntity<SmtpServerConfigurationDto> setSmtpServerConfiguration(HttpServletRequest request) {
        return ResponseEntity.ok(
                new SmtpServerConfigurationDto(
                        smtpServerConfigurationDao.findByOwnerAccount(
                                (OwnerAccount) requestHelper.getReqAccount(request)
                        )
                )
        );
    }
}
