package be.qnh.promoq.api.controllers;

import be.qnh.promoq.api.controllers.helpers.RegisterTicketControllerHelper;
import be.qnh.promoq.api.dataTransferObjects.TicketDto;
import be.qnh.promoq.main.Constants;
import be.qnh.promoq.main.exceptions.RegisterNotFoundException;
import be.qnh.promoq.main.exceptions.WrongRegisterException;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@CrossOrigin
@Component
@RequestMapping({
        Constants.API_ROOT + Constants.API_ROLE_REGISTER + "/ticket",
        Constants.API_ROOT + Constants.API_ROLE_ADMIN + "/ticket",
        Constants.API_ROOT + Constants.API_ROLE_OWNER + "/ticket"
})
@Api(value = "Register ticket registration and validation")
public class RegisterTicketController {

    @Autowired
    private RegisterTicketControllerHelper registerTicketControllerHelper;

    @RequestMapping(
            method = RequestMethod.POST)
    public ResponseEntity<Object> registerTicket(
            @RequestBody TicketDto ticketDto)
            throws WrongRegisterException, RegisterNotFoundException, Exception {
        return registerTicketControllerHelper.registerTicket(ticketDto);
    }

    @RequestMapping(
            path = "{id}",
            method = RequestMethod.GET)
    public ResponseEntity<TicketDto> getTicket(
            @PathVariable long id,
            @RequestParam(value = "dummy", defaultValue = "false") boolean dummy,
            HttpServletRequest request) {
        if (dummy) {
            return registerTicketControllerHelper.getDummyTicket(id, request);
        } else {
            return registerTicketControllerHelper.getTicketById(id, request);
        }
    }

    @RequestMapping(
            path = "settle/{id}",
            method = RequestMethod.POST)
    public ResponseEntity<TicketDto> settleTicket(
            @PathVariable long id,
            @RequestParam(value = "forgotten_card", defaultValue = "false") boolean forgottenCard,
            @RequestParam(value = "client_card_id", required = false) String clientCardId,
            HttpServletRequest request) {
        return registerTicketControllerHelper.settleTicket(id, forgottenCard, clientCardId, request);
    }

    @RequestMapping(
            path = "settle/giftArticle/{cardId}/{giftArticleId}",
            method = RequestMethod.POST)
    public ResponseEntity<?> settleGiftArticle(
            @PathVariable String cardId,
            @PathVariable long giftArticleId,
            HttpServletRequest request) {
        return registerTicketControllerHelper.settleGiftArticle(cardId, giftArticleId, request);
    }
}
