package be.qnh.promoq.api.controllers;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Aspect
@Component
public class AnnotationParameterValidatorProxy {
    @Pointcut("within(@org.springframework.web.bind.annotation.RestController  *)")
    public void controller() {
    }

    @Pointcut("execution(* *(..))")
    public void methodPointcut() {
    }

    @Pointcut("within(@org.springframework.web.bind.annotation.RequestMapping *)")
    public void requestMapping() {
    }

    @Before("controller() && methodPointcut() && requestMapping()")
    public void aroundControllerMethod(JoinPoint joinPoint) throws Throwable {
        System.out.println("Invoked: " + niceName(joinPoint));
    }

    @AfterReturning("controller() && methodPointcut() && requestMapping()")
    public void afterControllerMethod(JoinPoint joinPoint) {
        System.out.println("Finished: " + niceName(joinPoint));
        System.out.println("Finished: " + joinPoint.getSignature().toLongString());
    }

    @Around("controller() $$ methodPointcut() $$ requestMapping()")
    public Object aroundControllerMethod(ProceedingJoinPoint call) throws Throwable {
        Object result = call.proceed();
        return result;
    }

    private String niceName(JoinPoint joinPoint) {
        return joinPoint.getTarget().getClass()
                + "#" + joinPoint.getSignature().getName()
                + "\n\targs:" + Arrays.toString(joinPoint.getArgs());
    }

}
