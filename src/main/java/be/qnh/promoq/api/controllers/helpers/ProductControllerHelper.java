package be.qnh.promoq.api.controllers.helpers;

import be.qnh.promoq.api.controllers.RequestHelper;
import be.qnh.promoq.api.dataTransferObjects.ProductDto;
import be.qnh.promoq.main.dataAccessObjects.FirmDaoImpl;
import be.qnh.promoq.main.dataAccessObjects.ProductDaoImpl;
import be.qnh.promoq.main.models.Firm;
import be.qnh.promoq.main.models.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collection;

@Component
public class ProductControllerHelper {
    @Autowired
    private FirmDaoImpl firmDao;
    @Autowired
    ProductDaoImpl productDao;
    @Autowired
    private RequestHelper requestHelper;

    public ResponseEntity<ProductDto> updateProduct(ProductDto productDto, boolean isNew, HttpServletRequest request) {
        Firm firm = firmDao.findById(productDto.getFirmId());
        if (requestHelper.getReqAccountFirms(request).contains(firm)) {
            Product product = isNew ? new Product() : productDao.findById(productDto.getId());
            if (product != null) {
                product.setArticleGroup(productDto.getArticleGroup());
                product.setProductCode(productDto.getProductCode());
                product.setName(productDto.getName());
                product.setFirm(firm);
                productDao.save(product);
                productDto = new ProductDto(product);
                return ResponseEntity.ok(productDto);
            }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
    }

    public ResponseEntity<Collection<ProductDto>> getAllProductsForFirm(long id, HttpServletRequest request) {
        Firm firm = firmDao.findById(id);
        if (requestHelper.getReqAccountFirms(request).contains(firm)) {
            Collection<Product> products = productDao.findByFirm(firm);
            Collection<ProductDto> productDtos = new ArrayList<ProductDto>() {{
                products.forEach(product -> add(new ProductDto(product)));
            }};
            return ResponseEntity.ok(productDtos);
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
    }
}