package be.qnh.promoq.api.controllers.helpers;

import be.qnh.promoq.api.controllers.RequestHelper;
import be.qnh.promoq.api.dataTransferObjects.*;
import be.qnh.promoq.main.AsyncLogger;
import be.qnh.promoq.main.AsyncTicketProcessor;
import be.qnh.promoq.main.dataAccessObjects.ClientAccountGiftArticleDaoImpl;
import be.qnh.promoq.main.dataAccessObjects.ClientDaoImpl;
import be.qnh.promoq.main.dataAccessObjects.GiftArticleDaoImpl;
import be.qnh.promoq.main.dataAccessObjects.TicketDaoImpl;
import be.qnh.promoq.main.models.*;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import static be.qnh.promoq.main.enums.Method.POST;
import static be.qnh.promoq.main.enums.TicketStatus.*;

@Component
public class RegisterTicketControllerHelper {
    @Autowired
    private RequestHelper requestHelper;
    @Autowired
    private TicketDaoImpl ticketDao;
    @Autowired
    private ClientDaoImpl clientDao;

    @Autowired
    private AsyncTicketProcessor asyncTicketProcessor;
    @Autowired
    private AsyncLogger asyncLogger;
    @Autowired
    private ClientAccountGiftArticleDaoImpl clientAccountGiftArticleDao;
    @Autowired
    private GiftArticleDaoImpl giftArticleDao;

    public ResponseEntity<TicketDto> getDummyTicket(long ticketId, HttpServletRequest request) {

        RegisterAccount registerAccount = (RegisterAccount) requestHelper.getReqAccount(request);
        Collection<TicketProductDto> ticketProductDtos = new ArrayList<TicketProductDto>() {{
            add(new TicketProductDto(5, "PLU0004", "usefull product", 5, 5.22f, 2.55f));
        }};
        Collection<TicketPromotionDto> ticketPromotionDtos = new ArrayList<TicketPromotionDto>() {{
            add(new TicketPromotionDto(8, 2, 55, 500,
                    new ArrayList<GiftArticleDto>() {{
                        add(new GiftArticleDto(1, 2, "5euro bon",
                                new ArrayList<Long>() {{
                                    add(2L);
                                    add(5L);
                                }}));
                    }},
                    new Date(), new Date()));
        }};
        Collection<ClientAccountGiftArticleDto> clientAccountGiftArticleDtos = new ArrayList<ClientAccountGiftArticleDto>() {{
            add(new ClientAccountGiftArticleDto(13, "2.5 euro bon",
                    new ArrayList<String>() {{
                        add("2");
                        add("5");
                    }}, 2));
        }};
        TicketDto ticketDto = new TicketDto(ticketId, registerAccount.getSerialNr(), "Client00001",
                "Joris", 2, new Date(), ticketProductDtos, ticketPromotionDtos,
                clientAccountGiftArticleDtos, null);
        return ResponseEntity.ok(ticketDto);
    }

    public ResponseEntity<TicketDto> getTicketById(long ticketId, HttpServletRequest request) {
        RegisterAccount registerAccount = (RegisterAccount) requestHelper.getReqAccount(request);
        Ticket ticket = ticketDao.findById(ticketId);
        if (ticket.getRegisterAccount().getBranch() != registerAccount.getBranch())
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
        if (ticket.getTicketStatus().nummer() == 70 || ticket.getTicketStatus().nummer() <= 35) {
            return ResponseEntity.ok(new TicketDto(ticket.getId(), ticket.getTicketStatus()));
        }
        return ResponseEntity.ok(new TicketDto(ticket));
    }

    public ResponseEntity<Object> registerTicket(TicketDto ticketDto) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        asyncLogger.logger(POST, "registerTicket", mapper.writeValueAsString(ticketDto));
        Ticket ticket = ticketDao.save(new Ticket(S10));
        asyncTicketProcessor.registerTicket(ticketDto, ticket);
        return ResponseEntity.ok(new TicketDto(ticket.getId(), ticket.getTicketStatus()));
    }

    public ResponseEntity<TicketDto> settleForgottenCard(String clientCardId, Ticket ticket) {
        Client client = clientDao.findByCardId(clientCardId);
        if (client == null) return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        ticket.setClient(client);
        ticket.setTicketStatus(S47);
        ticketDao.save(ticket);
        asyncTicketProcessor.settleTicket(ticket);
        return ResponseEntity.ok(new TicketDto(ticket.getId(), ticket.getTicketStatus()));
    }

    public ResponseEntity<TicketDto> settleTicketAfterPayment(Ticket ticket) {
        if (ticket.getClient() == null) {
            ticket.setTicketStatus(S46);
            ticketDao.save(ticket);
        } else {
            ticket.setTicketStatus(S45);
            ticketDao.save(ticket);
            asyncTicketProcessor.settleTicket(ticket);
        }
        return ResponseEntity.ok(new TicketDto(ticket.getId(), ticket.getTicketStatus()));
    }

    public ResponseEntity<GiftArticleDto> settleGiftArticle(String clientCardId, Long giftArticleId,
                                                            HttpServletRequest request) {
        RegisterAccount registerAccount = (RegisterAccount) requestHelper.getReqAccount(request);
        ClientAccount clientAccount = clientDao.findByCardId(clientCardId).getAccount();
        GiftArticle giftArticle = giftArticleDao.findById(giftArticleId);
        if (registerAccount.getBranch().getFirm() != giftArticle.getFirm()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
        }
        ClientAccountGiftArticle clientAccountGiftArticle =
                clientAccountGiftArticleDao.findByClientAccountAndGiftArticle(clientAccount, giftArticleId);
        if (clientAccountGiftArticle != null) {
            long quantity = clientAccountGiftArticle.getQuantity();
            if (quantity > 0) {
                clientAccountGiftArticle.setQuantity(quantity - 1);
                clientAccountGiftArticleDao.save(clientAccountGiftArticle);
                return ResponseEntity.ok(new GiftArticleDto(clientAccountGiftArticle.getGiftArticle()));
            }
        }
        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(null);
    }

    public ResponseEntity<TicketDto> settleTicket(Long ticketId,
                                                  boolean forgottenCard, String clientCardId, HttpServletRequest request) {
        RegisterAccount registerAccount = (RegisterAccount) requestHelper.getReqAccount(request);
        Ticket ticket = ticketDao.findById(ticketId);
        if (ticket.getRegisterAccount().getBranch() != registerAccount.getBranch())
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
        if (forgottenCard) {
            return settleForgottenCard(clientCardId, ticket);
        } else {
            return settleTicketAfterPayment(ticket);
        }
    }
}
