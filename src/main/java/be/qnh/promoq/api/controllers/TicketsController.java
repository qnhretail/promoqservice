package be.qnh.promoq.api.controllers;

import be.qnh.promoq.api.dataTransferObjects.TicketDto;
import be.qnh.promoq.api.dataTransferObjects.TicketProductDto;
import be.qnh.promoq.main.Constants;
import be.qnh.promoq.main.dataAccessObjects.TicketDaoImpl;
import be.qnh.promoq.main.dataAccessObjects.TicketProductDaoImpl;
import be.qnh.promoq.main.models.TicketProduct;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collection;

@RestController
@CrossOrigin
@RequestMapping(Constants.API_ROOT)
@Api(value = "ticket management")
public class TicketsController {
    @Autowired
    private TicketDaoImpl ticketDao;
    @Autowired
    private TicketProductDaoImpl ticketProductDao;

    @RequestMapping(path = {
            Constants.API_ROLE_OWNER + "/tickets/history/client/{id}",
            Constants.API_ROLE_STAFF + "/tickets/history/client/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<Iterable<TicketDto>> getAllTicketsForClientAccount(@PathVariable("id") long id,
                                                                             HttpServletRequest request) {
        Collection<TicketDto> tickets = ticketDao.findAllByClientAccountId(id);
        return ResponseEntity.ok(tickets);
    }

    @RequestMapping(path = {
            Constants.API_ROLE_OWNER + "/tickets/{id}/ticketProducts",
            Constants.API_ROLE_STAFF + "/tickets/{id}/ticketProducts"}, method = RequestMethod.GET)
    public ResponseEntity<Iterable<TicketProductDto>> getAllTicketProductsForTicket(@PathVariable("id") long id,
                                                                                    HttpServletRequest request) {
        Collection<TicketProductDto> ticketProductDtos = new ArrayList<TicketProductDto>() {{
            Collection<TicketProduct> ticketProducts = ticketProductDao.findByticketId(id);
            if (ticketProducts != null) ticketProducts.forEach(
                    ticketProduct -> add(new TicketProductDto(ticketProduct)));
        }};
        return ResponseEntity.ok(ticketProductDtos);
    }
}
