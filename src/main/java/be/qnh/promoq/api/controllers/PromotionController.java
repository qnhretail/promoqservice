package be.qnh.promoq.api.controllers;

import be.qnh.promoq.api.dataTransferObjects.AmountDto;
import be.qnh.promoq.api.dataTransferObjects.CreditTransferDto;
import be.qnh.promoq.api.dataTransferObjects.PointTransferDto;
import be.qnh.promoq.api.dataTransferObjects.PromotionDto;
import be.qnh.promoq.main.Constants;
import be.qnh.promoq.main.dataAccessObjects.ClientAccountPromotionDaoImpl;
import be.qnh.promoq.main.dataAccessObjects.FirmDaoImpl;
import be.qnh.promoq.main.dataAccessObjects.PromotionDao;
import be.qnh.promoq.main.dataAccessObjects.TransferDaoImpl;
import be.qnh.promoq.main.models.ClientAccount;
import be.qnh.promoq.main.models.Firm;
import be.qnh.promoq.main.models.PointTransfer;
import be.qnh.promoq.main.models.Promotion;
import be.qnh.promoq.security.dataAccessObjects.AccountDaoImpl;
import be.qnh.promoq.security.models.Account;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collection;

import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@CrossOrigin
@RequestMapping(Constants.API_ROOT)
@Api(value = "promotion management")
public class PromotionController {
    @Autowired
    private FirmDaoImpl firmDao;
    @Autowired
    private RequestHelper requestHelper;
    @Autowired
    private PromotionDao promotionDao;
    @Autowired
    private ClientAccountPromotionDaoImpl clientAccountPromotionDao;
    @Autowired
    private AccountDaoImpl accountDao;
    @Autowired
    private TransferDaoImpl transferDao;

    @RequestMapping(path = {
            Constants.API_ROLE_OWNER + "/promotions/{id}",
            Constants.API_ROLE_CLIENT + "/promotions/{id}",
            Constants.API_ROLE_STAFF + "/promotions/{id}"
    }, method = GET)
    public ResponseEntity<PromotionDto> getPromotionById(@PathVariable long id) {
        Promotion promotion = promotionDao.findById(id);
        return ResponseEntity.ok(new PromotionDto(promotion));
    }

    @RequestMapping(path = {
            Constants.API_ROLE_OWNER + "/promotions/firm/{id}",
            Constants.API_ROLE_CLIENT + "/promotions/firm/{id}",
            Constants.API_ROLE_STAFF + "/promotions/firm/{id}"
    }, method = GET)
    public ResponseEntity<Iterable<PromotionDto>> getAllPromotionsForFirm(
            @PathVariable long id, @RequestParam(value = "active",
            required = false, defaultValue = "false") boolean active,
            HttpServletRequest request
    ) {
        Firm firm = firmDao.findById(id);
        if (!requestHelper.getReqAccountFirms(request).contains(firm)
                && !request.isUserInRole(Constants.ROLE_CLIENT)) {
            return ResponseEntity.status(FORBIDDEN).body(null);
        }
        Collection<PromotionDto> result = new ArrayList<>();
        promotionDao.findByFirm(firm, active).forEach(p -> result.add(new PromotionDto(p)));
        return ResponseEntity.ok(result);
    }

    @RequestMapping(path = {
            Constants.API_ROLE_OWNER + "/promotions",
            Constants.API_ROLE_STAFF + "/promotions"
    }, method = POST)
    public ResponseEntity<PromotionDto> addPromotion(
            @RequestBody PromotionDto promotion, HttpServletRequest request
    ) {
        Firm firm = firmDao.findById(promotion.getFirmId());
        if (!requestHelper.getReqAccountFirms(request).contains(firm)) {
            return ResponseEntity.status(FORBIDDEN).body(null);
        }
        Promotion newPromotion = promotionDao.save(promotion);
        return ResponseEntity.ok(new PromotionDto(newPromotion));
    }

    @RequestMapping(path = {
            Constants.API_ROLE_CLIENT + "/promotions/points/{id}"
    }, method = GET)
    public ResponseEntity<AmountDto> getPromotionPointsForLoggedInClient(@PathVariable long id,
                                                                         HttpServletRequest request) {
        ClientAccount clientAccount = (ClientAccount) requestHelper.getReqAccount(request);
        return ResponseEntity.ok(
                clientAccountPromotionDao.getClientAccountPromotionPoints(clientAccount.getId(), id));
    }

    @RequestMapping(path = {
            Constants.API_ROLE_OWNER + "/promotions/points/{promotionId}/{clientAccountId}",
            Constants.API_ROLE_STAFF + "/promotions/points/{promotionId}/{clientAccountId}",
            Constants.API_ROLE_CLIENT + "/promotions/points/{promotionId}/{clientAccountId}"
    }, method = GET)
    public ResponseEntity<AmountDto> getPromotionPointsForClient(@PathVariable long promotionId,
                                                                 @PathVariable long clientAccountId,
                                                                 HttpServletRequest request) {
        Promotion promotion = promotionDao.findById(promotionId);
        if (requestHelper.isRequestAccountClientWithId(clientAccountId, request) ||
                requestHelper.getReqAccountFirms(request).contains(promotion.getFirm())) {
            return ResponseEntity.ok(
                    clientAccountPromotionDao.getClientAccountPromotionPoints(clientAccountId, promotionId));
        }
        return ResponseEntity.status(FORBIDDEN).body(null);
    }

    @RequestMapping(path = {
            Constants.API_ROLE_OWNER + "/promotions/points/{promotionId}/{clientAccountId}",
            Constants.API_ROLE_STAFF + "/promotions/points/{promotionId}/{clientAccountId}"
    }, method = POST)
    public ResponseEntity<AmountDto> addPromotionPointsToClientAccount(@PathVariable long promotionId,
                                                                       @PathVariable long clientAccountId,
                                                                       HttpServletRequest request,
                                                                       @RequestBody AmountDto amountDto) {
        Promotion promotion = promotionDao.findById(promotionId);
        if (requestHelper.getReqAccountFirms(request).contains(promotion.getFirm())) {
            ClientAccount clientAccount = accountDao.findClientAccountById(clientAccountId);
            long amount = amountDto.getAmount();
            registerPointTransfer(promotion, requestHelper.getReqAccount(request), clientAccount,
                    amountDto.getAmount(), amountDto.getReason());
            Long totalPoints = clientAccountPromotionDao.addClientAccountPromotionPoints(clientAccount, promotion, amount);
            return ResponseEntity.ok(new AmountDto(totalPoints, amountDto.getReason()));
        }
        return ResponseEntity.status(FORBIDDEN).body(null);
    }

    @RequestMapping(path = {
            Constants.API_ROLE_OWNER + "/promotions/pointtransfers/client/{clientAccountId}",
            Constants.API_ROLE_STAFF + "/promotions/pointtransfers/client/{clientAccountId}",
            Constants.API_ROLE_CLIENT + "/promotions/pointtransfers/client/{clientAccountId}"
    }, method = GET)
    public ResponseEntity<Collection<PointTransferDto>> getPointTransferHistoryForClient(@PathVariable long clientAccountId,
                                                                                         HttpServletRequest request) {
        Collection<Firm> firms = requestHelper.getReqAccountFirms(request);
        Collection<PointTransferDto> pointTransfers = new ArrayList<PointTransferDto>() {{
            transferDao.findPointTransfersByClientAccountId(clientAccountId).forEach(pointTransfer -> {
                if (firms.contains(pointTransfer.getPromotion().getFirm())) add(new PointTransferDto(pointTransfer));
            });
        }};
        return ResponseEntity.ok(pointTransfers);
    }

    @RequestMapping(path = {
            Constants.API_ROLE_OWNER + "/promotions/credittransfers/client/{clientAccountId}",
            Constants.API_ROLE_STAFF + "/promotions/credittransfers/client/{clientAccountId}",
            Constants.API_ROLE_CLIENT + "/promotions/credittransfers/client/{clientAccountId}"
    }, method = GET)
    public ResponseEntity<Collection<CreditTransferDto>> getCreditTransferHistoryForClient(@PathVariable long clientAccountId,
                                                                                           HttpServletRequest request) {
        Collection<Firm> firms = requestHelper.getReqAccountFirms(request);
        Collection<CreditTransferDto> creditTransfers = new ArrayList<CreditTransferDto>() {{
            transferDao.findCreditTransfersByClientAccountId(clientAccountId).forEach(creditTransfer -> {
                if (firms.contains(creditTransfer.getFirm())) add(new CreditTransferDto(creditTransfer));
            });
        }};
        return ResponseEntity.ok(creditTransfers);
    }
    private void registerPointTransfer(Promotion promotion, Account reqAccount, ClientAccount clientAccount, long amount, String reason) {
        PointTransfer pointTransfer = new PointTransfer(promotion, reqAccount, clientAccount, amount, reason);
        transferDao.save(pointTransfer);
    }
}
