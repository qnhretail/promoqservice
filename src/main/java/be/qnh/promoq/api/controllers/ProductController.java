package be.qnh.promoq.api.controllers;

import be.qnh.promoq.api.controllers.helpers.ProductControllerHelper;
import be.qnh.promoq.api.dataTransferObjects.ProductDto;
import be.qnh.promoq.main.Constants;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@CrossOrigin
@RequestMapping(Constants.API_ROOT)
@Api(value = "Products manager")
public class ProductController {
    @Autowired
    private ProductControllerHelper productControllerHelper;

    @RequestMapping(path = {
            Constants.API_ROLE_OWNER + "/products/firm/{id}",
            Constants.API_ROLE_STAFF + "/products/firm/{id}"
    }, method = GET)
    public ResponseEntity<Collection<ProductDto>> getAllProductsForFirm(
            @PathVariable long id, HttpServletRequest request) {
        return productControllerHelper.getAllProductsForFirm(id, request);
    }

    @RequestMapping(path = {
            Constants.API_ROLE_OWNER + "/products/",
            Constants.API_ROLE_STAFF + "/products/"
    }, method = POST)
    public ResponseEntity<ProductDto> addProduct(
            @RequestBody ProductDto productDto,
            HttpServletRequest request
    ) {
        return productControllerHelper.updateProduct(productDto, true, request);
    }

    @RequestMapping(path = {
            Constants.API_ROLE_OWNER + "/products/update/",
            Constants.API_ROLE_STAFF + "/products/update/"
    }, method = POST)
    public ResponseEntity<ProductDto> updateProduct(
            @RequestBody ProductDto productDto,
            HttpServletRequest request
    ) {
        return productControllerHelper.updateProduct(productDto, false, request);
    }
}
