package be.qnh.promoq.api.controllers.helpers;

import be.qnh.promoq.api.dataTransferObjects.ClientAccountFirmDto;
import be.qnh.promoq.main.models.ClientAccount;
import be.qnh.promoq.main.models.ClientAccountFirm;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ClientAccountFirmResponseHelper {
    public static ResponseEntity<ClientAccountFirmDto> getClientAccountFirmDtoForFirmId(
            ClientAccount clientAccount, long firmId) {
        for (ClientAccountFirm clientAccountFirm : clientAccount.getClientAccountsFirms()) {
            if (clientAccountFirm.getFirm().getId() == firmId)
                return ResponseEntity.ok(new ClientAccountFirmDto(clientAccountFirm));
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    }
}
