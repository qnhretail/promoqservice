package be.qnh.promoq.api.controllers;

import be.qnh.promoq.api.dataTransferObjects.BranchDto;
import be.qnh.promoq.main.Constants;
import be.qnh.promoq.main.dataAccessObjects.BranchDaoImpl;
import be.qnh.promoq.main.dataAccessObjects.FirmDaoImpl;
import be.qnh.promoq.main.models.Branch;
import be.qnh.promoq.main.models.Firm;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@CrossOrigin
@RequestMapping(Constants.API_ROOT)
@Api(value = "Branch management", description = "api for firm management")
public class Branchcontroller {
    @Autowired
    private BranchDaoImpl branchDao;
    @Autowired
    private RequestHelper requestHelper;
    @Autowired
    private FirmDaoImpl firmDao;

    @RequestMapping(path = {
            Constants.API_ROLE_OWNER + "/branches/",
            Constants.API_ROLE_STAFF + "/branches/"}, method = POST)
    public ResponseEntity<BranchDto> addBranch(@RequestBody BranchDto branch, HttpServletRequest request) {
        Firm firm = firmDao.findById(branch.getFirmId());
        if (!requestHelper.getReqAccountFirms(request).contains(firm))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
        Branch newBranch = branchDao.save(branch, firm);
        return ResponseEntity.ok(new BranchDto(newBranch));
    }

    @RequestMapping(path = {
            Constants.API_ROLE_OWNER + "/branches/{id}",
            Constants.API_ROLE_CLIENT + "/branches/{id}",
            Constants.API_ROLE_STAFF + "/branches/{id}"}, method = GET)
    public ResponseEntity<BranchDto> getBranchById(@PathVariable long id, HttpServletRequest request) {
        Branch branch = branchDao.findById(id);
        return ResponseEntity.ok(new BranchDto(branch));
    }

    @RequestMapping(path = {
            Constants.API_ROLE_OWNER + "/branches/{id}"}, method = DELETE)
    public ResponseEntity<?> deleteBranchById(
            @PathVariable long id, HttpServletRequest request,
            @RequestParam(value = "force", required = false) boolean force) {
        Branch branch = branchDao.findById(id);
        Firm firm = branch.getFirm();
        if (!requestHelper.getReqAccountFirms(request).contains(firm))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
        branchDao.deactivate(branch);
        return ResponseEntity.ok(Constants.BRANCH_ + id + Constants._SUCCESSFULLY_DELETED);

    }

    @RequestMapping(path = {
            Constants.API_ROLE_OWNER + "/branches/firm/{id}",
            Constants.API_ROLE_STAFF + "/branches/firm/{id}"}, method = GET)
    public ResponseEntity<?> getAllBranchesForFirm(@PathVariable long id, HttpServletRequest request) {
        Firm firm = firmDao.findById(id);
        if (!requestHelper.getReqAccountFirms(request).contains(firm))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
        List<BranchDto> result = new ArrayList<BranchDto>();
        branchDao.findByFirm(firm).forEach(b -> result.add(new BranchDto(b)));
        return ResponseEntity.ok(result);
    }
}
