package be.qnh.promoq.api.controllers;

import be.qnh.promoq.api.dataTransferObjects.AmountDto;
import be.qnh.promoq.api.dataTransferObjects.GiftArticleDto;
import be.qnh.promoq.main.Constants;
import be.qnh.promoq.main.dataAccessObjects.ClientAccountGiftArticleDaoImpl;
import be.qnh.promoq.main.dataAccessObjects.FirmDaoImpl;
import be.qnh.promoq.main.dataAccessObjects.GiftArticleDaoImpl;
import be.qnh.promoq.main.dataAccessObjects.PromotionDao;
import be.qnh.promoq.main.models.*;
import be.qnh.promoq.security.dataAccessObjects.AccountDaoImpl;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@CrossOrigin
@RequestMapping(Constants.API_ROOT)
@Api(value = "GiftArticle manager")
public class GiftArticleController {
    @Autowired
    private FirmDaoImpl firmDao;
    @Autowired
    private RequestHelper requestHelper;
    @Autowired
    private GiftArticleDaoImpl giftArticleDaoImpl;
    @Autowired
    private ClientAccountGiftArticleDaoImpl clientAccountGiftArticleDao;
    @Autowired
    private PromotionDao promotionDao;
    @Autowired
    private AccountDaoImpl accountDao;

    @RequestMapping(path = {
            Constants.API_ROLE_OWNER + "/giftarticles/firm/{id}",
            Constants.API_ROLE_STAFF + "/giftarticles/firm/{id}"
    }, method = GET)
    public ResponseEntity<List<GiftArticleDto>> getAllGiftArticlesForFirm(
            @PathVariable long id, HttpServletRequest request
    ) {
        Firm firm = firmDao.findById(id);
        if (!requestHelper.getReqAccountFirms(request).contains(firm)) {
            return ResponseEntity.status(FORBIDDEN).body(null);
        }
        List<GiftArticleDto> result = new ArrayList<>();
        giftArticleDaoImpl.findByFirm(firm).forEach(giftArticle -> result.add(new GiftArticleDto(giftArticle)));
        return ResponseEntity.ok(result);
    }

    @RequestMapping(path = {
            Constants.API_ROLE_OWNER + "/giftarticles",
            Constants.API_ROLE_STAFF + "/giftarticles"
    }, method = POST)
    public ResponseEntity<GiftArticleDto> addGiftArticle(
            @RequestBody GiftArticleDto giftArticle, HttpServletRequest request
    ) {
        Firm firm = firmDao.findById(giftArticle.getFirmId());
        if (!firm.getAssignedToCreatedByAssignedBy().contains(requestHelper.getReqAccount(request))) {
            return ResponseEntity.status(FORBIDDEN).body(null);
        }
        GiftArticleDto newGiftArticle = giftArticleDaoImpl.save(giftArticle);
        return ResponseEntity.ok(newGiftArticle);
    }

    @RequestMapping(path = {
            Constants.API_ROLE_CLIENT + "/giftarticles/amount/{id}"
    }, method = GET)
    public ResponseEntity<AmountDto> getGiftsToCollectForLoggedInClient(@PathVariable long id,
                                                                        HttpServletRequest request) {
        GiftArticle giftArticle = ((ValuePromotionWithGiftArticle) promotionDao.findById(id)).getGiftArticle();
        ClientAccount clientAccount = (ClientAccount) requestHelper.getReqAccount(request);
        return ResponseEntity.ok(
                clientAccountGiftArticleDao.getClientAccountGiftArticleAmount(clientAccount, giftArticle));
    }

    @RequestMapping(path = {
            Constants.API_ROLE_OWNER + "/giftarticles/amount/{promotionId}/{clientAccountId}",
            Constants.API_ROLE_STAFF + "/giftarticles/amount/{promotionId}/{clientAccountId}"
    }, method = GET)
    public ResponseEntity<AmountDto> getGiftsToCollectForClientAccount(@PathVariable long promotionId,
                                                                       @PathVariable long clientAccountId,
                                                                       HttpServletRequest request) {
        Promotion promotion = promotionDao.findById(promotionId);
        if (promotion instanceof ValuePromotionWithGiftArticle) {
            GiftArticle giftArticle = ((ValuePromotionWithGiftArticle) promotionDao.findById(promotionId)).getGiftArticle();
            ClientAccount clientAccount = accountDao.findClientAccountById(clientAccountId);
            return ResponseEntity.ok(
                    clientAccountGiftArticleDao.getClientAccountGiftArticleAmount(clientAccount, giftArticle));
        }
        return ResponseEntity.status(BAD_REQUEST).body(null);

    }
}
