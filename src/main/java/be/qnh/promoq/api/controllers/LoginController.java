package be.qnh.promoq.api.controllers;

import be.qnh.promoq.api.dataTransferObjects.LoginDto;
import be.qnh.promoq.security.dataAccessObjects.AccountDaoImpl;
import be.qnh.promoq.security.models.Role;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collection;

@Controller
@RestController
@CrossOrigin
@RequestMapping("/api/session")
@Api(value = "Session Management")
public class LoginController {
    @Autowired
    AccountDaoImpl accountDao;
    @Autowired
    AuthenticationManager authenticationManager;

    @RequestMapping(path = "login", method = RequestMethod.POST)
    public ResponseEntity<LoginDto> login(HttpServletRequest request, HttpServletResponse response)
            throws ServletException {
        String username = "none";
        if (request.getUserPrincipal() == null) {
            Authentication authenticationResult =
                    authenticationManager.authenticate(
                            new UsernamePasswordAuthenticationToken(username, username));
            SecurityContextHolder.getContext().setAuthentication(authenticationResult);
        } else username = request.getUserPrincipal().getName();
        Collection<Role> roles = accountDao.findByUsername(username).getRoles();
        return new ResponseEntity<LoginDto>(
                new LoginDto(username, roles)
                , HttpStatus.OK);
    }
}
