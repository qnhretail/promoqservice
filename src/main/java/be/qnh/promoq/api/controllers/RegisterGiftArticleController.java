package be.qnh.promoq.api.controllers;

import be.qnh.promoq.api.dataTransferObjects.ClientAccountGiftArticleDto;
import be.qnh.promoq.api.dataTransferObjects.TicketDto;
import be.qnh.promoq.api.dataTransferObjects.ValidateGiftArticleDto;
import be.qnh.promoq.main.Constants;
import be.qnh.promoq.main.dataAccessObjects.ClientAccountGiftArticleDaoImpl;
import be.qnh.promoq.main.dataAccessObjects.ClientDaoImpl;
import be.qnh.promoq.main.dataAccessObjects.TicketDaoImpl;
import be.qnh.promoq.main.enums.TicketStatus;
import be.qnh.promoq.main.models.*;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.Date;

@RestController
@CrossOrigin
@Component
@RequestMapping({
        Constants.API_ROOT + Constants.API_ROLE_REGISTER + "/promotionarticle",
        Constants.API_ROOT + Constants.API_ROLE_ADMIN + "/promotionarticle",
        Constants.API_ROOT + Constants.API_ROLE_OWNER + "/promotionarticle"
})
@Api(value = "Register promotion validation")
public class RegisterGiftArticleController {

    @Autowired
    private ClientAccountGiftArticleDaoImpl clientAccountGiftArticleDao;
    @Autowired
    private ClientDaoImpl clientDao;
    @Autowired
    private TicketDaoImpl ticketDao;
    @Autowired
    private RequestHelper requestHelper;

    @PostMapping()
    public ResponseEntity<TicketDto> validateGiftArticle(@RequestBody ValidateGiftArticleDto validateGiftArticleDto,
                                                         HttpServletRequest request) {
        RegisterAccount registerAccount = (RegisterAccount) requestHelper.getReqAccount(request);
        Client client = clientDao.findByCardId(validateGiftArticleDto.getClientCardNr());
        ClientAccountGiftArticle clientAccountGiftArticle = clientAccountGiftArticleDao
                .findByClientAccountAndGiftArticle(client.getAccount(), validateGiftArticleDto.getId());
        clientAccountGiftArticle
                .setQuantity(clientAccountGiftArticle.getQuantity() - validateGiftArticleDto.getQuantity());
        clientAccountGiftArticleDao.save(clientAccountGiftArticle);
        Ticket ticket = ticketDao.save(new Ticket(
                registerAccount,
                client,
                validateGiftArticleDto.getStaffMember(),
                -1,
                new Date(),
                null,
                null,
                TicketStatus.S60
        ));
        return ResponseEntity.ok(ticketDao.ticketToDto(ticket));

    }

    @RequestMapping(path =
            "{cardId}/giftarticles",
            method = RequestMethod.GET)
    public ResponseEntity<ClientAccountGiftArticleDto> getGiftArticles(@PathVariable String cardId, HttpServletRequest request) {
        RegisterAccount registerAccount = (RegisterAccount) requestHelper.getReqAccount(request);
        ClientAccount clientAccount = clientDao.findByCardId(cardId).getAccount();
        Collection<ClientAccountGiftArticle> clientAccountGiftArticles = clientAccountGiftArticleDao.findByClientAccount(clientAccount);
        return ResponseEntity.ok(null);
    }
}
