package be.qnh.promoq.api.controllers;

import be.qnh.promoq.api.controllers.helpers.ClientAccountFirmResponseHelper;
import be.qnh.promoq.api.dataTransferObjects.ClientAccountFirmDto;
import be.qnh.promoq.api.dataTransferObjects.ClientDto;
import be.qnh.promoq.api.dataTransferObjects.ClientListDto;
import be.qnh.promoq.api.dataTransferObjects.DecimalAmountDto;
import be.qnh.promoq.main.Constants;
import be.qnh.promoq.main.dataAccessObjects.ClientAccountFirmDaoImpl;
import be.qnh.promoq.main.dataAccessObjects.ClientDaoImpl;
import be.qnh.promoq.main.dataAccessObjects.FirmDaoImpl;
import be.qnh.promoq.main.dataAccessObjects.TransferDao;
import be.qnh.promoq.main.models.*;
import be.qnh.promoq.security.dataAccessObjects.AccountDaoImpl;
import be.qnh.promoq.security.models.Account;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@CrossOrigin
@RequestMapping(Constants.API_ROOT)
@Api(value = "clients management")
public class ClientController {
    @Autowired
    ClientDaoImpl clientDao;
    @Autowired
    FirmDaoImpl firmDao;
    @Autowired
    AccountDaoImpl accountDao;
    @Autowired
    RequestHelper requestHelper;
    @Autowired
    ClientAccountFirmDaoImpl clientAccountFirmDao;
    @Autowired
    private TransferDao transferDao;

    @RequestMapping(path = {
            Constants.API_ROLE_OWNER + "/clients/"
    }, method = GET)
    public ResponseEntity<Collection<ClientDto>> getAllClients(HttpServletRequest request) {
        List<Client> clients = new ArrayList<Client>();
        Account owner = requestHelper.getReqAccount(request);
        firmDao.getAllCreatedByOrAssignedByOrAssignedTo(owner)
                .forEach(firm -> accountDao.findAllClientAccountsByFirm(firm).forEach(account ->
                        clients.addAll((Collection<? extends Client>) clientDao.findByAccount(account))));
        return ResponseEntity.ok(new ClientListDto(clients).getClients());
    }

    @RequestMapping(path = {
            Constants.API_ROLE_OWNER + "/clients/{ClientCardId}",
            Constants.API_ROLE_STAFF + "/clients/{ClientCardId}"
    }, method = GET)
    public ResponseEntity<ClientDto> getClientByClientCardId(@PathVariable("ClientCardId") String clientCardId,
                                                             HttpServletRequest request) {
        return ResponseEntity.ok(new ClientDto(clientDao.findByCardId(clientCardId)));
    }

    @RequestMapping(path = {
            Constants.API_ROLE_CLIENT + "/clients/clientAccount/{id}",
            Constants.API_ROLE_OWNER + "/clients/clientAccount/{id}",
            Constants.API_ROLE_STAFF + "/clients/clientAccount/{id}"
    }, method = GET)
    public ResponseEntity<Collection<ClientDto>> getAllClientsForClientAccount(@PathVariable("id") long clientAccountId,
                                                                               HttpServletRequest request) {
        ClientAccount clientAccount = accountDao.findClientAccountById(clientAccountId);
        if (request.isUserInRole(Constants.ROLE_CLIENT)) {
            ClientAccount requestAccount = (ClientAccount) requestHelper.getReqAccount(request);
            if (requestAccount != clientAccount) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
            }
            return ResponseEntity.ok(new ClientListDto(clientDao.findByAccount(clientAccount)).getClients());
        } else {
            Collection<Firm> requestAccountFirms = requestHelper.getReqAccountFirms(request);
            if (clientAccount.getFirms().stream().anyMatch(firm -> requestAccountFirms.contains(firm))) {
                return ResponseEntity.ok(new ClientListDto(clientDao.findByAccount(clientAccount)).getClients());
            }
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
        }
    }

    @RequestMapping(path = {
            Constants.API_ROLE_OWNER + "/clients/clientAccountFirms/",
            Constants.API_ROLE_STAFF + "/clients/clientAccountFirms/",
            Constants.API_ROLE_CLIENT + "/clients/clientAccountFirms"
    }, method = POST)
    public ResponseEntity<ClientAccountFirmDto> postClientAccountFirm(@RequestBody ClientAccountFirmDto clientAccountFirmDto,
                                                                      HttpServletRequest request) {
        Firm firm = firmDao.findById(clientAccountFirmDto.getFirmId());
        if (requestHelper.isRequestAccountClientWithId(clientAccountFirmDto.getClientId(), request) ||
                requestHelper.getReqAccountFirms(request).contains(firm)) {
            return ResponseEntity.ok(new ClientAccountFirmDto(clientAccountFirmDao.save(clientAccountFirmDto)));
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
    }

    @RequestMapping(path = {
            Constants.API_ROLE_OWNER + "/clients/clientAccountFirms/addCredit/{clientAccountId}/{firmId}",
            Constants.API_ROLE_STAFF + "/clients/clientAccountFirms/addCredit/{clientAccountId}/{firmId}",
    }, method = POST)
    public ResponseEntity<ClientAccountFirmDto> addCreditToClientAccountFirm(
            @RequestBody DecimalAmountDto amountDto,
            @PathVariable("clientAccountId") long clientAccountId,
            @PathVariable("firmId") long firmId,
            HttpServletRequest request) {
        Firm firm = firmDao.findById(firmId);
        ClientAccountFirm clientAccountFirm = clientAccountFirmDao.findByClientAccountIdAndFirmId(clientAccountId, firmId);
        if (requestHelper.getReqAccountFirms(request).contains(firm)) {
            clientAccountFirm.setCredit(clientAccountFirm.getCredit() + amountDto.getAmount());
            Transfer transfer = new CreditTransfer(
                    requestHelper.getReqAccount(request),
                    clientAccountFirm.getClientAccount(),
                    clientAccountFirm.getFirm(),
                    amountDto.getAmount(),
                    amountDto.getReason());
            transferDao.save(transfer);
            return ResponseEntity.ok(new ClientAccountFirmDto(clientAccountFirmDao.save(clientAccountFirm)));
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
    }

    @RequestMapping(path = {
            Constants.API_ROLE_CLIENT + "/clients/clientAccountFirms/{id}",
            Constants.API_ROLE_OWNER + "/clients/clientAccountFirms/{id}",
            Constants.API_ROLE_STAFF + "/clients/clientAccountFirms/{id}"
    }, method = GET)
    public ResponseEntity<Collection<ClientAccountFirmDto>> getClientAccountFirmsForClientAccount(
            @PathVariable("id") long clientAccountId, HttpServletRequest request) {
        ClientAccount clientAccount = accountDao.findClientAccountById(clientAccountId);
        if (request.isUserInRole(Constants.ROLE_CLIENT)) {
            ClientAccount requestAccount = (ClientAccount) requestHelper.getReqAccount(request);
            if (requestAccount != clientAccount) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
            }
            return ResponseEntity.ok(new ArrayList<ClientAccountFirmDto>() {{
                clientAccount.getClientAccountsFirms().forEach(clientAccountFirm ->
                        add(new ClientAccountFirmDto(clientAccountFirm)));
            }});

        } else {
            return ResponseEntity.ok(new ArrayList<ClientAccountFirmDto>() {{
                clientAccount.getClientAccountsFirms().forEach(clientAccountFirm -> {
                    add(new ClientAccountFirmDto(clientAccountFirm));
                });
            }});

        }
    }

    @RequestMapping(path = {
            Constants.API_ROLE_CLIENT + "/clients/clientAccountFirms/{clientAccountId}/{firmId}",
            Constants.API_ROLE_OWNER + "/clients/clientAccountFirms/{clientAccountId}/{firmId}",
            Constants.API_ROLE_STAFF + "/clients/clientAccountFirms/{clientAccountId}/{firmId}"
    }, method = GET)
    public ResponseEntity<ClientAccountFirmDto> getClientAccountFirm(
            @PathVariable("clientAccountId") long clientAccountId,
            @PathVariable("firmId") long firmId,
            HttpServletRequest request) {
        ClientAccount clientAccount = accountDao.findClientAccountById(clientAccountId);
        if (request.isUserInRole(Constants.ROLE_CLIENT)) {
            ClientAccount requestAccount = (ClientAccount) requestHelper.getReqAccount(request);
            if (requestAccount != clientAccount) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
            }
        }

        return ClientAccountFirmResponseHelper.getClientAccountFirmDtoForFirmId(clientAccount, firmId);
    }

    @RequestMapping(path = {
            Constants.API_ROLE_CLIENT + "/clients/clientAccountFirms/loggedInClient/{firmId}"
    }, method = GET)
    public ResponseEntity<ClientAccountFirmDto> getClientAccountFirmForLoggedInClientAndForFirmId(
            @PathVariable long firmId, HttpServletRequest request
    ) {
        ClientAccount clientAccount = (ClientAccount) requestHelper.getReqAccount(request);
        return ClientAccountFirmResponseHelper.getClientAccountFirmDtoForFirmId(clientAccount, firmId);
    }

    @RequestMapping(path = {
            Constants.API_ROLE_OWNER + "/clients/{id}",
            Constants.API_ROLE_STAFF + "/clients/{id}"
    }, method = DELETE)
    public ResponseEntity<?> deleteClientFromAccount(@PathVariable("id") long clientId,
                                                     HttpServletRequest request) {
        Collection<Firm> reqAccountFirms = requestHelper.getReqAccountFirms(request);
        Client client = clientDao.findById(clientId);
        if (client.getAccount().getFirms().stream().anyMatch(
                reqAccountFirms::contains)) {
            client.setAccount(null);
            clientDao.save(client);
            return ResponseEntity.ok(null);
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
    }

    @RequestMapping(path = {
            Constants.API_ROLE_OWNER + "/clients/",
            Constants.API_ROLE_CLIENT + "/clients/",
            Constants.API_ROLE_STAFF + "/clients/"
    }, method = POST)
    public ResponseEntity<ClientDto> addClient(@RequestBody ClientDto clientDto,
                                               HttpServletRequest request) {
        if (requestHelper.isRequestAccountClientWithId(clientDto.getAccountId(), request)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
        }

        Client existing = clientDao.findByCardId(clientDto.getCardId());
        if (existing != null) {
            long accountId = clientDto.getAccountId();
            if (existing.getAccount() != null) {
                if (existing.getAccount().getId() == accountId)
                    return ResponseEntity.ok().body(null);
                return ResponseEntity.badRequest().body(null);
            } else {
                existing.setAccount((ClientAccount) accountDao.findById(accountId));
                return ResponseEntity.ok(new ClientDto(clientDao.save(existing)));
            }
        }
        Client client = clientDao.save(clientDto);
        return ResponseEntity.ok(new ClientDto(client));
    }
}
