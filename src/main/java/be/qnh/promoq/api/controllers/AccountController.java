package be.qnh.promoq.api.controllers;

import be.qnh.promoq.api.dataTransferObjects.*;
import be.qnh.promoq.mail.EmailService;
import be.qnh.promoq.main.Constants;
import be.qnh.promoq.main.PasswordUtils;
import be.qnh.promoq.main.dataAccessObjects.BranchDaoImpl;
import be.qnh.promoq.main.dataAccessObjects.ClientAccountFirmDaoImpl;
import be.qnh.promoq.main.dataAccessObjects.ClientDaoImpl;
import be.qnh.promoq.main.dataAccessObjects.FirmDaoImpl;
import be.qnh.promoq.main.models.*;
import be.qnh.promoq.security.config.CustomPasswordEncoder;
import be.qnh.promoq.security.dataAccessObjects.AccountDaoImpl;
import be.qnh.promoq.security.models.Account;
import io.swagger.annotations.Api;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@Controller
@RestController
@CrossOrigin
@RequestMapping(Constants.API_ROOT)
@Api(value = "account management")
public class AccountController {
    @Autowired
    private RequestHelper requestHelper;
    @Autowired
    private AccountDaoImpl accountDao;
    @Autowired
    private BranchDaoImpl branchDao;
    @Autowired
    private FirmDaoImpl firmDao;
    @Autowired
    private ClientAccountFirmDaoImpl clientAccountsFirmsDao;
    @Autowired
    private EmailService emailService;
    @Autowired
    private ClientDaoImpl clientDao;
    @Autowired
    private CustomPasswordEncoder passwordEncoder;

    @RequestMapping(path = Constants.API_ROLE_ROOT + "/accounts/{role}", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Account>> getAccountsWithRole(@PathVariable("role") String role) {
        return ResponseEntity.ok(accountDao.findAllByRole(role));
    }

    @RequestMapping(path = {
            Constants.API_ROLE_OWNER + "/accounts/client/",
            Constants.API_ROLE_STAFF + "/accounts/client/"}, method = GET)
    public ResponseEntity<Iterable<ClientAccountDto>> getAllClientAccountsForOwner(HttpServletRequest request) {
        Collection<ClientAccount> clientAccounts = new ArrayList<>();
        if (request.isUserInRole(Constants.ROLE_OWNER)) {
            OwnerAccount ownerAccount = (OwnerAccount) requestHelper.getReqAccount(request);
            ownerAccount.getFirms().forEach(firm ->
                    clientAccounts.addAll(accountDao.findAllClientAccountsByFirm(firm)));
        } else {
            StaffAccount staffAccount = (StaffAccount) requestHelper.getReqAccount(request);
            clientAccounts.addAll(accountDao.findAllClientAccountsByFirm(staffAccount.getFirm()));
        }
        return ResponseEntity.ok(new ClientAccountListDto(clientAccounts).getClientAccounts());
    }

    @RequestMapping(path = {Constants.API_ROLE_CLIENT + "/accounts/client/"}, method = GET)
    public ResponseEntity<ClientAccountDto> getLoggedInClientAccount(HttpServletRequest request) {
        return ResponseEntity.ok(new ClientAccountDto(
                (ClientAccount) requestHelper.getReqAccount(request)
        ));
    }

    @RequestMapping(path = {
            Constants.API_ROLE_OWNER + "/accounts/client/update",
            Constants.API_ROLE_STAFF + "/accounts/client/update"}, method = POST)
    public ResponseEntity<Object> updateClientAccount(@RequestBody ClientAccountDto clientAccountDto,
                                                      HttpServletRequest request) {
        Collection<Firm> reqAccountFirms = requestHelper.getReqAccountFirms(request);
        Collection<Long> clientAccountFirmIds = new ArrayList<Long>() {{
            clientAccountsFirmsDao.findByClientAccountId(clientAccountDto.getId()).forEach(
                    clientAccountFirm -> add(clientAccountFirm.getFirm().getId()));
        }};
        if (!checkClientAccountFirmsFromRequestAccount(clientAccountFirmIds, reqAccountFirms)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
        }
        if (doClientAccountsConflict(clientAccountDto,
                (ClientAccount) accountDao.findByUsername(clientAccountDto.getUsername()))) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(
                    new Object() {
                        public String msg = Constants.API_ERROR_DUPLICATE_USERNAME;
                    });
        }
        if (doClientAccountsConflict(clientAccountDto, accountDao.findByEmail(clientAccountDto.getEmail()))) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(
                    new Object() {
                        public String msg = Constants.API_ERROR_DUPLICATE_EMAIL;
                    });
        }
        return ResponseEntity.ok(new ClientAccountDto(accountDao.update(clientAccountDto)));
    }

    private Boolean doClientAccountsConflict(ClientAccountDto account1, ClientAccount account2) {
        if (account1 != null && account2 != null) {
            if (account1.getId() != account2.getId())
                return true;
        }
        return false;
    }

    @RequestMapping(path = {
            Constants.API_ROLE_OWNER + "/accounts/client/",
            Constants.API_ROLE_STAFF + "/accounts/client/"}, method = POST)
    public ResponseEntity<ClientAccountDto> addClientAccount(@RequestBody ClientAccountDto clientAccountDto,
                                                             HttpServletRequest request,
                                                             Locale locale) throws IOException, MessagingException {
        Collection<Firm> reqAccountFirms = requestHelper.getReqAccountFirms(request);
        // Check if ClientAccount firms are from requestAccount
        Collection<Long> clientAccountFirms = clientAccountDto.getFirms();
        if (clientAccountFirms != null) {
            if (!checkClientAccountFirmsFromRequestAccount(clientAccountFirms, reqAccountFirms)) {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
            }
        } clientAccountDto.setFirms(new ArrayList<Long>(){{reqAccountFirms.forEach(firm -> add(firm.getId()));}});
        ClientAccount clientAccount = (ClientAccount) accountDao.findByUsername(clientAccountDto.getUsername());
        if (clientAccount != null) {
            if (Objects.equals(clientAccount.getEmail(), clientAccountDto.getEmail())) {
                addFirmsToClientAccount(clientAccount, clientAccountDto.getFirms());
                return ResponseEntity.ok(
                        new ClientAccountDto((ClientAccount) accountDao.findById(clientAccount.getId())));
            } else return ResponseEntity.badRequest().body(null);
        } // If client does not exist
        clientAccount = accountDao.save(clientAccountDto);
        ClientAccountDto newClientAccountDto = new ClientAccountDto(clientAccount);
        if (clientAccount.getPassword() == null) {
            newClientAccountDto.setPassword(this.generateNewPassword(clientAccount, request, true, locale));
        }
        return ResponseEntity.ok(newClientAccountDto);
    }

    @RequestMapping(path = {
            Constants.API_ROLE_OWNER + "/accounts/resetPassword/{id}",
            Constants.API_ROLE_STAFF + "/accounts/resetPassword/{id}"}, method = GET)
    public ResponseEntity<?> resetPassword(@PathVariable("id") long id,
                                           HttpServletRequest request,
                                           Locale locale) throws IOException, MessagingException {
        Account account = accountDao.findById(id);
        if (account instanceof ClientAccount) {
            ClientAccountDto clientAccountDto = new ClientAccountDto((ClientAccount) account);
            clientAccountDto.setPassword(this.generateNewPassword((ClientAccount) account, request, false, locale));
            return ResponseEntity.ok(clientAccountDto);
        }
        return ResponseEntity.ok(null);
    }

    @RequestMapping(path = {
            Constants.API_ROLE_CLIENT + "/accounts/client/setNewPassword"}, method = POST)
    public ResponseEntity<?> setNewPassword(@RequestBody NewPasswordDto newPasswordDto,
                                            HttpServletRequest request) {
        ClientAccount clientAccount = (ClientAccount) requestHelper.getReqAccount(request);
        byte[] decodedBytes = Base64.decodeBase64(newPasswordDto.getPassword().getBytes());
        String[] decoded = new String(Base64.decodeBase64(newPasswordDto.getPassword().getBytes()))
                .split(":");

        if (!passwordEncoder.matches(decoded[0], clientAccount.getPassword())) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(null);
        }
        clientAccount.setPassword(decoded[1]);
        accountDao.save(clientAccount);
        return ResponseEntity.ok(null);
    }

    private String generateNewPassword(ClientAccount clientAccount, HttpServletRequest request, boolean newAccount, Locale locale) throws IOException, MessagingException {
        String password = PasswordUtils.generate();
        clientAccount.setPassword(password);
        accountDao.save(clientAccount);
        if (clientAccount.getEmail() != null) {
            if (newAccount == true) {
                emailService.sendNewAccount(
                        getOwnerAccount(request),
                        clientAccount,
                        password, locale);
            } else {
                emailService.sendNewPassword(getOwnerAccount(request), clientAccount, password, locale);
            }
            return null;
        }
        return password;
    }

    private OwnerAccount getOwnerAccount(HttpServletRequest request) {
        Account reqAccount = requestHelper.getReqAccount(request);
        OwnerAccount ownerAccount = null;
        if (request.isUserInRole(Constants.ROLE_OWNER)) ownerAccount = (OwnerAccount) reqAccount;
        else if (request.isUserInRole(Constants.ROLE_STAFF))
            ownerAccount = (OwnerAccount) ((StaffAccount) reqAccount).getFirm().getAssignedTo();
        return ownerAccount;
    }

    private void addFirmsToClientAccount(ClientAccount clientAccount, Collection<Long> firms) {
        firms.forEach(firmId -> {
            if (clientAccount.getFirms().stream().noneMatch(x -> x.getId() == firmId)) {
                clientAccountsFirmsDao.save(
                        new ClientAccountFirm(
                                clientAccount,
                                firmDao.findById(firmId),
                                false, false
                        )
                );
            }
        });
    }

    private boolean checkClientAccountFirmsFromRequestAccount(Collection<Long> firms,
                                                              Collection<Firm> requestAccountFirms) {
        for (long firmId : firms) {
            if (requestAccountFirms.stream().noneMatch(x -> x.getId() == firmId)) return false;
        }
        return true;
    }

    @RequestMapping(path = {
            Constants.API_ROLE_OWNER + "/accounts/client/{id}",
            Constants.API_ROLE_STAFF + "/accounts/client/{id}"}, method = DELETE)
    public ResponseEntity<?> deleteClientAccount(@PathVariable("id") long clientAccountId,
                                                 HttpServletRequest request) {
        ClientAccount clientAccount = accountDao.findClientAccountById(clientAccountId);
        clientAccountsFirmsDao.delete(clientAccount.getClientAccountsFirms()
                .stream().filter(x -> requestHelper.getReqAccountFirms(request).contains(x.getFirm()))
                .collect(Collectors.toList()));
        return ResponseEntity.ok(null);
    }

    @RequestMapping(path = {
            Constants.API_ROLE_OWNER + "/accounts/client/firm/{id}"}, method = GET)
    public ResponseEntity<Iterable<ClientAccountDto>> getAllClientAccountsForFirm(@PathVariable("id") long firmId,
                                                                                  HttpServletRequest request) {
        Firm firm = firmDao.findById(firmId);
        OwnerAccount ownerAccount = (OwnerAccount) requestHelper.getReqAccount(request);
        if (firm.getAssignedTo() == ownerAccount) {
            ArrayList<ClientAccount> clientAccounts =
                    (ArrayList<ClientAccount>) accountDao.findAllClientAccountsByFirm(firm);
            return ResponseEntity.ok(new ClientAccountListDto(clientAccounts).getClientAccounts());
        } else return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
    }

    @RequestMapping(path =
            Constants.API_ROLE_ROOT + "/accounts/owner/", method = POST)
    public void postNewOwner(@RequestBody NewOwnerDto newOwner) {
        accountDao.save(newOwner);
    }

    @RequestMapping(path =
            Constants.API_ROLE_ROOT + "/accounts/{id}", method = RequestMethod.DELETE)
    public void deleteAccount(@RequestParam("id") long id) {
        accountDao.delete(id);
    }

    @RequestMapping(path = {
            Constants.API_ROLE_ROOT + "/accounts/{id}",
            Constants.API_ROLE_ADMIN + "/accounts/{id}",
            Constants.API_ROLE_STAFF + "/accounts/{id}"
    },
            method = RequestMethod.GET)
    public ResponseEntity<BaseAccountDto> getBaseAccount(@PathVariable long id, HttpServletRequest request) {
        return ResponseEntity.ok(new BaseAccountDto(accountDao.findById(id)));
    }

    @RequestMapping(path =
            Constants.API_ROLE_OWNER + "/accounts/staff/branch/{branchId}",
            method = RequestMethod.GET)
    public ResponseEntity<Collection<StaffAccountDto>> getStaffForBranch(@PathVariable("branchId") long branchId,
                                                                         HttpServletRequest request) {
        Branch branch = branchDao.findById(branchId);
        Firm firm = branch.getFirm();
        Account user = requestHelper.getReqAccount(request);
        if (firm.getAssignedTo() == user) {
            ArrayList<StaffAccount> staffAccounts =
                    (ArrayList<StaffAccount>) accountDao.findStaffAccountsByBranch(branch);
            return ResponseEntity.ok(new StaffAccountListDto(staffAccounts).getStaffAccounts());
        } else return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
    }

    @RequestMapping(path =
            Constants.API_ROLE_OWNER + "/accounts/staff/firm/{firmId}",
            method = RequestMethod.GET)
    public ResponseEntity<Collection<StaffAccountDto>> getStaffForFirm(@PathVariable("firmId") long firmId,
                                                                       HttpServletRequest request) {
        Firm firm = firmDao.findById(firmId);
        Account user = requestHelper.getReqAccount(request);
        if (firm.getAssignedTo() == user)
            return ResponseEntity.ok(new StaffAccountListDto(firm.getStaff()).getStaffAccounts());
        else return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
    }

    @RequestMapping(path =
            Constants.API_ROLE_OWNER + "/accounts/staff/branch/{branchId}/{id}",
            method = RequestMethod.DELETE)
    public void deleteStaffAccountFromBranch(@PathVariable("branchId") long branchId,
                                             @PathVariable("id") long id,
                                             HttpServletRequest request) {
        Branch branch = branchDao.findById(branchId);
        Firm firm = branch.getFirm();
        Account user = requestHelper.getReqAccount(request);
        if (firm.getAssignedTo() == user) {
            StaffAccount account = accountDao.findStaffAccountById(id);
            account.deleteBranch(branch);
            accountDao.save(account);
        }
    }

    @RequestMapping(path =
            Constants.API_ROLE_OWNER + "/accounts/staff/", method = POST)
    public ResponseEntity<StaffAccountDto> addStaffAccount(@RequestBody NewStaffAccountDto staffAccount,
                                                           HttpServletRequest request) {
        Firm firm = firmDao.findById(staffAccount.getFirmId());
        Account user = requestHelper.getReqAccount(request);
        if (firm.getAssignedTo() == user) {
            accountDao.save(staffAccount);
            return ResponseEntity.ok(
                    new StaffAccountDto((StaffAccount) accountDao.findByUsername(staffAccount.getUsername()))
            );
        } else return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
    }

    @RequestMapping(path =
            Constants.API_ROLE_OWNER + "/accounts/staff/branch/{branchId}/{id}", method = PUT)
    public ResponseEntity<StaffAccountDto> addStaffAccountToBranch(@PathVariable("branchId") long branchId,
                                                                   @PathVariable("id") long id,
                                                                   HttpServletRequest request) {
        Branch branch = branchDao.findById(branchId);
        Firm firm = branch.getFirm();
        Account user = requestHelper.getReqAccount(request);
        if (firm.getAssignedTo() == user) {
            StaffAccount account = accountDao.findStaffAccountById(id);
            if (account.getBranches().contains(branch))
                return ResponseEntity.status(HttpStatus.NOT_MODIFIED).body(null);
            else {
                account.addBranch(branch);
                accountDao.save(account);
                return ResponseEntity.ok(new StaffAccountDto(accountDao.findStaffAccountById(account.getId())));
            }
        } else return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
    }

    @RequestMapping(path = {
            Constants.API_ROLE_OWNER + "/accounts/maxfirms/"}, method = GET)
    public ResponseEntity<Long> getMaxFirmsForOwner(HttpServletRequest request) {
        OwnerAccount ownerAccount = (OwnerAccount) requestHelper.getReqAccount(request);
        long maxFirms = ownerAccount.getMaxFirms();
        return ResponseEntity.ok(maxFirms);
    }
}
