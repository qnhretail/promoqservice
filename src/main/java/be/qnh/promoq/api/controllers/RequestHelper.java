package be.qnh.promoq.api.controllers;

import be.qnh.promoq.main.Constants;
import be.qnh.promoq.main.dataAccessObjects.FirmDaoImpl;
import be.qnh.promoq.main.models.Firm;
import be.qnh.promoq.main.models.StaffAccount;
import be.qnh.promoq.security.dataAccessObjects.AccountDaoImpl;
import be.qnh.promoq.security.models.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collection;

@Component
public class RequestHelper {
    @Autowired
    private AccountDaoImpl accountDao;
    @Autowired
    private FirmDaoImpl firmDao;

    public Account getReqAccount(HttpServletRequest request) {
        return accountDao.findByUsername(request.getUserPrincipal().getName());
    }

    public boolean isRoot(HttpServletRequest request) {
        return request.isUserInRole(Constants.ROLE_ROOT);
    }

    public Collection<Firm> getReqAccountFirms(HttpServletRequest request) {
        Account account = getReqAccount(request);
        Collection<Firm> firms = new ArrayList<>();
        if (request.isUserInRole(Constants.ROLE_OWNER)) {
            firms = firmDao.getAllCreatedByOrAssignedByOrAssignedTo(account);
        } else if (request.isUserInRole(Constants.ROLE_STAFF)) {
            firms.add(((StaffAccount) account).getFirm());
        }
        return firms;
    }

    public boolean isRequestAccountClientWithId(long id, HttpServletRequest request) {
        if (request.isUserInRole(Constants.ROLE_CLIENT))
            if (getReqAccount(request).getId() == id)
                return true;
        return false;
    }
}
