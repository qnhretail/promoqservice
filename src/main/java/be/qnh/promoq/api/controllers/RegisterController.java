package be.qnh.promoq.api.controllers;

import be.qnh.promoq.api.dataTransferObjects.RegisterDto;
import be.qnh.promoq.main.Constants;
import be.qnh.promoq.main.dataAccessObjects.BranchDaoImpl;
import be.qnh.promoq.main.dataAccessObjects.RegisterAccountDaoImpl;
import be.qnh.promoq.main.models.Firm;
import be.qnh.promoq.main.models.RegisterAccount;
import be.qnh.promoq.security.models.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(Constants.API_ROOT)
public class RegisterController {
    @Autowired
    private RegisterAccountDaoImpl registerDao;
    @Autowired
    private BranchDaoImpl branchDao;
    @Autowired
    private RequestHelper requestHelper;

    @RequestMapping(path =
            Constants.API_ROLE_OWNER + "/registers/",
            method = RequestMethod.POST)
    public ResponseEntity<?> addRegister(@RequestBody RegisterDto registerDto, HttpServletRequest request) {
        HttpStatus httpStatus = HttpStatus.OK;
        Object responseBody;
        RegisterAccount newRegisterAccount = new RegisterAccount(
                registerDto.getDescription(),
                registerDto.getSerialNr(),
                registerDto.getPassword(),
                branchDao.findById(registerDto.getBranchId()));
        Firm firm = newRegisterAccount.getBranch().getFirm();
        Account user = requestHelper.getReqAccount(request);
        if (firm.getAssignedToCreatedByAssignedBy().contains(user)) {
            newRegisterAccount = registerDao.save(newRegisterAccount);
            responseBody = new RegisterDto(newRegisterAccount);
        } else {
            httpStatus = HttpStatus.FORBIDDEN;
            responseBody = null;
        }
        return new ResponseEntity<Object>(responseBody, httpStatus);
    }

    @RequestMapping(path =
            Constants.API_ROLE_OWNER + "/registers/{id}",
            method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteRegisterById(@PathVariable long id, HttpServletRequest request) {
        HttpStatus httpStatus = HttpStatus.OK;
        Object responseBody = null;
        RegisterAccount registerAccount = registerDao.findById(id);
        Firm firm = registerAccount.getBranch().getFirm();
        Account user = requestHelper.getReqAccount(request);
        if (firm.getAssignedTo() != user) {
            httpStatus = HttpStatus.FORBIDDEN;
            responseBody = null;
        }
        registerDao.delete(registerAccount);
        return new ResponseEntity<Object>(responseBody, httpStatus);
    }

    @RequestMapping(path = {
            Constants.API_ROLE_OWNER + "/registers/branch/{id}",
            Constants.API_ROLE_STAFF + "/registers/branch/{id}"
    }, method = RequestMethod.GET)
    public ResponseEntity<?> getAllRegistersForBranch(@PathVariable long id, HttpServletRequest request) {
        HttpStatus httpStatus = HttpStatus.OK;
        List<RegisterDto> responseBody = new ArrayList<>();
        Iterable<RegisterAccount> registers = registerDao.findAllByBranch(branchDao.findById(id));
        for (RegisterAccount registerAccount : registers) {
            responseBody.add(new RegisterDto(registerAccount));
        }
        return new ResponseEntity<Object>(responseBody, httpStatus);
    }
}
