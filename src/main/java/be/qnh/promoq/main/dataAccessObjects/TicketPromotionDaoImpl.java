package be.qnh.promoq.main.dataAccessObjects;

import be.qnh.promoq.main.models.Ticket;
import be.qnh.promoq.main.models.TicketPromotion;
import be.qnh.promoq.main.repositories.TicketPromotionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class TicketPromotionDaoImpl implements TicketPromotionDao {
    @Autowired
    private TicketPromotionRepository ticketPromotionRepository;

    @Override
    public TicketPromotion save(TicketPromotion ticketPromotion) {
        return ticketPromotionRepository.save(ticketPromotion);
    }

    public Collection<TicketPromotion> findByTicket(Ticket ticket) {
        return ticketPromotionRepository.findByTicket(ticket);
    }
}
