package be.qnh.promoq.main.dataAccessObjects;

import be.qnh.promoq.main.models.Transfer;
import org.springframework.stereotype.Component;

@Component
public interface TransferDao {
    Transfer save(Transfer transfer);
}
