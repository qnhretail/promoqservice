package be.qnh.promoq.main.dataAccessObjects;

import be.qnh.promoq.main.models.ClientAccount;
import be.qnh.promoq.main.repositories.ClientAccountRepository;
import be.qnh.promoq.security.dataAccessObjects.AccountDao;
import be.qnh.promoq.security.models.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ClientAccountDaoImpl implements AccountDao {
    @Autowired
    private ClientAccountRepository clientAccountRepository;

    @Override
    public Account save(Account account) {
        return clientAccountRepository.save((ClientAccount) account);
    }

    @Override
    public Account findByUsername(String name) {
        return clientAccountRepository.findByUsername(name);
    }

    @Override
    public ClientAccount findById(long clientAccountId) {
        return clientAccountRepository.findById(clientAccountId);
    }

}
