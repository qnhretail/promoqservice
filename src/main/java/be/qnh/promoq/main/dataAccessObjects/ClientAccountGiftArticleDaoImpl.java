package be.qnh.promoq.main.dataAccessObjects;

import be.qnh.promoq.api.dataTransferObjects.AmountDto;
import be.qnh.promoq.main.models.ClientAccount;
import be.qnh.promoq.main.models.ClientAccountGiftArticle;
import be.qnh.promoq.main.models.GiftArticle;
import be.qnh.promoq.main.repositories.ClientAccountGiftArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class ClientAccountGiftArticleDaoImpl implements ClientAccountGiftArticleDao {

    @Autowired
    private ClientAccountGiftArticleRepository clientAccountGiftArticleRepository;

    public ClientAccountGiftArticle save(ClientAccountGiftArticle clientAccountGiftArticle) {
        return clientAccountGiftArticleRepository.save(clientAccountGiftArticle);
    }

    @Override
    public ClientAccountGiftArticle findById(ClientAccountGiftArticle clientAccountGiftArticle) {
        return clientAccountGiftArticleRepository.findById(clientAccountGiftArticle);
    }

    public Collection<ClientAccountGiftArticle> findByClientAccount(ClientAccount clientAccount) {
        return clientAccountGiftArticleRepository.findByClientAccount(clientAccount);
    }

    public ClientAccountGiftArticle findByClientAccountAndGiftArticle(ClientAccount clientAccount, long giftArticleId) {
        return clientAccountGiftArticleRepository.findByClientAccountAndGiftArticle_Id(clientAccount, giftArticleId);
    }

    public AmountDto getClientAccountGiftArticleAmount(ClientAccount clientAccount, GiftArticle giftArticle) {
        ClientAccountGiftArticle clientAccountGiftArticle =
                findByClientAccountAndGiftArticle(clientAccount,
                        giftArticle.getId());
        if (clientAccountGiftArticle != null) {
            return new AmountDto(clientAccountGiftArticle.getQuantity(), null);
        }
        return new AmountDto(0, null);
    }
}
