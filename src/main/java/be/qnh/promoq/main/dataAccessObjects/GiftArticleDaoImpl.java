package be.qnh.promoq.main.dataAccessObjects;

import be.qnh.promoq.api.dataTransferObjects.GiftArticleDto;
import be.qnh.promoq.main.models.Firm;
import be.qnh.promoq.main.models.GiftArticle;
import be.qnh.promoq.main.models.Product;
import be.qnh.promoq.main.repositories.GiftArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;

@Component
public class GiftArticleDaoImpl implements GiftArticleDao {
    @Autowired
    private GiftArticleRepository giftArticleRepository;
    @Autowired
    private FirmDaoImpl firmDao;
    @Autowired
    private ProductDaoImpl productDao;

    @Override
    public GiftArticle findById(long id) {
        return giftArticleRepository.findById(id);
    }

    public Iterable<GiftArticle> findByFirm(Firm firm) {
        return giftArticleRepository.findByFirm(firm);
    }

    public GiftArticleDto save(GiftArticleDto giftArticle) {
        return new GiftArticleDto(save(dtoToGiftArticle(giftArticle)));
    }

    @Override
    public GiftArticle save(GiftArticle giftArticle) {
        return giftArticleRepository.save(giftArticle);
    }

    private GiftArticle dtoToGiftArticle(GiftArticleDto dto) {
        GiftArticle giftArticle = new GiftArticle();
        giftArticle.setName(dto.getName());
        giftArticle.setFirm(firmDao.findById(dto.getFirmId()));
        giftArticle.setId(dto.getId());
        Collection<Product> products = new ArrayList<>();
        dto.getProductPlus().forEach(
                product -> products.add(productDao.findById(product)));
        giftArticle.setProducts(products);
        return giftArticle;
    }

    public Iterable<GiftArticle> save(Collection<GiftArticle> giftArticles) {
        return giftArticleRepository.save(giftArticles);
    }

    public GiftArticle findByName(String name) {
        return giftArticleRepository.findByName(name);
    }
}
