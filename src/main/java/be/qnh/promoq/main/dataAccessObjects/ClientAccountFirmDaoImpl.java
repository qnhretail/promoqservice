package be.qnh.promoq.main.dataAccessObjects;

import be.qnh.promoq.api.dataTransferObjects.ClientAccountFirmDto;
import be.qnh.promoq.main.models.ClientAccount;
import be.qnh.promoq.main.models.ClientAccountFirm;
import be.qnh.promoq.main.models.Firm;
import be.qnh.promoq.main.repositories.ClientAccountsFirmsRepository;
import be.qnh.promoq.security.dataAccessObjects.AccountDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ClientAccountFirmDaoImpl implements ClientAccountFirmDao {
    @Autowired
    private ClientAccountsFirmsRepository clientAccountsfimrsRepository;
    @Autowired
    private AccountDaoImpl accountDao;
    @Autowired
    private FirmDaoImpl firmDao;

    public Iterable<ClientAccountFirm> findByFirm(Firm firm) {
        return clientAccountsfimrsRepository.findAllByFirm(firm);
    }

    public ClientAccountFirm save(ClientAccountFirm clientAccountsFirms) {
        return clientAccountsfimrsRepository.save(clientAccountsFirms);
    }

    public void delete(List<ClientAccountFirm> clientAccountFirms) {
        clientAccountsfimrsRepository.delete(clientAccountFirms);
    }

    public Iterable<ClientAccountFirm> findByClientAccountId(long id) {
        return clientAccountsfimrsRepository.findAllByClientAccountId(id);
    }

    public ClientAccountFirm save(ClientAccountFirmDto clientAccountFirmDto) {
        ClientAccountFirm clientAccountFirm = clientAccountsfimrsRepository.findByFirmIdAndClientAccountId(clientAccountFirmDto.getFirmId(), clientAccountFirmDto.getClientId());
        if (clientAccountFirm != null) {
            clientAccountFirm.setReceiveNewsletter(clientAccountFirmDto.isReceiveNewsletter());
            clientAccountFirm.setReceivePromotions(clientAccountFirmDto.isReceivePromotions());
        } else {
            clientAccountFirm = new ClientAccountFirm(
                    accountDao.findClientAccountById(clientAccountFirmDto.getClientId()),
                    firmDao.findById(clientAccountFirmDto.getFirmId()),
                    clientAccountFirmDto.isReceivePromotions(),
                    clientAccountFirmDto.isReceiveNewsletter());
        }
        return clientAccountsfimrsRepository.save(clientAccountFirm);
    }

    public ClientAccountFirm findByClientAccountAndFirm(ClientAccount clientAccount, Firm firm) {
        return clientAccountsfimrsRepository.findByClientAccountAndFirm(clientAccount, firm);
    }

    public ClientAccountFirm findByClientAccountIdAndFirmId(long clientAccountId, long firmId) {
        return clientAccountsfimrsRepository.findByClientAccountIdAndFirmId(clientAccountId, firmId);
    }
}
