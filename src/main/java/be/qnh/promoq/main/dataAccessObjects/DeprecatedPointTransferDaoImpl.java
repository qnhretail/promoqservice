package be.qnh.promoq.main.dataAccessObjects;

import be.qnh.promoq.main.models.DeprecatedPointTransfer;
import be.qnh.promoq.main.repositories.DeprecatedPointTransferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DeprecatedPointTransferDaoImpl implements DeprecatedPointTransferDao {
    @Autowired
    private DeprecatedPointTransferRepository deprecatedPointTransferRepository;

    public Iterable<DeprecatedPointTransfer> findAll() {
        return deprecatedPointTransferRepository.findAll();
    }
}
