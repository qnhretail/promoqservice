package be.qnh.promoq.main.dataAccessObjects;

import be.qnh.promoq.main.models.Ticket;
import be.qnh.promoq.main.models.TicketProduct;
import be.qnh.promoq.main.repositories.TicketProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class TicketProductDaoImpl implements TicketProductDao {
    @Autowired
    private TicketProductRepository ticketProductRepository;

    @Override
    public TicketProduct save(TicketProduct ticketProduct) {
        return ticketProductRepository.save(ticketProduct);
    }

    public Collection<TicketProduct> findByticket(Ticket ticket) {
        return ticketProductRepository.findByTicket(ticket);
    }

    public Collection<TicketProduct> findByticketId(long id) {
        return ticketProductRepository.findByTicketId(id);
    }

    public Collection<TicketProduct> getAllTicketProducts() {
        return (Collection<TicketProduct>) ticketProductRepository.findAll();
    }
}
