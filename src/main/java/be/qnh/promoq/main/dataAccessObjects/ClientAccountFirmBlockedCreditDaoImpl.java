package be.qnh.promoq.main.dataAccessObjects;

import be.qnh.promoq.main.models.ClientAccountFirmBlockedCredit;
import be.qnh.promoq.main.models.RegisterAccount;
import be.qnh.promoq.main.repositories.ClientAccountFirmBlockedCreditRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ClientAccountFirmBlockedCreditDaoImpl implements ClientAccountFirmBlockedCreditDao {
    @Autowired
    ClientAccountFirmBlockedCreditRepository clientAccountFirmBlockedCreditRepository;

    public void delete(ClientAccountFirmBlockedCredit blockedCredit) {
        clientAccountFirmBlockedCreditRepository.delete(blockedCredit);
    }

    public ClientAccountFirmBlockedCredit findFirstByRegisterAccount(RegisterAccount registerAccount) {
        return clientAccountFirmBlockedCreditRepository.findFirstByRegisterAccount(registerAccount);
    }
}
