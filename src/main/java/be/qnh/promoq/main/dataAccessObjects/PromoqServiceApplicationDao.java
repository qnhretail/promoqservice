package be.qnh.promoq.main.dataAccessObjects;

import be.qnh.promoq.main.models.PromoqServiceApplication;

public interface PromoqServiceApplicationDao {
    PromoqServiceApplication findById(long i);

    PromoqServiceApplication save(PromoqServiceApplication promoqServiceApplication);
}
