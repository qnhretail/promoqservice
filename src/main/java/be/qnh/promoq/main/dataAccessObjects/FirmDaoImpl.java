package be.qnh.promoq.main.dataAccessObjects;

import be.qnh.promoq.main.models.Firm;
import be.qnh.promoq.main.models.OwnerAccount;
import be.qnh.promoq.main.repositories.FirmRepository;
import be.qnh.promoq.security.dataAccessObjects.AccountDaoImpl;
import be.qnh.promoq.security.models.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;

@Component
public class FirmDaoImpl implements FirmDao {
    @Autowired
    FirmRepository firmRepository;
    @Autowired
    private AccountDaoImpl accountDao;
    private ClientAccountDaoImpl clientAccountDao;

    @Override
    public Firm save(Firm firm) {
        return firmRepository.save(firm);
    }

    @Override
    public Firm findById(long id) {
        return firmRepository.findById(id);
    }

    @Override
    public Collection<Firm> findAll() {
        return (Collection<Firm>) firmRepository.findAll();
    }

    @Override
    public void delete(long id) {
        firmRepository.delete(id);
    }

    public Iterable<Firm> getAllOrphan() {
        return firmRepository.findFirmByAssignedToIsNull();
    }

    public Iterable<Firm> getAllOrphanCreatedBy(Account account) {
        return firmRepository.findFirmByAssignedToIsNullAndCreatedBy(account);
    }

    public void setOwner(long id, long ownerId, String assignedBy) {
        Firm firm = firmRepository.findById(id);
        OwnerAccount account = (OwnerAccount) accountDao.findById(ownerId);
        firm.setAssignedTo(account);
        Account assignedByAccount = accountDao.findByUsername(assignedBy);
        firm.setAssignedBy(assignedByAccount);
        firmRepository.save(firm);
    }

    public Firm deleteOwner(long id) {
        Firm firm = firmRepository.findById(id);
        firm.setAssignedTo(null);
        firm.setAssignedBy(null);
        return firm;
    }

    public Firm save(Firm firm, OwnerAccount account) {
        firm.setAssignedBy(account);
        firm.setAssignedTo(account);
        firm.setCreatedBy(account);
        firmRepository.save(firm);
        return firm;
    }

    public Collection<Firm> getAllCreatedByOrAssignedByOrAssignedTo(Account account) {
        Collection<Firm> firms = (Collection<Firm>) firmRepository.findFirmByCreatedByOrAssignedByOrAssignedTo(account, account, account);
        firms.removeIf(x -> !x.isActive());
        return firms;
    }

    public Firm addOwner(long id, OwnerAccount account, String assignedBy) {
        Firm firm = firmRepository.findById(id);
        firm.setAssignedTo(account);
        firm.setAssignedBy(accountDao.findByUsername(assignedBy));
        firmRepository.save(firm);
        return firm;
    }

    public Firm addClientAccount(long id, ArrayList<Integer> clientAccountIds) {
        Firm firm = firmRepository.findById(id);
        for (int clientAccountId : clientAccountIds) {
            //firm.addClient(clientAccountDao.findById(clientAccountId));
        }
        firmRepository.save(firm);
        return firm;
    }

    public Firm deleteClientAccounts(long id, ArrayList<Integer> clientAccountIds) {
        Firm firm = firmRepository.findById(id);
        for (int clientAccountId : clientAccountIds) {
            //firm.deleteClient(clientAccountDao.findById(clientAccountId0;
        }
        firmRepository.save(firm);
        return firm;
    }

    public Firm deactivate(Firm firm) {
        firm.setActive(false);
        return firmRepository.save(firm);
    }

    public Firm findByName(String name) {
        return firmRepository.findByName(name);
    }
}
