package be.qnh.promoq.main.dataAccessObjects;

import be.qnh.promoq.api.dataTransferObjects.PromotionDto;
import be.qnh.promoq.main.models.*;
import be.qnh.promoq.main.repositories.PromotionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

@Component
public class PromotionDao {
    @Autowired
    PromotionRepository promotionRepository;
    @Autowired
    private FirmDaoImpl firmDao;
    @Autowired
    private BranchDaoImpl branchDao;
    @Autowired
    private GiftArticleDaoImpl giftArticleDao;

    public Collection<Promotion> findByFirm(Firm firm) {
        return findByFirm(firm, false);
    }

    public Collection<Promotion> findByFirm(Firm firm, boolean active) {
        Collection<Promotion> allPromotions = promotionRepository.findByFirm(firm);
        return !active ?
                allPromotions :
                allPromotions.stream().filter(promo ->
                        !promo.getValidUntil().before(new Date()) &&
                                promo.getValidFrom().before(new Date()))
                        .collect(Collectors.toList());
    }

    public Promotion save(PromotionDto promotion) {
        Collection<Branch> branches = new ArrayList<>();
        promotion.getBranches().forEach(branch -> branches.add(branchDao.findById(branch)));
        switch (promotion.getType()) {
            case 0:
                switch (promotion.getValuePromotionType()) {
                    case 0:
                        return save(new ValuePromotionWithGiftArticle(
                                promotion.getName(),
                                promotion.getDescription(),
                                promotion.getValidFrom(),
                                promotion.getValidUntil(),
                                promotion.getValidateUntil(),
                                promotion.isFirmPromotion(),
                                firmDao.findById(promotion.getFirmId()),
                                branches,
                                promotion.getValueOfPoint(),
                                promotion.getValueGift(),
                                giftArticleDao.findById(promotion.getGiftArticleId())
                        ));
                    case 1:
                        return save(new ValuePromotionDirectDisc(
                                promotion.getName(),
                                promotion.getDescription(),
                                promotion.getValidFrom(),
                                promotion.getValidUntil(),
                                promotion.isFirmPromotion(),
                                firmDao.findById(promotion.getFirmId()),
                                branches,
                                promotion.getValueOfPoint(),
                                promotion.getValueGift(),
                                promotion.getGiftAmount()
                        ));
                }

            default:
                return null;
        }
    }

    private ValuePromotion save(ValuePromotion valuePromotion) {
        return promotionRepository.save(valuePromotion);
    }

    public Promotion save(Promotion promotion) {
        return promotionRepository.save(promotion);
    }

    public Collection<? extends Promotion> findValidFirmPromotions(Firm firm, Date date) {
        return promotionRepository.
                findByFirmPromotionIsTrueAndFirmAndValidFromIsLessThanEqualAndValidUntilGreaterThanEqual(
                        firm, date, date
                );
    }

    public Collection<? extends Promotion> findValidBranchPromotions(Branch branch, Date date) {
        return promotionRepository.
                findByBranchesContainsAndValidFromIsLessThanEqualAndValidUntilGreaterThanEqual(
                        branch, date, date
                );
    }

    public Promotion findById(long id) {
        return promotionRepository.findById(id);
    }

    public void delete(Promotion promotion) {
        promotionRepository.delete(promotion);
    }
}
