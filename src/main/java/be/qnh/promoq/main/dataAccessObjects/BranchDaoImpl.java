package be.qnh.promoq.main.dataAccessObjects;

import be.qnh.promoq.api.dataTransferObjects.BranchDto;
import be.qnh.promoq.main.models.Branch;
import be.qnh.promoq.main.models.Firm;
import be.qnh.promoq.main.repositories.BranchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BranchDaoImpl implements BranchDao {
    @Autowired
    BranchRepository branchRepository;

    @Override
    public Branch save(Branch branch) {
        return branchRepository.save(branch);
    }

    @Override
    public Branch findById(long id) {
        return branchRepository.findById(id);
    }

    public Branch save(BranchDto branch, Firm firm) {
        return save(new Branch(branch, firm));

    }

    public void deleteBranch(Branch branch) {
        branchRepository.delete(branch);
    }

    public Iterable<Branch> findByFirm(Firm firm) {
        return branchRepository.findByFirmAndActiveIsTrue(firm);
    }

    public Branch deactivate(Branch branch) {
        branch.setActive(false);
        return branchRepository.save(branch);
    }

    public Branch findByName(String name) {
        return branchRepository.findByName(name);
    }
}
