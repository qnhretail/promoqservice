package be.qnh.promoq.main.dataAccessObjects;

import be.qnh.promoq.main.models.ClientAccountFirm;

public interface ClientAccountFirmDao {
    ClientAccountFirm save(ClientAccountFirm clientAccountFirm);
}
