package be.qnh.promoq.main.dataAccessObjects;

import be.qnh.promoq.main.models.TicketProduct;

public interface TicketProductDao {
    TicketProduct save(TicketProduct ticketProduct);
}
