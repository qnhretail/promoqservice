package be.qnh.promoq.main.dataAccessObjects;

import be.qnh.promoq.main.models.ClientAccountGiftArticle;

public interface ClientAccountGiftArticleDao {
    ClientAccountGiftArticle save(ClientAccountGiftArticle clientAccountGiftArticle);

    ClientAccountGiftArticle findById(ClientAccountGiftArticle clientAccountGiftArticle);
}
