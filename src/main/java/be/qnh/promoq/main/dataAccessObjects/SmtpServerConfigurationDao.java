package be.qnh.promoq.main.dataAccessObjects;

import be.qnh.promoq.main.models.OwnerAccount;
import be.qnh.promoq.main.models.SmtpServerConfiguration;
import be.qnh.promoq.main.repositories.SmtpServerConfigurationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SmtpServerConfigurationDao {
    @Autowired
    private SmtpServerConfigurationRepository smtpServerConfigurationRepository;

    public SmtpServerConfiguration findByOwnerAccount(OwnerAccount ownerAccount) {
        return smtpServerConfigurationRepository.findByOwnerAccount(ownerAccount);
    }

    public SmtpServerConfiguration save(SmtpServerConfiguration smtpServerConfiguration) {
        return smtpServerConfigurationRepository.save(smtpServerConfiguration);
    }
}
