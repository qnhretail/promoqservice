package be.qnh.promoq.main.dataAccessObjects;

import be.qnh.promoq.api.dataTransferObjects.AmountDto;
import be.qnh.promoq.main.models.*;
import be.qnh.promoq.main.repositories.ClientAccountGiftArticleRepository;
import be.qnh.promoq.main.repositories.ClientAccountsFirmsRepository;
import be.qnh.promoq.main.repositories.ClientAccountsPromotionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;

@Component
public class ClientAccountPromotionDaoImpl implements ClientAccountPromotionDao {
    @Autowired
    private ClientAccountsPromotionsRepository clientAccountsPromotionsRepository;
    @Autowired
    private ClientAccountGiftArticleRepository clientAccountGiftArticleRepository;
    @Autowired
    private ClientAccountsFirmsRepository clientAccountFirmRepository;

    public ClientAccountPromotion findByClientAccountAndPromotion(ClientAccount clientAccount, Promotion promotion) {
        return clientAccountsPromotionsRepository.findByClientAccountAndPromotion(clientAccount, promotion);
    }

    @Override
    public ClientAccountPromotion save(ClientAccountPromotion clientAccountPromotion) {
        return clientAccountsPromotionsRepository.save(clientAccountPromotion);
    }

    public void delete(ClientAccountPromotion clientAccountPromotion) {
        clientAccountsPromotionsRepository.delete(clientAccountPromotion);
    }

    public Collection<ClientAccountPromotion> findValuePromotionsByClientAccount(ClientAccount account) {
        return new ArrayList<ClientAccountPromotion>() {{
            clientAccountsPromotionsRepository.findByClientAccount(account)
                    .forEach(clientAccountPromotion -> {
                        if (clientAccountPromotion.getPromotion() instanceof ValuePromotion)
                            add(clientAccountPromotion);
                    });
        }};
    }

    public AmountDto getClientAccountPromotionPoints(long clientAccountId, long promotionId) {
        ClientAccountPromotion clientAccountPromotion =
                findByClientAccountIdAndPromotionId(clientAccountId, promotionId);
        if (clientAccountPromotion != null) {
            return new AmountDto(clientAccountPromotion.getPoints(), null);
        }
        return new AmountDto(0L, null);
    }

    private ClientAccountPromotion findByClientAccountIdAndPromotionId(long clientAccountId, long promotionId) {
        return clientAccountsPromotionsRepository.findByClientAccountIdAndPromotionId(clientAccountId, promotionId);
    }

    public long addClientAccountPromotionPoints(ClientAccount clientAccount, Promotion promotion, Long amount) {
        ClientAccountPromotion clientAccountPromotion =
                findByClientAccountIdAndPromotionId(clientAccount.getId(), promotion.getId());
        if (clientAccountPromotion == null) clientAccountPromotion =
                new ClientAccountPromotion(clientAccount, promotion);
        clientAccountPromotion.addPoints(amount);
        if (promotion instanceof ValuePromotion)
            addClientAccountGiftArticles(clientAccountPromotion);
        return clientAccountPromotion.getPoints();
    }

    private void addClientAccountGiftArticles(ClientAccountPromotion clientAccountPromotion) {
        ValuePromotion promotion = (ValuePromotion) clientAccountPromotion.getPromotion();
        long valueGift = promotion.getValueGift();
        long numberOfGiftArticlesEarned = clientAccountPromotion.getPoints() / valueGift;
        if (numberOfGiftArticlesEarned >= 1) {
            ClientAccount account = clientAccountPromotion.getClientAccount();
            if (promotion instanceof ValuePromotionWithGiftArticle) {
                GiftArticle giftArticle = ((ValuePromotionWithGiftArticle) promotion).getGiftArticle();

                ClientAccountGiftArticle clientAccountGiftArticle =
                        clientAccountGiftArticleRepository.findByClientAccountAndGiftArticle(
                                account, giftArticle);
                if (clientAccountGiftArticle == null) clientAccountGiftArticle =
                        new ClientAccountGiftArticle(account, giftArticle, numberOfGiftArticlesEarned);
                else clientAccountGiftArticle.addQuantity(numberOfGiftArticlesEarned);
                clientAccountGiftArticleRepository.save(clientAccountGiftArticle);
            } else if (promotion instanceof ValuePromotionDirectDisc) {
                ClientAccountFirm clientAccountFirm = clientAccountFirmRepository.findByClientAccountAndFirm(account, promotion.getFirm());
                Float giftAmount = ((ValuePromotionDirectDisc) promotion).getGiftAmount();
                clientAccountFirm.setCredit(clientAccountFirm.getCredit() + numberOfGiftArticlesEarned * giftAmount);
            }
            clientAccountPromotion.setPoints(clientAccountPromotion.getPoints() % valueGift);
            clientAccountsPromotionsRepository.save(clientAccountPromotion);
        }
    }
}
