package be.qnh.promoq.main.dataAccessObjects;

import be.qnh.promoq.main.models.Branch;

public interface BranchDao {
    Branch save(Branch branch);

    Branch findById(long id);
}
