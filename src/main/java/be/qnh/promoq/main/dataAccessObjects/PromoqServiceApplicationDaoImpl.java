package be.qnh.promoq.main.dataAccessObjects;

import be.qnh.promoq.main.models.PromoqServiceApplication;
import be.qnh.promoq.main.repositories.PromoqServiceApplicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PromoqServiceApplicationDaoImpl implements PromoqServiceApplicationDao {
    @Autowired
    private PromoqServiceApplicationRepository promoqServiceRepository;

    @Override
    public PromoqServiceApplication findById(long id) {
        return promoqServiceRepository.findById(id);
    }

    @Override
    public PromoqServiceApplication save(PromoqServiceApplication promoqServiceApplication) {
        return promoqServiceRepository.save(promoqServiceApplication);
    }
}
