package be.qnh.promoq.main.dataAccessObjects;

import be.qnh.promoq.main.models.RegisterLog;
import be.qnh.promoq.main.repositories.RegisterLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RegisterLogDaoImpl implements RegisterLogDao {
    @Autowired
    private RegisterLogRepository registerLogRepository;

    @Override
    public RegisterLog save(RegisterLog registerLog) {
        System.out.println(registerLog.getObject());
        return registerLogRepository.save(registerLog);
    }
}
