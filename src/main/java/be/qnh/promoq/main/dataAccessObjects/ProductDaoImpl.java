package be.qnh.promoq.main.dataAccessObjects;

import be.qnh.promoq.main.models.Firm;
import be.qnh.promoq.main.models.Product;
import be.qnh.promoq.main.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class ProductDaoImpl implements ProductDao {
    @Autowired
    private ProductRepository productRepository;

    @Override
    public Product findById(Long id) {
        return productRepository.findById(id);
    }

    public Product findByProductCode(String plu) {
        return productRepository.findByProductCode(plu);
    }

    @Override
    public Product save(Product product) {
        return productRepository.save(product);
    }

    public Iterable<Product> save(Collection<Product> productCollection) {
        return productRepository.save(productCollection);
    }

    public Product findByName(String name) {
        return productRepository.findByName(name);
    }

    public Collection<Product> findAll() {
        return (Collection<Product>) productRepository.findAll();
    }

    public Collection<Product> findByFirm(Firm firm) {
        return productRepository.findByFirm(firm);
    }
}
