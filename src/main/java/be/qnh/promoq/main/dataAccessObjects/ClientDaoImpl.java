package be.qnh.promoq.main.dataAccessObjects;

import be.qnh.promoq.api.dataTransferObjects.ClientDto;
import be.qnh.promoq.main.models.Client;
import be.qnh.promoq.main.models.ClientAccount;
import be.qnh.promoq.main.repositories.ClientRepository;
import be.qnh.promoq.security.dataAccessObjects.AccountDaoImpl;
import be.qnh.promoq.security.models.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ClientDaoImpl implements ClientDao {
    @Autowired
    ClientRepository clientRepository;
    @Autowired
    AccountDaoImpl accountDao;

    public Iterable<Client> findByAccount(Account account) {
        return clientRepository.findAllByAccount(account);
    }

    public Client save(ClientDto clientDto) {
        Client client = new Client();
        client.setName(clientDto.getName());
        client.setSurname(clientDto.getSurname());
        client.setBirthday(clientDto.getBirthday());
        client.setCardId(clientDto.getCardId());
        client.setAccount((ClientAccount) accountDao.findById(clientDto.getAccountId()));
        return clientRepository.save(client);
    }

    public Client findByCardId(String cardId) {
        return clientRepository.findOneByCardId(cardId);
    }

    @Override
    public Client findById(long id) {
        return clientRepository.findById(id);
    }

    @Override
    public Client save(Client client) {
        return clientRepository.save(client);
    }

    public Client findFirstByAccount(ClientAccount clientAccount) {
        return clientRepository.findFirstByAccount(clientAccount);
    }
}
