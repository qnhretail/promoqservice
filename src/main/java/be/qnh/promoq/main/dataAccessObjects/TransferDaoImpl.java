package be.qnh.promoq.main.dataAccessObjects;

import be.qnh.promoq.main.models.CreditTransfer;
import be.qnh.promoq.main.models.PointTransfer;
import be.qnh.promoq.main.models.Transfer;
import be.qnh.promoq.main.repositories.TransferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class TransferDaoImpl implements TransferDao {
    @Autowired
    private TransferRepository transferRepository;

    @Override
    public Transfer save(Transfer transfer) {
        return transferRepository.save(transfer);
    }

    public Collection<Transfer> findByClientAccountId(long clientAccountId) {
        return transferRepository.findByClientAccountId(clientAccountId);
    }

    public Collection<PointTransfer> findPointTransfersByClientAccountId(long clientAccountId) {
        return transferRepository.findAllPointTransfersByClientAccountId(clientAccountId);
    }

    public Collection<CreditTransfer> findCreditTransfersByClientAccountId(long clientAccountId) {
        return transferRepository.findAllCreditTransfersByClientAccountId(clientAccountId);
    }
}
