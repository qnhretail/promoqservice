package be.qnh.promoq.main.dataAccessObjects;

import be.qnh.promoq.main.models.Branch;
import be.qnh.promoq.main.models.RegisterAccount;
import be.qnh.promoq.main.repositories.RegisterAccountRepository;
import be.qnh.promoq.security.dataAccessObjects.AccountDao;
import be.qnh.promoq.security.models.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RegisterAccountDaoImpl implements AccountDao {
    @Autowired
    RegisterAccountRepository registerAccountRepository;

    public Iterable<RegisterAccount> findAllByBranch(Branch branch) {
        return registerAccountRepository.findAllByBranch(branch);
    }

    @Override
    public RegisterAccount save(Account account) {
        return registerAccountRepository.save((RegisterAccount) account);
    }

    @Override
    public RegisterAccount findByUsername(String name) {
        return null;
    }

    @Override
    public RegisterAccount findById(long id) {
        return registerAccountRepository.findById(id);
    }

    public void delete(RegisterAccount registerAccount) {
        registerAccountRepository.delete(registerAccount);
    }

}
