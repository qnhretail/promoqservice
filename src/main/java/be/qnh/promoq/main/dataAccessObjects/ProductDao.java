package be.qnh.promoq.main.dataAccessObjects;

import be.qnh.promoq.main.models.Product;

public interface ProductDao {
    Product findById(Long id);

    Product save(Product product);
}
