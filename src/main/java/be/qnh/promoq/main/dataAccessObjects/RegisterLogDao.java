package be.qnh.promoq.main.dataAccessObjects;

import be.qnh.promoq.main.models.RegisterLog;

public interface RegisterLogDao {
    RegisterLog save(RegisterLog registerLog);
}
