package be.qnh.promoq.main.dataAccessObjects;

import be.qnh.promoq.main.models.GiftArticle;

public interface GiftArticleDao {
    GiftArticle findById(long id);

    GiftArticle save(GiftArticle giftArticle);
}
