package be.qnh.promoq.main.dataAccessObjects;

import be.qnh.promoq.main.models.TicketPromotion;

public interface TicketPromotionDao {
    TicketPromotion save(TicketPromotion ticketPromotion);
}
