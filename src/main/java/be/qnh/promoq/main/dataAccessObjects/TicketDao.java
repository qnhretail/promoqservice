package be.qnh.promoq.main.dataAccessObjects;

import be.qnh.promoq.main.models.Ticket;

public interface TicketDao {
    Ticket save(Ticket ticket);

    Ticket findById(long id);
}
