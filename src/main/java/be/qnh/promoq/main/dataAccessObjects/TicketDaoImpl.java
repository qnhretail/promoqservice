package be.qnh.promoq.main.dataAccessObjects;

import be.qnh.promoq.api.dataTransferObjects.*;
import be.qnh.promoq.main.models.ClientAccount;
import be.qnh.promoq.main.models.Product;
import be.qnh.promoq.main.models.Ticket;
import be.qnh.promoq.main.repositories.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;

@Component
public class TicketDaoImpl implements TicketDao {

    @Autowired
    private ClientAccountPromotionDaoImpl clientAccountsPromotionsDao;
    @Autowired
    private ClientAccountGiftArticleDaoImpl ClientAccountgiftArticleDao;
    @Autowired
    private TicketPromotionDaoImpl ticketPromotionDao;
    @Autowired
    private TicketRepository ticketRepository;

    @Override
    public Ticket save(Ticket ticket) {
        return ticketRepository.save(ticket);
    }

    @Override
    public Ticket findById(long id) {
        return ticketRepository.findById(id);
    }

    public TicketDto ticketToDto(Ticket ticket) {
        Collection<TicketProductDto> articles = this.getTicketProductDtoCollection(ticket);
        Collection<TicketPromotionDto> promotions = this.getTicketPromotionDtoCollection(ticket);
        Collection<ClientAccountGiftArticleDto> giftArticles = null;
        try {
            giftArticles =
                    this.getClientAccountGiftArticleDtoCollection(ticket.getClient().getAccount());
        } catch (Exception ignored) {
        }
        Iterable<ClientAccountValuePromotionDto> valuePromotions = null;
        try {
            valuePromotions = this.getClientAccountValuePromotionCollection(ticket.getClient().getAccount());
        } catch (Exception ignored) {
        }

        return new TicketDto(
                ticket.getId(),
                ticket.getRegisterAccount().getSerialNr(),
                ticket.getClient().getCardId(),
                ticket.getStaffMemeber(),
                ticket.getTicketNumber(),
                ticket.getDateTime(),
                articles,
                promotions,
                giftArticles,
                valuePromotions);
    }

    // todo: refactoring: wat doet dit hier??????

    private Iterable<ClientAccountValuePromotionDto> getClientAccountValuePromotionCollection(ClientAccount account) {
        return new ArrayList<ClientAccountValuePromotionDto>() {{
            clientAccountsPromotionsDao.findValuePromotionsByClientAccount(account)
                    .forEach(clientAccountPromotion -> add(new ClientAccountValuePromotionDto(
                            clientAccountPromotion.getPromotion().getId(),
                            clientAccountPromotion.getPromotion().getName(),
                            clientAccountPromotion.getPoints()
                    )));
        }};
    }

    private Collection<ClientAccountGiftArticleDto> getClientAccountGiftArticleDtoCollection(ClientAccount account) {
        return new ArrayList<ClientAccountGiftArticleDto>() {
            {
                ClientAccountgiftArticleDao.findByClientAccount(account)
                        .forEach(clientAccountGiftArticle -> add(new ClientAccountGiftArticleDto(
                                clientAccountGiftArticle.getGiftArticle().getId(),
                                clientAccountGiftArticle.getGiftArticle().getName(),
                                getProductPluCollection(clientAccountGiftArticle.getGiftArticle().getProducts()),
                                clientAccountGiftArticle.getQuantity())));
            }

            private Iterable<String> getProductPluCollection(Collection<Product> products) {
                return new ArrayList<String>() {{
                    products.forEach(product -> add(product.getProductCode()));
                }};
            }
        };
    }

    private Collection<TicketPromotionDto> getTicketPromotionDtoCollection(Ticket ticket) {
        return new ArrayList<TicketPromotionDto>() {{
            ticketPromotionDao.findByTicket(ticket)
                    .forEach(ticketPromotion -> add(new TicketPromotionDto(
                            ticketPromotion.getId(),
                            ticketPromotion.getPromotion().getId(),
                            ticketPromotion.getPointsRewarded(),
                            0, null, ticketPromotion.getPromotion().getValidUntil(),
                            ticketPromotion.getPromotion().getValidUntil()
                    )));
        }};
    }

    private Collection<TicketProductDto> getTicketProductDtoCollection(Ticket ticket) {
        return new ArrayList<TicketProductDto>() {{
            ticket.getTicketProducts().forEach(
                    ticketProduct -> add(new TicketProductDto(ticketProduct))
            );
        }};
    }

    public Collection<TicketDto> findAllByClientAccountId(long id) {
        return new ArrayList<TicketDto>() {{
            ticketRepository.findAllByClientAccountId(id).forEach(
                    ticket -> add(new TicketDto(ticket)));
        }};
    }
}
