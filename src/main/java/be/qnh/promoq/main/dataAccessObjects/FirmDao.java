package be.qnh.promoq.main.dataAccessObjects;

import be.qnh.promoq.main.models.Firm;

import java.util.Collection;

public interface FirmDao {
    Firm save(Firm firm);

    Firm findById(long id);

    Collection<Firm> findAll();

    void delete(long id);
}
