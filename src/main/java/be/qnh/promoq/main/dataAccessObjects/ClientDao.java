package be.qnh.promoq.main.dataAccessObjects;

import be.qnh.promoq.main.models.Client;

public interface ClientDao {
    Client findById(long id);

    Client save(Client client);
}
