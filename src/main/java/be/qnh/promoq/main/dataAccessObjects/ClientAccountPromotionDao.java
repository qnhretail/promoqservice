package be.qnh.promoq.main.dataAccessObjects;

import be.qnh.promoq.main.models.ClientAccountPromotion;

public interface ClientAccountPromotionDao {
    ClientAccountPromotion save(ClientAccountPromotion clientAccountPromotion);
}
