package be.qnh.promoq.main.exceptions;

import be.qnh.promoq.main.Constants;

public class duplicateSerialException extends Throwable {
    public duplicateSerialException(String serialNr) {
        super(serialNr + " " + Constants.API_ERROR_ALREADY_IN_USE);
    }
}
