package be.qnh.promoq.main.exceptions;

import be.qnh.promoq.main.Constants;

public class RegisterNotFoundException extends Throwable {
    public RegisterNotFoundException(String serialNumber) {
        super(Constants.STRING_REGISTER_WITH_SERIAL + serialNumber + " was not found.");
    }
}
