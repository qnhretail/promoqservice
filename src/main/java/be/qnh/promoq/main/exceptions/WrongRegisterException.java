package be.qnh.promoq.main.exceptions;

import be.qnh.promoq.main.Constants;

public class WrongRegisterException extends Throwable {
    public WrongRegisterException(String clientId) {
        super(Constants.STRING_REGISTER_WITH_SERIAL + clientId + " is not registered in one of your firms!");
    }
}
