package be.qnh.promoq.main.enums;

public enum TicketStatus {

    S10(10, "wacht op registratie"),
    S15(15, "wordt geregistreerd"),
    S20(20, "wacht op identificatie klant"),
    S25(25, "identificatie klant wordt gescand"),
    S30(30, "wacht op verwerking"),
    S35(35, "wordt verwerkt"),
    S40(40, "wacht op afrekening"),
    S45(45, "wordt afgerekend"),
    S46(46, "wacht op koppeling klantenkaart"),
    S47(47, "gekoppeld aan klantenkaart"),
    S50(50, "wacht op afhandeling"),
    S55(55, "wordt afgehandeld"),
    S60(60, "afgehandeld"),
    S70(70, "cancelled");

    private final int nummer;
    private final String omschrijving;

    TicketStatus(int nummer, String omschrijving) {
        this.nummer = nummer;
        this.omschrijving = omschrijving;
    }

    public int nummer() {
        return nummer;
    }

    public String omschrijving() {
        return omschrijving;
    }
}
