package be.qnh.promoq.main.models;

import be.qnh.promoq.main.enums.Method;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.Date;

@Entity
@Component
public class RegisterLog {
    private long id;
    private Method method;
    private String request;
    private String object;
    private Date created;

    public RegisterLog(Method method, String request, String object) {
        this.method = method;
        this.request = request;
        this.object = object;
    }

    public RegisterLog() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Lob
    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    @PrePersist
    protected void onCreate() {
        created = new Date();
    }
}
