package be.qnh.promoq.main.models;

import be.qnh.promoq.api.dataTransferObjects.SmtpServerConfigurationDto;
import be.qnh.promoq.api.dataTransferObjects.SmtpServerCredentialsDto;
import org.springframework.stereotype.Component;

import javax.persistence.*;

@Entity
@Component
public class SmtpServerConfiguration {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String username;
    private String password;
    private String host;
    private short port;
    private boolean auth;
    private boolean startTls;
    @OneToOne
    private OwnerAccount ownerAccount;

    public SmtpServerConfiguration(String host, short port, boolean auth, boolean startTls, OwnerAccount ownerAccount) {
        this.host = host;
        this.port = port;
        this.auth = auth;
        this.startTls = startTls;
        this.ownerAccount = ownerAccount;
    }

    public SmtpServerConfiguration() {
    }

    public SmtpServerConfiguration(SmtpServerConfigurationDto smtpServerConfigurationDto, OwnerAccount ownerAccount) {
        this(smtpServerConfigurationDto.getHost(),
                smtpServerConfigurationDto.getPort(),
                smtpServerConfigurationDto.isAuth(),
                smtpServerConfigurationDto.isStartTls(),
                ownerAccount);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public short getPort() {
        return port;
    }

    public void setPort(short port) {
        this.port = port;
    }

    public boolean isAuth() {
        return auth;
    }

    public void setAuth(boolean auth) {
        this.auth = auth;
    }

    public boolean isStartTls() {
        return startTls;
    }

    public void setStartTls(boolean startTls) {
        this.startTls = startTls;
    }

    public OwnerAccount getOwnerAccount() {
        return ownerAccount;
    }

    public void setOwnerAccount(OwnerAccount ownerAccount) {
        this.ownerAccount = ownerAccount;
    }

    public void setCredentials(SmtpServerCredentialsDto credentials) {
        this.setUsername(credentials.getUsername());
        this.setPassword(credentials.getPassword());
    }
}
