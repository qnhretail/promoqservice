package be.qnh.promoq.main.models.helpers;

import be.qnh.promoq.main.dataAccessObjects.ClientAccountFirmDaoImpl;
import be.qnh.promoq.main.models.ClientAccountFirm;
import be.qnh.promoq.main.models.Ticket;

public class ClientAccountFirmHelper {
    public static void substractCredit(Ticket ticket) {
        ClientAccountFirmDaoImpl clientAccountFirmDao = new ClientAccountFirmDaoImpl();

        Float ticketDiscount = ticket.getTicketDiscount();
        ClientAccountFirm clientAccountFirm = clientAccountFirmDao.findByClientAccountAndFirm(
                ticket.getClient().getAccount(),
                ticket.getRegisterAccount().getBranch().getFirm());
        clientAccountFirm.setCredit(clientAccountFirm.getCredit() - ticketDiscount);
        ClientAccountFirmBlockedCreditHelper.deleteBlockedCredit(ticket.getRegisterAccount());
        clientAccountFirmDao.save(clientAccountFirm);
    }
}
