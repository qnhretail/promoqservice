package be.qnh.promoq.main.models;

import be.qnh.promoq.security.models.Account;
import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Date;

@Entity
@Component
public class CreditTransfer extends Transfer {
    private Float quantityF;
    @ManyToOne
    private Firm firm;

    public CreditTransfer(
            Account reqAccount,
            ClientAccount clientAccount,
            Firm firm,
            Float amount,
            String reason) {
        this.quantityF = amount;
        this.reason = reason;
        this.assignedBy = reqAccount;
        this.clientAccount = clientAccount;
        this.firm = firm;
        this.dateTime = new Date();
    }

    public CreditTransfer() {
    }

    public Float getQuantity() {
        return quantityF;
    }

    public void setQuantity(Float quantity) {
        this.quantityF = quantity;
    }

    public Firm getFirm() {
        return firm;
    }

    public void setFirm(Firm firm) {
        this.firm = firm;
    }
}
