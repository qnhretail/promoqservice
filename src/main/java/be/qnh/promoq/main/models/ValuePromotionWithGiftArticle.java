package be.qnh.promoq.main.models;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Collection;
import java.util.Date;

@Entity
public class ValuePromotionWithGiftArticle extends ValuePromotion {
    private GiftArticle giftArticle;

    public ValuePromotionWithGiftArticle(
            String name,
            String description,
            Date validFrom,
            Date validUntil,
            Date validateUntil,
            boolean firmPromotion,
            Firm firm,
            Collection<Branch> branches,
            long valueOfPoint,
            long valueGift,
            GiftArticle giftArticle) {
        super(name, description, validFrom, validUntil, validateUntil, firmPromotion, firm, branches, valueOfPoint, valueGift);
        this.giftArticle = giftArticle;
    }

    public ValuePromotionWithGiftArticle() {
    }

    @ManyToOne
    @JoinColumn(name = "gift_article_id")
    public GiftArticle getGiftArticle() {
        return giftArticle;
    }

    public void setGiftArticle(GiftArticle giftArticle) {
        this.giftArticle = giftArticle;
    }
}
