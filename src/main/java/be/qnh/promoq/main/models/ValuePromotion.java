package be.qnh.promoq.main.models;

import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import java.util.Collection;
import java.util.Date;

/**
 * The type Value promotion.
 */
@Entity
@Component
public class ValuePromotion extends Promotion {

    private long valueOfPoint;
    private long valueGift;

    public ValuePromotion() {
    }

    public ValuePromotion(String name,
                          String description,
                          Date validFrom,
                          Date validUntil,
                          Date validateUntil,
                          boolean firmPromotion,
                          Firm firm,
                          Collection<Branch> branches,
                          long valueOfPoint,
                          long valueGift) {
        super(name, description, validFrom, validUntil, validateUntil, firmPromotion, firm, branches);
        this.valueOfPoint = valueOfPoint;
        this.valueGift = valueGift;
    }

    public long getValueOfPoint() {
        return valueOfPoint;
    }

    public void setValueOfPoint(long valueOfPoint) {
        this.valueOfPoint = valueOfPoint;
    }

    public long getValueGift() {
        return valueGift;
    }

    public void setValueGift(long valueGift) {
        this.valueGift = valueGift;
    }
}
