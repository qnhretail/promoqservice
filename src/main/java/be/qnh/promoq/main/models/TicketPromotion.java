package be.qnh.promoq.main.models;

import org.springframework.stereotype.Component;

import javax.persistence.*;

@Entity
@Component
public class TicketPromotion {
    private long id;
    private Promotion promotion;
    private long pointsRewarded;
    private Ticket ticket;

    public TicketPromotion(Promotion promotion, Ticket ticket, long pointsRewarded) {
        this.promotion = promotion;
        this.ticket = ticket;
        this.pointsRewarded = pointsRewarded;
    }

    public TicketPromotion() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @ManyToOne
    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    @ManyToOne
    public Promotion getPromotion() {
        return promotion;
    }

    public void setPromotion(Promotion promotion) {
        this.promotion = promotion;
    }

    public long getPointsRewarded() {
        return pointsRewarded;
    }

    public void setPointsRewarded(long pointsRewarded) {
        this.pointsRewarded = pointsRewarded;
    }
}
