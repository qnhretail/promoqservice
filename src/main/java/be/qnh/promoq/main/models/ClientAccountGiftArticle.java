package be.qnh.promoq.main.models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class ClientAccountGiftArticle {
    @EmbeddedId
    public ClientAccountGiftArticleKey id = new ClientAccountGiftArticleKey();

    @MapsId("clientAccountId")
    @ManyToOne
    private ClientAccount clientAccount;
    @MapsId("giftArticleId")
    @ManyToOne
    private GiftArticle giftArticle;
    private long quantity;

    public ClientAccountGiftArticle(ClientAccount clientAccount, GiftArticle giftArticle, long quantity) {
        this.clientAccount = clientAccount;
        this.giftArticle = giftArticle;
        this.quantity = quantity;
    }

    public ClientAccountGiftArticle() {
    }

    public ClientAccount getClientAccount() {
        return clientAccount;
    }

    public void setClientAccount(ClientAccount clientAccount) {
        this.clientAccount = clientAccount;
    }

    public GiftArticle getGiftArticle() {
        return giftArticle;
    }

    public void setGiftArticle(GiftArticle giftArticle) {
        this.giftArticle = giftArticle;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public void addOne() {
        this.quantity++;
    }

    public void addQuantity(long quantity) {
        this.quantity += quantity;
    }
}

@Embeddable
class ClientAccountGiftArticleKey implements Serializable {
    private long clientAccountId;
    private long giftArticleId;
}
