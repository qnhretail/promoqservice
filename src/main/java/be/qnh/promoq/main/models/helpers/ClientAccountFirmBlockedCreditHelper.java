package be.qnh.promoq.main.models.helpers;

import be.qnh.promoq.main.dataAccessObjects.ClientAccountFirmBlockedCreditDaoImpl;
import be.qnh.promoq.main.models.ClientAccountFirm;
import be.qnh.promoq.main.models.ClientAccountFirmBlockedCredit;
import be.qnh.promoq.main.models.RegisterAccount;

public class ClientAccountFirmBlockedCreditHelper {
    public static Float getBlockedCredit(ClientAccountFirm clientAccountFirm, RegisterAccount registerAccount) {
        ClientAccountFirmBlockedCreditDaoImpl clientAccountFirmBlockedCreditDao = new ClientAccountFirmBlockedCreditDaoImpl();
        Float total = 0F;
        for (ClientAccountFirmBlockedCredit blockedCredit :
                clientAccountFirm.getClientAccountFirmBlockedCredits()) {
            if (blockedCredit.getRegisterAccount() == registerAccount) {
                clientAccountFirmBlockedCreditDao.delete(blockedCredit);
            } else {
                total += blockedCredit.getBlockedCredit();
            }
        }
        return total;
    }

    public static void deleteBlockedCredit(RegisterAccount registerAccount) {
        ClientAccountFirmBlockedCreditDaoImpl clientAccountFirmBlockedCreditDao = new ClientAccountFirmBlockedCreditDaoImpl();
        ClientAccountFirmBlockedCredit blockedCredit = clientAccountFirmBlockedCreditDao.findFirstByRegisterAccount(registerAccount);
        if (blockedCredit != null) {
            clientAccountFirmBlockedCreditDao.delete(blockedCredit);
        }
    }
}
