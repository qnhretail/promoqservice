package be.qnh.promoq.main.models;

import javax.persistence.Entity;
import java.util.Collection;
import java.util.Date;

@Entity
public class ValuePromotionDirectDisc extends ValuePromotion {
    private float giftAmount;


    public ValuePromotionDirectDisc(
            String name,
            String description,
            Date validFrom,
            Date validUntil,
            boolean firmPromotion,
            Firm firm,
            Collection<Branch> branches,
            long valueOfPoint,
            long valueGift,
            float giftAmount) {
        super(name, description, validFrom, validUntil, null, firmPromotion, firm, branches, valueOfPoint, valueGift);
        this.giftAmount = giftAmount;
    }

    public ValuePromotionDirectDisc() {
    }

    public ValuePromotionDirectDisc(ValuePromotion valuePromotion, float giftAmount) {
        super(valuePromotion.getName(),
                valuePromotion.getDescription(),
                valuePromotion.getValidFrom(),
                valuePromotion.getValidUntil(),
                null,
                valuePromotion.getFirmPromotion(),
                valuePromotion.getFirm(),
                valuePromotion.getBranches(),
                valuePromotion.getValueOfPoint(),
                valuePromotion.getValueGift());
        this.giftAmount = giftAmount;
    }

    public float getGiftAmount() {
        return giftAmount;
    }

    public void setGiftAmount(float giftAmount) {
        this.giftAmount = giftAmount;
    }
}
