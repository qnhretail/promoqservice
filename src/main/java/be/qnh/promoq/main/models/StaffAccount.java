package be.qnh.promoq.main.models;

import be.qnh.promoq.api.dataTransferObjects.NewStaffAccountDto;
import be.qnh.promoq.main.dataAccessObjects.FirmDaoImpl;
import be.qnh.promoq.security.dataAccessObjects.RoleDaoImpl;
import be.qnh.promoq.security.models.Account;
import be.qnh.promoq.security.models.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Table(name = "StaffAccount")
@Component
public class StaffAccount extends Account {
    @Transient
    private static FirmDaoImpl firmDao;
    @Transient
    private static RoleDaoImpl roleDao;

    //@Column(unique = true)
    private String email;
    private String name;
    private String surname;
    @ManyToOne
    @JoinColumn(name = "firm_id")
    private Firm firm;
    @ManyToMany()
    @JoinTable(name = "staff_accounts_branches",
            joinColumns = @JoinColumn(
                    name = "account_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "branch_id", referencedColumnName = "id"))
    private Collection<Branch> branches;
    public StaffAccount(String username, String password, ArrayList<Role> roles, String email) {
        super(username, password, roles);
        this.email = email;
    }

    public StaffAccount() {
    }

    public StaffAccount(String username, String password, ArrayList<Role> roles, String email, String name,
                        String surname, Firm firm) {
        super(username, password, roles);
        this.email = email;
        this.name = name;
        this.surname = surname;
        this.firm = firm;
    }

    public StaffAccount(NewStaffAccountDto account) {
        super(account.getUsername(), account.getPassword(), roleDao.findByName(account.getRoles()));
        this.email = account.getEmail();
        this.name = account.getName();
        this.surname = account.getSurname();
        this.firm = firmDao.findById(account.getFirmId());
    }

    @Autowired
    public void wireDaos(FirmDaoImpl firmdoa, RoleDaoImpl roleDao) {
        StaffAccount.firmDao = firmdoa;
        StaffAccount.roleDao = roleDao;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Collection<Branch> getBranches() {
        return branches;
    }

    public void setBranches(Collection<Branch> branches) {
        this.branches = branches;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void addBranch(Branch branch) {
        if (this.branches == null) this.branches = new ArrayList<Branch>();
        this.branches.add(branch);
    }

    public void deleteBranch(Branch branch) {
        this.branches.remove(branch);
    }

    public Firm getFirm() {
        return firm;
    }

    public void setFirm(Firm firm) {
        this.firm = firm;
    }
}
