package be.qnh.promoq.main.models;

import be.qnh.promoq.main.enums.TicketStatus;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Entity
@Component
public class Ticket {
    private long id;
    private RegisterAccount registerAccount;
    private Client client;
    private String staffMemeber;
    private long ticketNumber;
    private Date dateTime;
    private Collection<TicketProduct> ticketProducts;
    private Collection<TicketPromotion> ticketPromotions;
    private TicketStatus ticketStatus;
    private Float TicketDiscount;

    public Ticket(TicketStatus ticketStatus) {
        this();
        this.ticketStatus = ticketStatus;
    }

    public Ticket() {
        this.dateTime = new Date();
    }

    public Ticket(RegisterAccount registerAccount, Client client, String staffMember, long ticketNr,
                  Date dateTime, Collection<TicketProduct> ticketProducts,
                  Collection<TicketPromotion> ticketPromotions, TicketStatus ticketStatus) {
        this();
        this.registerAccount = registerAccount;
        this.client = client;
        this.staffMemeber = staffMember;
        this.ticketNumber = ticketNr;
        //this.dateTime = dateTime;
        this.ticketProducts = ticketProducts;
        this.ticketPromotions = ticketPromotions;
        this.ticketStatus = ticketStatus;

    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @ManyToOne
    public RegisterAccount getRegisterAccount() {
        return registerAccount;
    }

    public void setRegisterAccount(RegisterAccount registerAccount) {
        this.registerAccount = registerAccount;
    }

    @ManyToOne
    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getStaffMemeber() {
        return staffMemeber;
    }

    public void setStaffMemeber(String staffMemeber) {
        this.staffMemeber = staffMemeber;
    }

    public long getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(long ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    @OneToMany(mappedBy = "ticket")
    public Collection<TicketProduct> getTicketProducts() {
        return ticketProducts;
    }

    public void setTicketProducts(Collection<TicketProduct> ticketProducts) {
        this.ticketProducts = ticketProducts;
    }

    public TicketStatus getTicketStatus() {
        return ticketStatus;
    }

    public void setTicketStatus(TicketStatus ticketStatus) {
        this.ticketStatus = ticketStatus;
    }

    @OneToMany(mappedBy = "ticket")
    public Collection<TicketPromotion> getTicketPromotions() {
        return ticketPromotions;
    }

    public void setTicketPromotions(Collection<TicketPromotion> ticketPromotions) {
        this.ticketPromotions = ticketPromotions;
    }

    public Float getTicketDiscount() {
        return TicketDiscount;
    }

    public void setTicketDiscount(Float ticketDiscount) {
        TicketDiscount = ticketDiscount;
    }
}