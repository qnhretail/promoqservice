package be.qnh.promoq.main.models;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Entity(name = "gift_articles")
@Component
public class GiftArticle {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(unique = true)
    private String name;
    @ManyToMany
    @JoinTable(name = "gift_articles_products",
            joinColumns = @JoinColumn(
                    name = "gift_article_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "product_id", referencedColumnName = "id"))
    private Collection<Product> products;
    @OneToMany(mappedBy = "giftArticle")
    private Collection<ValuePromotionWithGiftArticle> promotions;
    @ManyToOne
    private Firm firm;
    @ManyToMany
    @JoinTable(name = "gift_articles_client_accounts",
            joinColumns = @JoinColumn(
                    name = "gift_article_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "client_account_id", referencedColumnName = "id"
            ))
    private Collection<ClientAccount> clientAccounts;

    public GiftArticle(String name, List<Product> products, Firm firm) {
        this.name = name;
        this.products = products;
        this.firm = firm;
    }

    public GiftArticle() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<Product> getProducts() {
        return products;
    }

    public void setProducts(Collection<Product> products) {
        this.products = products;
    }

    public Collection<ValuePromotionWithGiftArticle> getPromotions() {
        return promotions;
    }

    public void setPromotions(Collection<ValuePromotionWithGiftArticle> promotions) {
        this.promotions = promotions;
    }

    public Firm getFirm() {
        return firm;
    }

    public void setFirm(Firm firm) {
        this.firm = firm;
    }

    public Collection<ClientAccount> getClientAccounts() {
        return clientAccounts;
    }

    public void setClientAccounts(Collection<ClientAccount> clientAccounts) {
        this.clientAccounts = clientAccounts;
    }
}
