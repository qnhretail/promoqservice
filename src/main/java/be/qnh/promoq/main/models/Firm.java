package be.qnh.promoq.main.models;

import be.qnh.promoq.security.models.Account;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Entity(name = "firms")
public class Firm {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String name;
    @ManyToOne
    @JsonIgnore
    private Account createdBy;
    @ManyToOne
    private Account assignedBy;
    @ManyToOne
    private OwnerAccount assignedTo;
    @OneToMany(mappedBy = "firm", cascade = CascadeType.REMOVE)
    private Collection<Branch> branches;
    @OneToMany(mappedBy = "firm")
    private Collection<Promotion> promotions;
    @OneToMany(mappedBy = "firm")
    private Collection<StaffAccount> staff;
    @OneToMany(mappedBy = "firm")
    private Collection<CreditTransfer> creditTransfers;
    private Date created;
    private Date updated;
    private boolean active = true;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @PrePersist
    protected void onCreate() {
        created = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        updated = new Date();
    }

    public Collection<Branch> getBranches() {
        return branches;
    }

    public void setBranches(Collection<Branch> branches) {
        this.branches = branches;
    }

    public long getId() {
        return id;
    }

    public Account getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Account createdBy) {
        this.createdBy = createdBy;
    }

    public Account getAssignedBy() {
        return assignedBy;
    }

    public void setAssignedBy(Account assignedBy) {
        this.assignedBy = assignedBy;
    }

    public Account getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(OwnerAccount assignedTo) {
        this.assignedTo = assignedTo;
    }

    public Collection<Account> getAssignedToCreatedByAssignedBy() {
        return new ArrayList<Account>() {{
            add(getAssignedTo());
            add(getAssignedBy());
            add(getCreatedBy());
        }};
    }

    public Collection<Promotion> getPromotions() {
        return promotions;
    }

    public void setPromotions(Collection<Promotion> promotions) {
        this.promotions = promotions;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public Collection<StaffAccount> getStaff() {
        return staff;
    }

    public void setStaff(Collection<StaffAccount> staff) {
        this.staff = staff;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }

    public Collection<CreditTransfer> getCreditTransfers() {
        return creditTransfers;
    }

    public void setCreditTransfers(Collection<CreditTransfer> creditTransfers) {
        this.creditTransfers = creditTransfers;
    }
}
