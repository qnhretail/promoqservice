package be.qnh.promoq.main.models;

import be.qnh.promoq.api.dataTransferObjects.BranchDto;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Entity(name = "branches")
@Component
public class Branch {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String name;
    private String address;
    private String email;
    private String phone;
    private String site;
    @ManyToOne()
    @JoinColumn(name = "firm_id")
    private Firm firm;
    @OneToMany(mappedBy = "branch", cascade = CascadeType.REMOVE)
    private Collection<RegisterAccount> registerAccounts;
    private Date created;
    private Date updated;
    @ManyToMany(mappedBy = "branches")
    private Collection<StaffAccount> staffAccounts;
    @ManyToMany(mappedBy = "branches"
    )
    private Collection<Promotion> promotions;
    private boolean active = true;

    public Branch(BranchDto branch, Firm firm) {
        this.firm = firm;
        this.id = branch.getId();
        this.name = branch.getName();
        this.address = branch.getAddress();
        this.email = branch.getEmail();
        this.phone = branch.getPhone();
        this.site = branch.getSite();
    }

    public Branch(String name, Firm firm) {
        this.setName(name);
        this.setFirm(firm);
    }

    public Branch() {
    }

    public Branch(String name, String address, String email, String phone, String site, Firm firm) {
        this.name = name;
        this.address = address;
        this.email = email;
        this.phone = phone;
        this.site = site;
        this.firm = firm;
    }

    @PrePersist
    protected void onCreate() {
        created = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        updated = new Date();
    }

    public Firm getFirm() {
        return firm;
    }

    public void setFirm(Firm firm) {
        this.firm = firm;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<RegisterAccount> getRegisterAccounts() {
        return registerAccounts;
    }

    public void setRegisterAccounts(Collection<RegisterAccount> registerAccounts) {
        this.registerAccounts = registerAccounts;
    }

    public Collection<StaffAccount> getStaffAccounts() {
        return staffAccounts;
    }

    public void setStaffAccounts(Collection<StaffAccount> staffAccounts) {
        this.staffAccounts = staffAccounts;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public Collection<Promotion> getPromotions() {
        return promotions;
    }

    public void setPromotions(Collection<Promotion> promotions) {
        this.promotions = promotions;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }
}
