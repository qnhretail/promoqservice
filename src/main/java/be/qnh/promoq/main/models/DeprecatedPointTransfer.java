package be.qnh.promoq.main.models;

import be.qnh.promoq.security.models.Account;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.Date;

// This entity only exists for migration purposes from old db structure
@Component
@Entity
@Table(name = "point_transfer")
public class DeprecatedPointTransfer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private long quantity;
    private String reason;
    @ManyToOne
    protected ClientAccount clientAccount;
    @ManyToOne
    private Account assignedBy;
    @ManyToOne
    private Promotion promotion;
    protected Date dateTime;

    public DeprecatedPointTransfer() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public ClientAccount getClientAccount() {
        return clientAccount;
    }

    public void setClientAccount(ClientAccount clientAccount) {
        this.clientAccount = clientAccount;
    }

    public Account getAssignedBy() {
        return assignedBy;
    }

    public void setAssignedBy(Account assignedBy) {
        this.assignedBy = assignedBy;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public Promotion getPromotion() {
        return promotion;
    }

    public void setPromotion(Promotion promotion) {
        this.promotion = promotion;
    }
}
