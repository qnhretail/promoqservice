package be.qnh.promoq.main.models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class ClientAccountPromotion {
    @EmbeddedId
    public ClientAccountPromotionKey id = new ClientAccountPromotionKey();

    @MapsId("clientAccountId")
    @ManyToOne
    private ClientAccount clientAccount;
    @MapsId("promotionId")
    @ManyToOne
    private Promotion promotion;
    private long points;

    public ClientAccountPromotion(ClientAccount clientAccount, Promotion promotion) {
        this.clientAccount = clientAccount;
        this.promotion = promotion;
    }

    public ClientAccountPromotion() {
    }

    public ClientAccount getClientAccount() {
        return clientAccount;
    }

    public void setClientAccount(ClientAccount clientAccount) {
        this.clientAccount = clientAccount;
    }

    public Promotion getPromotion() {
        return promotion;
    }

    public void setPromotion(Promotion promotion) {
        this.promotion = promotion;
    }

    public long getPoints() {
        return points;
    }

    public void setPoints(long points) {
        this.points = points;
    }

    public void addPoints(Long amount) {
        points += amount;
    }

    public void deletePoints(long amount) {
        this.points -= amount;
    }

    public ClientAccountPromotionKey getId() {
        return id;
    }
}

@Embeddable
class ClientAccountPromotionKey implements Serializable {
    private long clientAccountId;
    private long promotionId;
}

