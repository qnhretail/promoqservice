package be.qnh.promoq.main.models;

import be.qnh.promoq.api.dataTransferObjects.ClientAccountDto;
import be.qnh.promoq.security.models.Account;
import be.qnh.promoq.security.models.Role;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.Collection;

@Entity
public class ClientAccount extends Account {
    //@Column(unique = true)
    private String name;
    private String email;
    private String phone;
    private String address;
    private String zipcode;
    private String town;
    private String note;
    @OneToMany(mappedBy = "clientAccount")
    private Collection<ClientAccountFirm> clientAccountsFirms;
    @OneToMany(mappedBy = "clientAccount")
    private Collection<ClientAccountPromotion> clientAccountsPromotions;
    @OneToMany(mappedBy = "clientAccount")
    private Collection<ClientAccountGiftArticle> clientAccountGiftArticles;
    @OneToMany(mappedBy = "clientAccount")
    private Collection<Transfer> transfers;

    public ClientAccount() {
        super();
    }

    public ClientAccount(String username, String email, String password,
                         String address, String zipcode, String town, String note, ArrayList<Role> roles) {
        super(username, password, roles);
        this.address = address;
        this.zipcode = zipcode;
        this.town = town;
        this.note = note;
        this.email = email;
    }

    public ClientAccount(String username, String email, String password, ArrayList<Role> roles) {
        super(username, password, roles);
        this.email = email;
    }

    public ClientAccount(ClientAccountDto clientAccountDto, ArrayList<Role> roles) {
        super(clientAccountDto.getUsername(),
                clientAccountDto.getPassword(),
                roles);
        this.name = clientAccountDto.getName();
        this.address = clientAccountDto.getAddress();
        this.zipcode = clientAccountDto.getZipCode();
        this.town = clientAccountDto.getTown();
        this.note = clientAccountDto.getNote();
        this.email = clientAccountDto.getEmail();
        this.phone = clientAccountDto.getPhone();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Collection<ClientAccountFirm> getClientAccountsFirms() {
        return clientAccountsFirms;
    }

    public void setClientAccountsFirms(Collection<ClientAccountFirm> clientAccountsFirms) {
        this.clientAccountsFirms = clientAccountsFirms;
    }

    public Collection<Firm> getFirms() {
        Collection<Firm> firms = new ArrayList<>();
        this.getClientAccountsFirms().forEach(x -> firms.add(x.getFirm()));
        return firms;
    }

    public Collection<ClientAccountPromotion> getClientAccountsPromotions() {
        return clientAccountsPromotions;
    }

    public void setClientAccountsPromotions(Collection<ClientAccountPromotion> clientAccountsPromotions) {
        this.clientAccountsPromotions = clientAccountsPromotions;
    }

    public Collection<ClientAccountGiftArticle> getClientAccountGiftArticles() {
        return clientAccountGiftArticles;
    }

    public void setClientAccountGiftArticles(Collection<ClientAccountGiftArticle> clientAccountGiftArticles) {
        this.clientAccountGiftArticles = clientAccountGiftArticles;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<Transfer> getTransfers() {
        return transfers;
    }

    public void setTransfers(Collection<Transfer> transfers) {
        this.transfers = transfers;
    }
}
