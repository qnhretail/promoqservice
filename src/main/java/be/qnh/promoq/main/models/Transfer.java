package be.qnh.promoq.main.models;

import be.qnh.promoq.security.models.Account;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.Date;

@Component
@Entity
public class Transfer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    protected String reason;
    @ManyToOne
    protected ClientAccount clientAccount;
    @ManyToOne
    protected Account assignedBy;
    protected Date dateTime;

    public Transfer() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public ClientAccount getClientAccount() {
        return clientAccount;
    }

    public void setClientAccount(ClientAccount clientAccount) {
        this.clientAccount = clientAccount;
    }

    public Account getAssignedBy() {
        return assignedBy;
    }

    public void setAssignedBy(Account assignedBy) {
        this.assignedBy = assignedBy;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }
}
