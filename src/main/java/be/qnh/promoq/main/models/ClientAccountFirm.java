package be.qnh.promoq.main.models;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

@Entity
@Component
public class ClientAccountFirm {
    @EmbeddedId
    public ClientAccountFirmKey id = new ClientAccountFirmKey();

    @MapsId("firmId")
    @ManyToOne
    private Firm firm;
    @MapsId("clientAccountId")
    @ManyToOne
    private ClientAccount clientAccount;
    @OneToMany(mappedBy = "clientAccountFirm")
    private Collection<ClientAccountFirmBlockedCredit> clientAccountFirmBlockedCredits;
    private boolean receivePromotions = false;
    private boolean receiveNewsletter = false;
    private Float credit;
    private Date created;
    private Date updated;

    public ClientAccountFirm() {
    }

    public ClientAccountFirm(ClientAccount clientAccount, Firm firm, boolean receivePromotions, boolean receiveNewsletter) {
        this();
        this.clientAccount = clientAccount;
        this.firm = firm;
        this.receivePromotions = receivePromotions;
        this.receiveNewsletter = receiveNewsletter;
    }

    @PrePersist
    protected void onCreate() {
        created = new Date();
        credit = 0F;
    }

    @PreUpdate
    protected void onUpdate() {
        updated = new Date();
    }

    public boolean isReceivePromotions() {
        return receivePromotions;
    }

    public void setReceivePromotions(boolean receivePromotions) {
        this.receivePromotions = receivePromotions;
    }

    public boolean isReceiveNewsletter() {
        return receiveNewsletter;
    }

    public void setReceiveNewsletter(boolean receiveNewsletter) {
        this.receiveNewsletter = receiveNewsletter;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public Firm getFirm() {
        return firm;
    }

    public void setFirm(Firm firm) {
        this.firm = firm;
    }

    public ClientAccount getClientAccount() {
        return clientAccount;
    }

    public void setClientAccount(ClientAccount clientAccount) {
        this.clientAccount = clientAccount;
    }

    public Float getCredit() {
        return credit;
    }

    public void setCredit(Float credit) {
        this.credit = credit;
    }

    public void addCredit(float giftAmount) {
        setCredit(getCredit() + giftAmount);
    }

    public Collection<ClientAccountFirmBlockedCredit> getClientAccountFirmBlockedCredits() {
        return clientAccountFirmBlockedCredits;
    }

    public void setClientAccountFirmBlockedCredits(Collection<ClientAccountFirmBlockedCredit> clientAccountFirmBlockedCredits) {
        this.clientAccountFirmBlockedCredits = clientAccountFirmBlockedCredits;
    }
}

@Embeddable
class ClientAccountFirmKey implements Serializable {
    private long firmId;
    private long clientAccountId;
}
