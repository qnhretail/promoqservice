package be.qnh.promoq.main.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class ClientAccountFirmBlockedCredit {
    @Id
    @GeneratedValue
    private long id;
    @ManyToOne
    private ClientAccountFirm clientAccountFirm;
    @ManyToOne
    private RegisterAccount registerAccount;
    private Float blockedCredit;

    public ClientAccountFirmBlockedCredit(
            ClientAccountFirm clientAccountFirm, RegisterAccount registerAccount, Float blockedCredit) {
        this.clientAccountFirm = clientAccountFirm;
        this.registerAccount = registerAccount;
        this.blockedCredit = blockedCredit;
    }

    public ClientAccountFirmBlockedCredit() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ClientAccountFirm getClientAccountFirm() {
        return clientAccountFirm;
    }

    public void setClientAccountFirm(ClientAccountFirm clientAccountFirm) {
        this.clientAccountFirm = clientAccountFirm;
    }

    public RegisterAccount getRegisterAccount() {
        return registerAccount;
    }

    public void setRegisterAccount(RegisterAccount registerAccount) {
        this.registerAccount = registerAccount;
    }

    public Float getBlockedCredit() {
        return blockedCredit;
    }

    public void setBlockedCredit(Float blockedCredit) {
        this.blockedCredit = blockedCredit;
    }
}
