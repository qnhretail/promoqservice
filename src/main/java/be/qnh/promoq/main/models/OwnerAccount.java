package be.qnh.promoq.main.models;

import be.qnh.promoq.security.models.Account;
import be.qnh.promoq.security.models.Role;
import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Table(name = "OwnerAccounts")
@Component
public class OwnerAccount extends Account {
    private int maxFirms;
    private int maxclients;
    private int maxAdmins;
    @OneToMany(mappedBy = "assignedTo")
    private Collection<Firm> firms;

    public OwnerAccount(int maxFirms, int maxclients, int maxAdmins) {
        this.maxFirms = maxFirms;
        this.maxclients = maxclients;
        this.maxAdmins = maxAdmins;
    }

    public OwnerAccount(String username, String password, int maxFirms, int maxclients, int maxAdmins) {
        super(username, password);
        this.maxFirms = maxFirms;
        this.maxclients = maxclients;
        this.maxAdmins = maxAdmins;
    }

    public OwnerAccount(String username, String password, ArrayList<Role> roles, int maxFirms, int maxclients, int maxAdmins) {
        super(username, password, roles);
        this.maxFirms = maxFirms;
        this.maxclients = maxclients;
        this.maxAdmins = maxAdmins;
    }

    public OwnerAccount() {
    }

    public int getMaxFirms() {
        return maxFirms;
    }

    public void setMaxFirms(int maxFirms) {
        this.maxFirms = maxFirms;
    }

    public int getMaxclients() {
        return maxclients;
    }

    public void setMaxclients(int maxclients) {
        this.maxclients = maxclients;
    }

    public int getMaxAdmins() {
        return maxAdmins;
    }

    public void setMaxAdmins(int maxAdmins) {
        this.maxAdmins = maxAdmins;
    }

    public Collection<Firm> getFirms() {
        return firms;
    }

    public void setFirms(Collection<Firm> firms) {
        this.firms = firms;
    }
}
