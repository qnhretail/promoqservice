package be.qnh.promoq.main.models.helpers;

import be.qnh.promoq.main.dataAccessObjects.ClientAccountFirmDaoImpl;
import be.qnh.promoq.main.models.*;

public class TicketHelper {

    public static Float getTicketValue(Ticket ticket) {
        Float value = 0F;
        for (TicketProduct tp : ticket.getTicketProducts()) {
            value += tp.getPrice();
        }
        return value;
    }

    public static Float getTicketValueInclDiscount(Ticket ticket) {
        Float value = 0F;
        for (TicketProduct tp : ticket.getTicketProducts()) {
            value += tp.getPrice();
        }
        return value;
    }

    public static Float getPossibleDiscount(Ticket ticket) {
        ClientAccountFirmDaoImpl clientAccountFirmDao = new ClientAccountFirmDaoImpl();
        ClientAccountFirm clientAccountFirm = clientAccountFirmDao.findByClientAccountAndFirm(
                ticket.getClient().getAccount(), ticket.getRegisterAccount().getBranch().getFirm());
        Float ticketValue = getTicketValue(ticket);
        Float clientAccountFirmCredit = clientAccountFirm.getCredit();
        Float blockedCredit = ClientAccountFirmBlockedCreditHelper.getBlockedCredit(clientAccountFirm,
                ticket.getRegisterAccount());
        clientAccountFirmCredit -= blockedCredit;
        return (ticketValue <= clientAccountFirmCredit) ? ticketValue : clientAccountFirmCredit;
    }

    public static void blockDiscount(Ticket ticket) {
        ClientAccountFirmDaoImpl clientAccountFirmDao = new ClientAccountFirmDaoImpl();

        Float discount = ticket.getTicketDiscount();
        ClientAccountFirm clientAccountFirm = clientAccountFirmDao.findByClientAccountAndFirm(
                ticket.getClient().getAccount(), ticket.getRegisterAccount().getBranch().getFirm());
        RegisterAccount registerAccount = ticket.getRegisterAccount();
        ClientAccountFirmBlockedCredit clientAccountFirmBlockedCredit =
                new ClientAccountFirmBlockedCredit(clientAccountFirm, registerAccount, discount);
    }
}
