package be.qnh.promoq.main.models;

import be.qnh.promoq.main.exceptions.duplicateSerialException;
import be.qnh.promoq.security.dataAccessObjects.RoleDaoImpl;
import be.qnh.promoq.security.models.Account;
import be.qnh.promoq.security.models.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Entity(name="registers")
@Table(name = "RegisterAccounts")
@Component
public class RegisterAccount extends Account {
    @Transient
    private static RoleDaoImpl roleDao;

    private Date created;
    private Date updated;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String description;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "branch_id")
    private Branch branch;
    @OneToMany(mappedBy = "registerAccount")
    private Collection<Ticket> tickets;
    @OneToMany(mappedBy = "registerAccount")
    private Collection<ClientAccountFirmBlockedCredit> clientAccountFirmBlockedCredits;

    public RegisterAccount() {
        super();
    }

    public RegisterAccount(String description, String serialNr, String password, Branch branch) {
        super(serialNr, password, new ArrayList<Role>() {{
            add(roleDao.findByName("ROLE_REGISTER"));
        }});
        this.description = description;
        this.branch = branch;
    }

    @PostConstruct
    public void init() {
    }

    @Autowired
    public void setServletContext(RoleDaoImpl roleRepository) {
        RegisterAccount.roleDao = roleRepository;
    }

    @PrePersist
    protected void onCreate() {
        created = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        updated = new Date();
    }

    public long getId() {

        return id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSerialNr() {
        return getUsername();
    }

    public void setSerialNr(String serialNr) throws duplicateSerialException {
        setUsername(serialNr);
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public Collection<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(Collection<Ticket> tickets) {
        this.tickets = tickets;
    }

    public Collection<ClientAccountFirmBlockedCredit> getClientAccountFirmBlockedCredits() {
        return clientAccountFirmBlockedCredits;
    }

    public void setClientAccountFirmBlockedCredits(Collection<ClientAccountFirmBlockedCredit> clientAccountFirmBlockedCredits) {
        this.clientAccountFirmBlockedCredits = clientAccountFirmBlockedCredits;
    }
}
