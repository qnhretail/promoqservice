package be.qnh.promoq.main.models;

import be.qnh.promoq.security.models.Account;
import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Date;

@Entity
@Component
public class PointTransfer extends Transfer {
    private long quantity;
    @ManyToOne
    private Promotion promotion;

    public PointTransfer(Promotion promotion, Account reqAccount, ClientAccount clientAccount, long amount, String reason) {
        this.quantity = amount;
        this.reason = reason;
        this.assignedBy = reqAccount;
        this.clientAccount = clientAccount;
        this.dateTime = new Date();
        this.promotion = promotion;
    }

    public PointTransfer(Promotion promotion, Account reqAccount, ClientAccount clientAccount, long amount, String reason, Date date) {
        this.quantity = amount;
        this.reason = reason;
        this.assignedBy = reqAccount;
        this.clientAccount = clientAccount;
        this.dateTime = date;
        this.promotion = promotion;
    }

    public PointTransfer() {
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }


    public Promotion getPromotion() {
        return promotion;
    }

    public void setPromotion(Promotion promotion) {
        this.promotion = promotion;
    }

}
