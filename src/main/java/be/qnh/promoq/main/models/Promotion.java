package be.qnh.promoq.main.models;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

/**
 * The type Promotion.
 */
@Entity(name = "promotions")
@Inheritance
@Component
public class Promotion {

    private long id;
    private String name;
    private String description;
    private Date validFrom;
    private Date validUntil;
    private Date validateUntil;
    private Collection<PromotionCondition> promotionConditions;
    private boolean firmPromotion;
    private Firm firm;
    private Collection<Branch> branches;
    private Date created;
    private Date updated;
    private Collection<ClientAccountPromotion> clientAccountsPromotions;

    public Promotion() {
    }

    public Promotion(String name, String description, Date validFrom, Date validUntil, Date validateUntil,
                     boolean firmPromotion, Firm firm, Collection<Branch> branches) {
        this.name = name;
        this.description = description;
        this.validFrom = validFrom;
        this.validUntil = validUntil;
        this.validateUntil = validateUntil;
        this.firmPromotion = firmPromotion;
        this.firm = firm;
        this.branches = branches;
    }

    @PrePersist
    protected void onCreate() {
        created = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        updated = new Date();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(Date validUntil) {
        this.validUntil = validUntil;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @ManyToMany()
    @JoinTable(name = "promotions_branches",
            joinColumns = @JoinColumn(
                    name = "promotion_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "branch_id", referencedColumnName = "id"))
    public Collection<Branch> getBranches() {
        return branches;
    }

    public void setBranches(Collection<Branch> branches) {
        this.branches = branches;
    }

    @ManyToOne()
    @JoinColumn(name = "firm_id")
    public Firm getFirm() {
        return firm;
    }

    public void setFirm(Firm firm) {
        this.firm = firm;
    }

    public Date getValidateUntil() {
        return validateUntil;
    }

    public void setValidateUntil(Date validateUntil) {
        this.validateUntil = validateUntil;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getFirmPromotion() {
        return firmPromotion;
    }

    public void setFirmPromotion(boolean firmPromotion) {
        this.firmPromotion = firmPromotion;
    }

    public void deleteBranch(Branch branch) {
        this.branches.remove(branch);
    }

    @OneToMany(mappedBy = "promotion")
    public Collection<PromotionCondition> getPromotionConditions() {
        return promotionConditions;
    }

    public void setPromotionConditions(Collection<PromotionCondition> promotionConditions) {
        this.promotionConditions = promotionConditions;
    }

    @OneToMany(mappedBy = "promotion")
    public Collection<ClientAccountPromotion> getClientAccountsPromotions() {
        return clientAccountsPromotions;
    }

    public void setClientAccountsPromotions(Collection<ClientAccountPromotion> clientAccountsPromotions) {
        this.clientAccountsPromotions = clientAccountsPromotions;
    }
}
