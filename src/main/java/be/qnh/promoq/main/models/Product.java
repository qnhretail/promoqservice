package be.qnh.promoq.main.models;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Component
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(unique = true)
    private String productCode;
    private String name;
    @ManyToMany(mappedBy = "products")
    private Collection<GiftArticle> giftArticles;
    @ManyToOne
    private Firm firm;
    private Long articleGroup;

    public Product(String productCode, String name, Firm firm) {
        this.productCode = productCode;
        this.name = name;
        this.firm = firm;
    }

    public Product() {
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<GiftArticle> getGiftArticles() {
        return giftArticles;
    }

    public void setGiftArticles(Collection<GiftArticle> giftArticles) {
        this.giftArticles = giftArticles;
    }

    public Firm getFirm() {
        return firm;
    }

    public void setFirm(Firm firm) {
        this.firm = firm;
    }

    public Long getArticleGroup() {
        return articleGroup;
    }

    public void setArticleGroup(Long articleGroup) {
        this.articleGroup = articleGroup;
    }
}
