package be.qnh.promoq.main.models;


import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class PromoqServiceApplication {

    @Id
    private long id;
    private String version;
    private long databaseVersion;

    public PromoqServiceApplication() {
    }

    public PromoqServiceApplication(long id, String version, long databaseVersion) {
        this.id = id;
        this.version = version;
        this.databaseVersion = databaseVersion;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public long getDatabaseVersion() {
        return databaseVersion;
    }

    public void setDatabaseVersion(long databaseVersion) {
        this.databaseVersion = databaseVersion;
    }
}
