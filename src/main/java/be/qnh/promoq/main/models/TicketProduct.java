package be.qnh.promoq.main.models;

import org.springframework.stereotype.Component;

import javax.persistence.*;

@Entity
@Component
public class TicketProduct {
    private Product product;
    private String description;
    private long articleGroup;
    private float price;
    private float priceInclDiscount;
    private long id;
    private Ticket ticket;

    public TicketProduct(Ticket ticket, Product product, String description, long articleGroup, float price, float priceInclDiscount) {
        this.ticket = ticket;
        this.product = product;
        this.description = description;
        this.product.setArticleGroup(articleGroup);
        this.price = price;
        this.priceInclDiscount = priceInclDiscount;
    }

    public TicketProduct() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @ManyToOne
    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    @ManyToOne
    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getArticleGroup() {
        return articleGroup;
    }

    public void setArticleGroup(long articleGroup) {
        this.articleGroup = articleGroup;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getPriceInclDiscount() {
        return priceInclDiscount;
    }

    public void setPriceInclDiscount(float priceInclDiscount) {
        this.priceInclDiscount = priceInclDiscount;
    }
}
