package be.qnh.promoq.main.factories;

import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Base64;

public final class Base64Factory {

    public static Object convertResourceImageToBase64String(String imageFileName) {
        try {
            ClassPathResource in = new ClassPathResource(imageFileName);
            File file = in.getFile();
            try (FileInputStream imageInFile = new FileInputStream(file)) {
                byte imageData[] = new byte[(int) file.length()];
                imageInFile.read(imageData);
                return Base64.getEncoder().encodeToString(imageData);
            } catch (IOException x) {
                return null;
            }
        } catch (Exception x) {
            return null;
        }
    }
}
