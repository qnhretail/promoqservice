package be.qnh.promoq.main.factories;

import be.qnh.promoq.main.Constants;
import be.qnh.promoq.main.models.SmtpServerConfiguration;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.Properties;

public class MailFactory {

    private final Session session;
    private final SmtpServerConfiguration smtpServerconfiguration;

    public MailFactory(SmtpServerConfiguration smtpServerConfiguration) {
        this.smtpServerconfiguration = smtpServerConfiguration;
        this.session = Session.getInstance(this.initProperties(), this.initAuthenticator());
    }

    private Authenticator initAuthenticator() {
        return new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(
                        smtpServerconfiguration.getUsername(),
                        smtpServerconfiguration.getPassword());
            }
        };
    }

    private Properties initProperties() {
        Properties properties = new Properties();
        properties.put(Constants.SMTP_HOST, smtpServerconfiguration.getHost());
        properties.put(Constants.SMTP_PORT, smtpServerconfiguration.getPort());
        properties.put(Constants.SMTP_AUTH, smtpServerconfiguration.isAuth());
        properties.put(Constants.SMTP_SSL_ENABLED, smtpServerconfiguration.isStartTls());
        return properties;
    }

    public boolean sendMessage(String recipient, String subject, String text) {
        Message message = new MimeMessage(session);
        try {
            message.setFrom(new InternetAddress(smtpServerconfiguration.getUsername()));
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
            message.setSentDate(new Date());
            message.setSubject(subject);
            message.setText(text);
            Transport.send(message);
        } catch (MessagingException e) {
            return false;
        }
        return true;
    }
}
