package be.qnh.promoq.main.config;

import be.qnh.promoq.main.Constants;
import be.qnh.promoq.main.dataAccessObjects.*;
import be.qnh.promoq.main.models.*;
import be.qnh.promoq.security.dataAccessObjects.AccountDaoImpl;
import be.qnh.promoq.security.dataAccessObjects.RoleDaoImpl;
import be.qnh.promoq.security.models.Account;
import be.qnh.promoq.security.models.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class SeedDatabase implements ApplicationRunner {

	private SimpleDateFormat formatter =
			new SimpleDateFormat(Constants.SIMPLE_DATE_FORMAT_PATTERM);

	@Autowired
    private RoleDaoImpl roleDao;
    @Autowired
    private FirmDaoImpl firmDao;
    @Autowired
    private AccountDaoImpl accountDao;
    @Autowired
    private BranchDaoImpl branchDao;
    @Autowired
    private RegisterAccountDaoImpl registerAccountdao;
    @Autowired
    private ProductDaoImpl productDao;
    @Autowired
    private GiftArticleDaoImpl giftArticleDao;
    @Autowired
	private SmtpServerConfigurationDao smtpServerConfigurationDao;
	@Autowired
	private PromotionDao promotionDao;
	@Autowired
	private PromoqServiceApplicationDaoImpl promoqServiceApplicationDao;
	@Autowired
	private ClientAccountFirmDaoImpl clientAccountFirmDao;

	@Value("${info.app.version}")
	private String version;
	@Value("${info.app.dbVersion}")
	private long dbVersion;
    @Autowired
    private TicketProductDaoImpl ticketProductDao;
    @Autowired
    private DeprecatedPointTransferDaoImpl deprecatedPointTransferDao;
    @Autowired
    private TransferDaoImpl transferDao;

	@Override
	public void run(ApplicationArguments arg0) throws Exception {

		PromoqServiceApplication promoqServiceApplication = promoqServiceApplicationDao.findById(1);
		if (promoqServiceApplication == null) {
			initDatabase();
			promoqServiceApplication = new PromoqServiceApplication(1, version, 0);
		}
		if (promoqServiceApplication.getDatabaseVersion() < 3) {
			initDbV1();
            initDbv2();
			initDbv3();
			promoqServiceApplication.setDatabaseVersion(3);
		}
        if (promoqServiceApplication.getDatabaseVersion() < 4) {
            initDbv4();
            promoqServiceApplication.setDatabaseVersion(4);
        }
        promoqServiceApplication.setVersion(version);

		promoqServiceApplicationDao.save(promoqServiceApplication);


	}

    private void initDbv4() {
        Iterable<DeprecatedPointTransfer> deprecatedPointTransfers = deprecatedPointTransferDao.findAll();
        deprecatedPointTransfers.forEach(deprecatedPointTransfer ->
                transferDao.save(
                        new PointTransfer(
                                deprecatedPointTransfer.getPromotion(),
                                deprecatedPointTransfer.getAssignedBy(),
                                deprecatedPointTransfer.getClientAccount(),
                                deprecatedPointTransfer.getQuantity(),
                                null,
                                deprecatedPointTransfer.getDateTime())));
    }

	private void initDbv3() {
		ValuePromotion valuePromotion = (ValuePromotion) promotionDao.findById(1);
		if (valuePromotion != null) {
			promotionDao.save(new ValuePromotionDirectDisc(valuePromotion, 2.50F));
			promotionDao.delete(valuePromotion);
		}
	}

	private void initDbv2() {
		ticketProductDao.getAllTicketProducts().forEach((TicketProduct ticketProduct) -> {
            Product product = ticketProduct.getProduct();
            try {
                product.setArticleGroup(ticketProduct.getArticleGroup());
                productDao.save(product);
            } catch (Exception e) {
            }
        });
    }

	private void initDbV1() {
		updateBranch("Taste Away - Grand Bazar", "Beddenstraat 2, 2000 Antwerpen", "03 233 71 54",
				"http://www.bakkerij-wouters.be/vestiging/taste-away-grand-bazar-shopping-antwerpen/");
		updateBranch("Merksem", "Lambrechtshoeklaan 109, 2170 Merksem", "03 646 70 46",
				"http://www.bakkerij-wouters.be/vestiging/merksem/");
		updateBranch("Brasschaat", "Miksebaan 152, 2930 Brasschaat", "03 653 41 51",
				"http://www.bakkerij-wouters.be/vestiging/brasschaat/");
		updateBranch("Mariaburg", "Schriek 366, 2180 Mariaburg", "03 665 41 42",
				"http://www.bakkerij-wouters.be/vestiging/mariaburg/");
		updateBranch("Stabroek", "Dorpstraat 51, 2940 Stabroek", "03 568 65 50",
				"http://www.bakkerij-wouters.be/vestiging/stabroek/");
		updateBranch("Wuustwezel Gooreind", "Kerkplaats 5, 2990 Wuustwezel", "03 663 03 70",
				"http://www.bakkerij-wouters.be/vestiging/wuustwezel-gooreind/");
		updateBranch("Wuustwezel centrum", "Bredabaan 353, 2990 Wuustwezel", "03 636 30 32",
				"http://www.bakkerij-wouters.be/vestiging/wuustwezel/");
		updateBranch("Kalmthout", "Kapellensteenweg 169, 2920 Kalmthout", "03 666 16 69",
				"http://www.bakkerij-wouters.be/vestiging/kalmthout/");
		updateBranch("Nieuwmoer", "Nieuwmoer-Dorp 26, 2920 Kalmthout", "03 667 32 56",
				"http://www.bakkerij-wouters.be/vestiging/nieuwmoer/");
		updateBranch("Putte", "Ertbrandstraat 288, 2950 Kapellen", "03 337 35 25",
				"http://www.bakkerij-wouters.be/vestiging/putte/");
	}

	private void updateBranch(String name, String address, String phone, String site) {
        Branch branch = branchDao.findByName(name);
        branch.setAddress(address);
		branch.setPhone(phone);
		branch.setSite(site);
        branchDao.save(branch);
    }

	private void initDatabase() throws ParseException {

		Collection<Role> newRoles = new ArrayList<>();
        if (roleDao.count() == 0) {
            for (String roleString:
					Arrays.asList(
							Constants.ROLE_ADMIN,
							Constants.ROLE_OWNER,
							Constants.ROLE_STAFF,
							Constants.ROLE_ROOT,
							Constants.ROLE_CLIENT,
							Constants.API_ROLE_REGISTER,
							Constants.ROLE_NONE,
							Constants.ROLE_ACTUATOR
					)) {
				newRoles.add(new Role(roleString));
			}
            roleDao.save(newRoles);
        }

        if (accountDao.count() <= 0) {
            List<Account> accounts = new ArrayList<Account>() {{

				add(new OwnerAccount("qnh", "qnh2018", new ArrayList<Role>() {{
					add(roleDao.findByName(Constants.ROLE_OWNER));
					add(roleDao.findByName(Constants.ROLE_ACTUATOR));
				}}, Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE));
				add(new Account(
						Constants.STRING_NONE,
						Constants.STRING_NONE,
						new ArrayList<Role>() {{
							add(roleDao.findByName(Constants.ROLE_NONE));
						}}));
			}};
            accountDao.save(accounts);
        }


		// init Wouters
        Firm firm = firmDao.findByName("Wouters");
        if (firm == null) {
			firm = new Firm();
            firm.setAssignedTo((OwnerAccount) accountDao.findByUsername("qnh"));
            firm.setName("Wouters");
            firmDao.save(firm);

			// SMTP configuratie
			SmtpServerConfiguration smtpServerConfiguration =
					new SmtpServerConfiguration("smtp.gmail.com", (short) 587, true, true,
                            ((OwnerAccount) accountDao.findByUsername("qnh")));
            smtpServerConfiguration.setUsername("promoqservice@gmail.com");
			smtpServerConfiguration.setPassword("12398765");
			smtpServerConfigurationDao.save(smtpServerConfiguration);

			// Add branches to Wouters
			int kassaNr = 1;
			for (Branch branch :
					Arrays.asList(
							new Branch("Taste Away - Grand Bazar",
									firm),
							new Branch("Merksem",
									firm),
							new Branch("Brasschaat",
									firm),
							new Branch("Mariaburg", firm),
							new Branch("Stabroek", firm),
							new Branch("Wuustwezel Gooreind", firm),
							new Branch("Wuustwezel centrum", firm),
							new Branch("Kalmthout", firm),
							new Branch("Nieuwmoer", firm),
							new Branch("Putte", firm)
					)) {
                branchDao.save(branch);

				// Add registers to branch
                registerAccountdao.save(new RegisterAccount(branch.getName() + " - Kassa 1",
                        "KW00" + kassaNr, "password", branch));
				kassaNr++;
			}

			Firm finalFirm = firm;
			Collection<Product> tegoedBonnen = new ArrayList<Product>() {{
				add(new Product("2", "Tegoedbon 2.50 euro", finalFirm));
			}};
            productDao.save(tegoedBonnen);

			Collection<GiftArticle> giftArticles = new ArrayList<GiftArticle>() {{
				add(new GiftArticle(
						"Spaaractie 2.50 euro",
                        Collections.singletonList(productDao.findByName("Tegoedbon 2.50 euro")),
                        finalFirm));
			}};
            giftArticleDao.save(giftArticles);

			promotionDao.save(
					new ValuePromotionWithGiftArticle(
							"2,50 euro waardebon",
							"ontvang een waardebon van 2,50 euro bij iedere totale aankoopschijf van 250 euro",
							formatter.parse("01/01/2018"),
							formatter.parse("01/01/2099"),
							formatter.parse("01/01/2099"),
							true,
							firmDao.findByName("Wouters"),
							null,
							1l,
							250l,
                            giftArticleDao.findByName("Spaaractie 2.50 euro")
                    ));

            accountDao.save(new StaffAccount("wouters", "w1234",
                    new ArrayList<Role>() {{
						add(roleDao.findByName(Constants.ROLE_STAFF));
					}}, "wouters@wouters.be",
                    "", "", firmDao.findByName("Wouters")));
            accountDao.save(new ClientAccount("peterDebrier", "peter.debrier@qnh.be", "p1234",
                    new ArrayList<Role>() {{
						add(roleDao.findByName(Constants.ROLE_CLIENT));
					}}));
            clientAccountFirmDao.save(new ClientAccountFirm((ClientAccount) accountDao.findByUsername("peterDebrier"),
                    firmDao.findByName("Wouters"), true, true));
        }
	}
}
