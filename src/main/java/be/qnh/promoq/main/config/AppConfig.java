package be.qnh.promoq.main.config;

import be.qnh.promoq.main.Constants;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableAsync
@EnableSwagger2
public class AppConfig extends WebMvcConfigurerAdapter {

    // Locales Configuration
    @Bean
    public ReloadableResourceBundleMessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("classpath:messages");
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }

    @Bean
    public LocaleChangeInterceptor localeInterceptor() {
        LocaleChangeInterceptor interceptor = new LocaleChangeInterceptor();
        interceptor.setParamName("lang");
        return interceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(localeInterceptor());
    }


    // SWAGGER Configuration

    @Bean
    public Docket userApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("userApi")
                .select()
                .apis(RequestHandlerSelectors.basePackage("be.qnh.promoq"))
                .paths(regex(Constants.API_ROOT + ".*"))
                .build();

    }

    @Bean
    public Docket registerApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("registerApi")
                .select()
                .apis(RequestHandlerSelectors.basePackage("be.qnh.promoq"))
                .paths(regex(Constants.API_ROOT + "/register.*"))
                .build();

    }
}
