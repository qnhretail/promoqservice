package be.qnh.promoq.main.repositories;

import be.qnh.promoq.main.models.ClientAccount;
import be.qnh.promoq.main.models.ClientAccountGiftArticle;
import be.qnh.promoq.main.models.GiftArticle;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface ClientAccountGiftArticleRepository extends CrudRepository<ClientAccountGiftArticle, Long> {
    Collection<ClientAccountGiftArticle> findByClientAccount(ClientAccount clientAccount);

    ClientAccountGiftArticle findByClientAccountAndGiftArticle_Id(ClientAccount clientAccount, long id);

    ClientAccountGiftArticle findById(ClientAccountGiftArticle clientAccountGiftArticle);

    ClientAccountGiftArticle findByClientAccountAndGiftArticle(ClientAccount clientAccount, GiftArticle giftArticle);
}
