package be.qnh.promoq.main.repositories;

import be.qnh.promoq.main.models.Branch;
import be.qnh.promoq.main.models.Firm;
import be.qnh.promoq.main.models.Promotion;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Date;

@Repository
public interface PromotionRepository extends CrudRepository<Promotion, Long> {

    Collection<Promotion> findByFirm(Firm firm);

    Collection<Promotion> findByFirmPromotionIsTrueAndFirm(Firm firm);

    Collection<? extends Promotion> findByBranchesContains(Branch branch);

    Collection<? extends Promotion> findByFirmPromotionIsTrueAndFirmAndValidFromIsLessThanEqualAndValidUntilGreaterThanEqual(
            Firm firm, Date validFrom, Date validUntil);

    Collection<? extends Promotion> findByBranchesContainsAndValidFromIsLessThanEqualAndValidUntilGreaterThanEqual(
            Branch branch, Date date, Date date1);

    Promotion findById(long id);
}
