package be.qnh.promoq.main.repositories;

import be.qnh.promoq.main.models.RegisterLog;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RegisterLogRepository extends CrudRepository<RegisterLog, Long> {
}
