package be.qnh.promoq.main.repositories;

import be.qnh.promoq.main.models.Client;
import be.qnh.promoq.main.models.ClientAccount;
import be.qnh.promoq.security.models.Account;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends CrudRepository<Client, Long> {
    Iterable<Client> findAllByAccount(Account account);

    Client findOneByCardId(String cardNr);

    Client findById(long id);

    Client findFirstByAccount(ClientAccount clientAccount);
}
