package be.qnh.promoq.main.repositories;

import be.qnh.promoq.main.models.Branch;
import be.qnh.promoq.main.models.Firm;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BranchRepository extends CrudRepository<Branch, Long> {
    Branch findById(long id);

    Iterable<Branch> findByFirm(Firm firm);

    Branch findByName(String name);

    Iterable<Branch> findByFirmAndActiveIsTrue(Firm firm);
}

