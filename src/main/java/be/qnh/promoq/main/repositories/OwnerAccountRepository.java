package be.qnh.promoq.main.repositories;

import be.qnh.promoq.main.models.Firm;
import be.qnh.promoq.main.models.OwnerAccount;
import be.qnh.promoq.security.repositories.AccountBaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OwnerAccountRepository extends AccountBaseRepository<OwnerAccount> {
    Iterable<OwnerAccount> findOwnerAccountsByFirmsContains(Firm firm);

    OwnerAccount findById(long ownerId);
}
