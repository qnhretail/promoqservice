package be.qnh.promoq.main.repositories;

import be.qnh.promoq.main.models.PromoqServiceApplication;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PromoqServiceApplicationRepository extends CrudRepository<PromoqServiceApplication, Long> {
    PromoqServiceApplication findById(long id);
}
