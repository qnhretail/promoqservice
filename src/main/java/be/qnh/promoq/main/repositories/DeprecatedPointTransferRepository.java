package be.qnh.promoq.main.repositories;

import be.qnh.promoq.main.models.DeprecatedPointTransfer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeprecatedPointTransferRepository extends CrudRepository<DeprecatedPointTransfer, Long> {
}
