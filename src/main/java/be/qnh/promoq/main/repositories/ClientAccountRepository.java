package be.qnh.promoq.main.repositories;

import be.qnh.promoq.main.models.ClientAccount;
import be.qnh.promoq.security.repositories.AccountBaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientAccountRepository extends AccountBaseRepository<ClientAccount> {
    ClientAccount findByUsername(String username);

    ClientAccount findByEmail(String email);
}
