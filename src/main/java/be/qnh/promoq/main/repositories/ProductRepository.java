package be.qnh.promoq.main.repositories;

import be.qnh.promoq.main.models.Firm;
import be.qnh.promoq.main.models.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {
    Product findByName(String name);

    Product findById(Long id);

    Product findFirstByProductCode(String plu);

    Product findByProductCode(String plu);

    Collection<Product> findByFirm(Firm firm);
}
