package be.qnh.promoq.main.repositories;

import be.qnh.promoq.main.models.Ticket;
import be.qnh.promoq.main.models.TicketProduct;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface TicketProductRepository extends CrudRepository<TicketProduct, Long> {
    Collection<TicketProduct> findByTicket(Ticket ticket);

    Collection<TicketProduct> findByTicketId(long id);
}
