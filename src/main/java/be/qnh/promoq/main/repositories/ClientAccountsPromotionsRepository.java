package be.qnh.promoq.main.repositories;

import be.qnh.promoq.main.models.ClientAccount;
import be.qnh.promoq.main.models.ClientAccountPromotion;
import be.qnh.promoq.main.models.Promotion;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface ClientAccountsPromotionsRepository extends CrudRepository<ClientAccountPromotion, Long> {
    ClientAccountPromotion findByClientAccountAndPromotion(ClientAccount clientAccount, Promotion promotion);

    Collection<ClientAccountPromotion> findByClientAccount(ClientAccount account);

    ClientAccountPromotion findByClientAccountIdAndPromotionId(long clientAccountId, long promotionId);
}
