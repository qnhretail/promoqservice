package be.qnh.promoq.main.repositories;

import be.qnh.promoq.main.models.Firm;
import be.qnh.promoq.security.models.Account;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FirmRepository extends CrudRepository<Firm, Long> {
    Firm findById(long id);

    Iterable<Firm> findFirmByAssignedToIsNull();


    Iterable<Firm> findFirmByAssignedToIsNullAndCreatedBy(Account account);

    Firm findByName(String name);

    Iterable<Firm> findFirmByCreatedByOrAssignedByOrAssignedTo(Account account, Account account1, Account account2);
}
