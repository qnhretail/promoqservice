package be.qnh.promoq.main.repositories;

import be.qnh.promoq.main.models.Branch;
import be.qnh.promoq.main.models.RegisterAccount;
import be.qnh.promoq.security.repositories.AccountBaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RegisterAccountRepository extends AccountBaseRepository<RegisterAccount> {

    Iterable<RegisterAccount> findAllByBranch(Branch branch);

    RegisterAccount findById(long id);
}
