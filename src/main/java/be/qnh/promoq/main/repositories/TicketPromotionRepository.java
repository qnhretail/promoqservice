package be.qnh.promoq.main.repositories;

import be.qnh.promoq.main.models.Ticket;
import be.qnh.promoq.main.models.TicketPromotion;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface TicketPromotionRepository extends CrudRepository<TicketPromotion, Long> {
    Collection<TicketPromotion> findByTicket(Ticket ticket);
}
