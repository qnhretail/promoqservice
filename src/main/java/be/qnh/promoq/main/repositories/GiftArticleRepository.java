package be.qnh.promoq.main.repositories;

import be.qnh.promoq.main.models.Firm;
import be.qnh.promoq.main.models.GiftArticle;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GiftArticleRepository extends CrudRepository<GiftArticle, Long> {
    GiftArticle findById(long id);

    Iterable<GiftArticle> findByFirm(Firm firm);

    GiftArticle findByName(String s);
}
