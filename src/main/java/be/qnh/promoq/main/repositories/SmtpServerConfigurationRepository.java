package be.qnh.promoq.main.repositories;

import be.qnh.promoq.main.models.OwnerAccount;
import be.qnh.promoq.main.models.SmtpServerConfiguration;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SmtpServerConfigurationRepository extends CrudRepository<SmtpServerConfiguration, Long> {
    SmtpServerConfiguration findByOwnerAccount(OwnerAccount ownerAccount);
}
