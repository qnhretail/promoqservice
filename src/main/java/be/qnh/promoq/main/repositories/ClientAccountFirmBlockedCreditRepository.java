package be.qnh.promoq.main.repositories;

import be.qnh.promoq.main.models.ClientAccountFirmBlockedCredit;
import be.qnh.promoq.main.models.RegisterAccount;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientAccountFirmBlockedCreditRepository extends CrudRepository<ClientAccountFirmBlockedCredit, Long> {
    ClientAccountFirmBlockedCredit findFirstByRegisterAccount(RegisterAccount registerAccount);
}
