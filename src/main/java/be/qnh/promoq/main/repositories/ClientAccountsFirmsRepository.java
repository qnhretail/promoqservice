package be.qnh.promoq.main.repositories;

import be.qnh.promoq.main.models.ClientAccount;
import be.qnh.promoq.main.models.ClientAccountFirm;
import be.qnh.promoq.main.models.Firm;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientAccountsFirmsRepository extends CrudRepository<ClientAccountFirm, Long> {
    Iterable<ClientAccountFirm> findAllByFirm(Firm firm);

    Iterable<ClientAccountFirm> findAllByClientAccountId(long id);

    ClientAccountFirm findByFirmIdAndClientAccountId(long firmId, long clientId);

    ClientAccountFirm findByClientAccountAndFirm(ClientAccount clientAccount, Firm firm);

    ClientAccountFirm findByClientAccountIdAndFirmId(long clientAccountId, long firmId);
}
