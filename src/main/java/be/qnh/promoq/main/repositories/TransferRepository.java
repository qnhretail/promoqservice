package be.qnh.promoq.main.repositories;

import be.qnh.promoq.main.models.CreditTransfer;
import be.qnh.promoq.main.models.PointTransfer;
import be.qnh.promoq.main.models.Transfer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface TransferRepository extends CrudRepository<Transfer, Long> {
    Collection<Transfer> findByClientAccountId(long clientAccountId);

    Collection<PointTransfer> findAllPointTransfersByClientAccountId(long clientAccountId);

    Collection<CreditTransfer> findAllCreditTransfersByClientAccountId(long clientAccountId);
}
