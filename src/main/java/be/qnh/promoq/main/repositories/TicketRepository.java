package be.qnh.promoq.main.repositories;

import be.qnh.promoq.main.models.Ticket;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface TicketRepository extends CrudRepository<Ticket, Long> {
    Ticket findById(long id);

    Collection<Ticket> findAllByClientAccountId(long id);
}
