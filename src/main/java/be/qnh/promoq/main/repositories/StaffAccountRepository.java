package be.qnh.promoq.main.repositories;

import be.qnh.promoq.main.models.Branch;
import be.qnh.promoq.main.models.StaffAccount;
import be.qnh.promoq.security.repositories.AccountBaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StaffAccountRepository extends AccountBaseRepository<StaffAccount> {
    Iterable<StaffAccount> findStaffAccountByBranchesContains(Branch branch);

    StaffAccount findStaffAccountById(long id);
}
