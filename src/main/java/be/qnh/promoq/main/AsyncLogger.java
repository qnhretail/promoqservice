package be.qnh.promoq.main;

import be.qnh.promoq.main.dataAccessObjects.RegisterLogDaoImpl;
import be.qnh.promoq.main.enums.Method;
import be.qnh.promoq.main.models.RegisterLog;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class AsyncLogger {
    @Autowired
    private RegisterLogDaoImpl registerLogDao;

    @Async
    public void logger(Method method, String request, Object object) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        this.registerLogDao.save(new RegisterLog(method, request, mapper.writeValueAsString(object)));
    }

}
