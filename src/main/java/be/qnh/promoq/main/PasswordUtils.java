package be.qnh.promoq.main;

import org.apache.commons.lang.RandomStringUtils;

public class PasswordUtils {
    public static String generate() {
        return RandomStringUtils.random(20, true, true);
    }
}
