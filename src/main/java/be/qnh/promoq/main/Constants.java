package be.qnh.promoq.main;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

public interface Constants {

    String SIMPLE_DATE_FORMAT_PATTERM = "dd/MM/yyyy";

    // ROLES
    String ROLE_NONE = "ROLE_NONE";
    String ROLE_ACTUATOR = "ROLE_ACTUATOR";
    String ROLE_ROOT = "ROLE_ROOT";
    String ROLE_OWNER = "ROLE_OWNER";
    String ROLE_ADMIN = "ROLE_ADMIN";
    String ROLE_CLIENT = "ROLE_CLIENT";
    String ROLE_STAFF = "ROLE_STAFF";

    // API
    String API_ROOT = "/api";
    String API_ROLE_OWNER = "/owner";
    String API_ROLE_ROOT = "/root";
    String API_ROLE_STAFF = "/staff";
    String API_ROLE_CLIENT = "/client";
    String API_ROLE_ADMIN = "/admin";
    String API_ROLE_REGISTER = "/register";

    String API_ERROR_ALREADY_IN_USE = "is already in use";
    String API_ERROR_DUPLICATE_USERNAME = "Username " + API_ERROR_ALREADY_IN_USE;
    String API_ERROR_DUPLICATE_EMAIL = "Email " + API_ERROR_ALREADY_IN_USE;

    // SMTP
    String SMTP_ROOT_PROP = "mail.smtp";
    String SMTP_HOST = SMTP_ROOT_PROP + ".host";
    String SMTP_PORT = SMTP_ROOT_PROP + ".port";
    String SMTP_AUTH = SMTP_ROOT_PROP + ".auth";
    String SMTP_SSL_ENABLED = SMTP_ROOT_PROP + ".starttls.enable";


    String BRANCH_ = "Branch ";
    String _SUCCESSFULLY_DELETED = " Successfully deleted";

    String STRING_NONE = "none";
    String STRING_REGISTER_WITH_SERIAL = "Register with serialNr. ";
}
