package be.qnh.promoq.main;

import be.qnh.promoq.api.dataTransferObjects.TicketDto;
import be.qnh.promoq.api.dataTransferObjects.TicketProductDto;
import be.qnh.promoq.main.dataAccessObjects.*;
import be.qnh.promoq.main.models.*;
import be.qnh.promoq.main.models.helpers.ClientAccountFirmHelper;
import be.qnh.promoq.main.models.helpers.TicketHelper;
import be.qnh.promoq.security.dataAccessObjects.AccountDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import static be.qnh.promoq.main.enums.TicketStatus.*;

@Component
public class AsyncTicketProcessor {
    @Autowired
    private TicketDaoImpl ticketDao;
    @Autowired
    private AccountDaoImpl accountDao;
    @Autowired
    private ClientDaoImpl clientDao;
    @Autowired
    private PromotionDao promotionDao;
    @Autowired
    private TicketProductDaoImpl ticketProductDao;
    @Autowired
    private TicketPromotionDaoImpl ticketPromotionDao;
    @Autowired
    private ProductDaoImpl productDao;
    @Autowired
    private ClientAccountPromotionDaoImpl clientAccountPromotionDao;

    @Async
    public void registerTicket(TicketDto ticketDto, Ticket ticket) throws Exception {
        try {
            ticket.setTicketStatus(S15);
            ticketDao.save(ticket);
            ticket = ticketDao.save(this.registerOriginalTicket(ticket, ticketDto));
            // get ticket client
            String clientCardNr = ticketDto.getClientCardNr();
            if (clientCardNr != null) ticket.setClient(clientDao.findByCardId(ticketDto.getClientCardNr()));
            ticket.setTicketStatus(S35);
            ticketDao.save(ticket);
            // get promotions
            // firm promotions
            ticket = ticketDao.save(this.registerTicketPromotions(ticket));
        } catch (Exception ex) {
            ticket.setTicketStatus(S70);
            ticketDao.save(ticket);
        }
    }

    private Ticket registerOriginalTicket(Ticket ticket, TicketDto ticketDto) {
        ticket.setStaffMemeber(ticketDto.getStaffMember());
        ticket.setTicketNumber(ticketDto.getTicketNr());
        ticket.setRegisterAccount(accountDao.findRegisterAccountBySerialNr(ticketDto.getRegisterSerialNr()));
        ticket.setClient(clientDao.findByCardId(ticketDto.getClientCardNr()));
        ticket = this.registerTicketProducts(ticket, ticketDto);
        ticket.setTicketStatus(S25);
        return ticket;
    }

    private Ticket registerTicketProducts(Ticket ticket, TicketDto ticketDto) {
        for (TicketProductDto ticketProductDto : ticketDto.getArticles()) {
            TicketProduct ticketProduct = new TicketProduct(
                    ticket,
                    this.findOrGenerateProduct(ticketProductDto, ticket.getRegisterAccount().getBranch().getFirm()),
                    ticketProductDto.getDescription(),
                    ticketProductDto.getArticleGroup(),
                    ticketProductDto.getPrice(),
                    ticketProductDto.getPriceInclDiscount());
            ticketProductDao.save(ticketProduct);
        }
        return ticket;
    }

    private Product findOrGenerateProduct(TicketProductDto ticketProductDto, Firm firm) {
        Product product = productDao.findByProductCode(ticketProductDto.getPlu());
        return product == null ?
                productDao.save(new Product(ticketProductDto.getPlu(), ticketProductDto.getDescription(), firm)) :
                product;
    }

    private Ticket registerTicketPromotions(Ticket ticket) {
        Collection<Promotion> possiblePromotions = new ArrayList<>();
        Collection<Promotion> promotionCollection = new ArrayList<>();

        // save firm and branch promotions to possiblePromotions
        Date now = new Date();
        Branch branch = ticket.getRegisterAccount().getBranch();
        possiblePromotions.addAll(promotionDao.findValidFirmPromotions(branch.getFirm(), now));
        possiblePromotions.addAll(promotionDao.findValidBranchPromotions(branch, now));

        promotionCollection.addAll(possiblePromotions);

        // register ticketPromotions
        promotionCollection.forEach((Promotion promotion) -> {
            // If the promotion of type ValuePromotion
            if (promotion instanceof ValuePromotion) {
                this.registerValuePromotion(promotion, ticket);
            }
        });

        // register discount
        ticket.setTicketDiscount(TicketHelper.getPossibleDiscount(ticket));
        TicketHelper.blockDiscount(ticket);

        ticket.setTicketStatus(S40);
        return ticket;
    }

    private void registerValuePromotion(Promotion promotion, Ticket ticket) {
        Float ticketValue = TicketHelper.getTicketValueInclDiscount(ticket);
        long pointsRewarded = (long) (ticketValue / ((ValuePromotion) promotion).getValueOfPoint());
        ticketPromotionDao.save(new TicketPromotion(promotion, ticket, pointsRewarded));
    }

    @Async
    public void settleTicket(Ticket ticket) {
        ticket.setTicketStatus(S50);

        ClientAccountFirmHelper.substractCredit(ticket);

        ticketDao.save(ticket);
        this.settlePromotions(ticket);
        ticket.setTicketStatus(S60);
        ticketDao.save(ticket);
    }

    private void settlePromotions(Ticket ticket) {
        ClientAccount clientAccount = ticket.getClient().getAccount();
        for (TicketPromotion ticketPromotion : ticketPromotionDao.findByTicket(ticket)) {
            Promotion promotion = ticketPromotion.getPromotion();

            // If the promotion of type ValuePromotion
            if (promotion instanceof ValuePromotion) {
                this.settleValuePromotion(ticketPromotion, clientAccount);
            }

        }
    }

    private void settleValuePromotion(TicketPromotion ticketPromotion, ClientAccount clientAccount) {
        clientAccountPromotionDao.addClientAccountPromotionPoints(
                clientAccount,
                ticketPromotion.getPromotion(),
                ticketPromotion.getPointsRewarded());
    }
}
