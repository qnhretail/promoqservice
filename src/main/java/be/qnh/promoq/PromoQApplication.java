package be.qnh.promoq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

@SpringBootApplication
@EnableGlobalMethodSecurity
public class PromoQApplication {

	public static void main(String[] args) {
		SpringApplication.run(PromoQApplication.class, args);
	}
}
