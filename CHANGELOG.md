|| [English](CHANGELOG.md) || [Nederlands](CHANGELOG_nl.md) || [français](CHANGELOG_fr.md) ||

# Change Log

## 0.1.0 (2018-02-21)

- Response with password for email-free customers
- Phone number for customer account
- Scale company image on customer portal on small screens
- Email layout and text adjusted

### Bugfixes
- chrome popup will not show up with bad credentials

### Functionality
- manually assigned points can be requested
- ticket-details can be requested
- point history can be requested

## 0.0.1 Initial working version