|| [English](README.md) || [Nederlands](README_nl.md) || [français](README_fr.md) ||

# PromoQservice
PromoQservice est un service RESTful pour la gestion de systèmes promotionnels. Le service sert de backend pour le
gestion / site client comme pour le service lié aux systèmes de caisse.

## Installation
### Construire des profils
PromoQ est équipé de différents profils de construction. Le profil standard est destiné à un environnement de développement local. le profil peut être choisi en ajoutant `-Dspring.profiles.active = [profile]` à la commande de construction.

Profils disponibles:

* standard (sans ajout à la commande buid): localhost
* staging: pour sur le serveur de transfert (192.168.3.13).

## Lien vers les systèmes de caisse enregistreuse
### Authentification
l'authentification est utilisée comme authentification. Pour ce faire, le format suivant est utilisé dans l'en-tête Authentication: "Basic authenticationHash"
Le hachage d'authentification est un hachage base64 du nom d'utilisateur suivi d'un deux-points et du mot de passe.
Une fois connecté, le cookie JSESSIONID peut être utilisé pour l'authentification.
###La communication
la communication est effectuée via des appels AJAX avec des objets json. Les appels possibles peuvent être consultés et testés sur http://<service-url>/swagger-ui.html
! Note: Les appels effectués sur swagger sont en fait exécutés sur le serveur de production!

passer un ticket
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '<chaîne json comme ci-dessous>' 'http://localhost:8090/api/register/ticket'


    {
       "registerSerialNr": registre du numéro de série tel que spécifié dans le service (chaîne),
       "clientCardNr": numéro de carte client (chaîne),
       "staffMember": nomme le membre du personnel comme connecté, pour la journalisation (chaîne de caractères),
       "ticketNr": numéro de ticket enregistré dans la caisse enregistreuse (long int),
       "dateTime": ticket de production d'horodatage (date),
       "articles": [
         {
           "plu": code de produit (chaîne),
           "description": nom du produit dans la caisse enregistreuse (chaîne),
           "articleGroup": groupe d'éléments, si défini (long int),
           "price": prix normal (float),
           "priceInclDiscount": prix incluant les remises (float)
         }
       ],
     }

Si le service n'est pas disponible, un code 500 est donné en réponse, avec d'autres erreurs un code 4 **, seulement avec le code 200 la transaction est réussie. En réponse, un ticket vide avec uniquement ticketId et ticketStatus est renvoyé:

    {
      "ticketStatus": "S10",
      "id": 27,
      "registerSerialNr": null,
      "clientCardNr": null,
      "staffMember": null,
      "ticketNr": 0,
      "dateTime": null,
      "ticketProductDtos": null,
      "ticketPromotionDtos": null
    }
    
TicketStatuses avec leur signification:

* S10 (10, "en attente d'inscription"),
* S15 (15, "est enregistré"),
* S20 (20, "attente d'identification du client"),
* S25 (25, "l'identification du client est scannée"),
* S30 (30, "en attente de traitement"),
* S35 (35, "est en cours de traitement"),
* S40 (40, "en attente de règlement"),
* S45 (45, "est réglé"),
* S50 (50, "en attente d'achèvement"),
* S55 (55, "est manipulé"),
* S60 (60, "manipulé"),
* S70 (70, "annulé");

s70 n'est renvoyé que si quelque chose s'est mal passé ou si le billet a été annulé par la billetterie.
Demande de statut de ticket

    curl -X GET - header 'Accept: application/json' 'http://localhost:8090/api/register/ticket /<ticketId>'

jusqu'à et y compris s35 un ticket vide avec seulement ticketId et le statut est retourné. Au statut s40, le traitement du ticket se termine et le billet entier incluant les promotions et les remises est retourné.

    {
      "ticketStatus": null,
      "id": 122,
      "registerSerialNr": "001",
      "clientCardNr": "Client00001",
      "staffMember": "Joris",
      "ticketNr": 2,
      "dateTime": 1511881142508,
      "articles": [
        {
          "id": 5,
          "plu": "PLU0004",
          "description": "produit utile",
          "articleGroup": 5,
          "price": 5,22,
          "priceInclDiscount": 2,55
        }
      ],
      "promotions": [
        {
          "id": 8,
          "promotionId": 2,
          "pointsRevenus": 55,
          "totalPoints": 500,
          "cadeauxArticles": [
            {
              "id": 1,
              "firmId": 2,
              "name": "5euro bon",
              "productPlus": [
                2,
                5
              ]
            }
          ],
          "validUntil": 1511881142481,
          "validateUntil": 1511881142481
        }
      ],
      "cadeauxArticles": [
        {
          "giftArticleId": 13,
          "name": "2,5 euro bon",
          "productPlus": [
            2,
            5
          ],
          "quantity": 2
        }
      ]
    }

Ticket de caisse
    
    curl -X POST - header 'Accept: application/json' 'http://localhost:8090/api/register/ ticket/settle/<TicketId>'

Après le règlement d'un ticket, celui-ci doit être transmis au service avec l'appel AJAX mentionné ci-dessus. Seul le numéro de ticket doit être transmis. Après cela, les points de service et les articles promotionnels seront calculés sur la base de points et liés au client. Seuls ticketId et status sont renvoyés.
Avec la récupération de l'état du ticket, le ticket traité peut à nouveau être obtenu à l'état S60.
Récompense de validation

    curl -X POST - header 'Accept: application/json' 'http://localhost:8090/api/register/ticket/settle/giftArticle/<clientCardNr>/<giftArticleId'

Si la récompense demandée n'est pas présente chez le client spécifié, un statut 406 (Non acceptable) est renvoyé. Avec le code 200 (Ok) la récompense a été retirée avec succès du client et peut être livrée par le caissier. le corps de la réponse affiche la récompense sélectionnée.

     {
       "giftArticleId": 13,
       "name": "2,5 euro bon",
       "productPlus": [
         2,
         5
       ]
     }


## la source
* [Portal](https://bitbucket.org/qnhretail/promoqportal)
* [Service](https://bitbucket.org/qnhretail/promoqservice)

## Visual Studio Team Services
* [TSVS](https://qnhomniqstore.visualstudio.com/QNH%20Promotion%20Service)
