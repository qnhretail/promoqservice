|| [English](README.md) || [Nederlands](README_nl.md) || [français](README_fr.md) ||

# PromoQservice
PromoQservice is een RESTful service voor het beheer van promotieSystemen. De service dient zowel als backend voor de 
beheer/klantensite als voor de service gekoppeld aan kassasystemen.

## Installatie
### Build profiles
PromoQ is voorzien van verschillende build profiles. Het standaard profiel is bedoeld voor op een locale 
ontwikkel-omgeving. het profile kan gekozen worden door `-Dspring.profiles.active=[profile]` toe te voegen aan het build commando.

Beschikbare profiles:

* standaard (zonder toevoeging aan buid commando): localhost 
* staging: voor op stagingserver (192.168.3.13)

## Koppeling aan kassasystemen
###Authentication
als authenticatie wordt basic authentication gebruikt. Hiervoor wordt in de Authentication header de volgende opmaak gebruikt: “Basic authenticationHash“
De authentication hash is een base64 hash van de gebruikersnaam gevolgd door een dubbelpunt en dan het wachtwoord.
eens ingelogd kan de JSESSIONID cookie gebruikt worden voor authenticatie. 
###Communicatie
de communicatie gebeurt door middel van AJAX-calls met json-objecten. De mogelijke calls kunnen bekeken en getest worden op http://<service-url>/swagger-ui.html
! Opgelet: Op de productieserver worden de op swagger uitgevoerde calls echt uitgevoerd !

doorgeven van een ticket
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '<json string zoals hieronder>' 'http://localhost:8090/api/register/ticket'

    {
      "registerSerialNr": volgnummer kassa zoals in service opgegeven (string),
      "clientCardNr": klantenkaartnummer (string),
      "staffMember": naam personeelslid zoals ingelogd, voor logging (string),
      "ticketNr": volgnummer ticket zoals geregistreerd in de kassa (long int),
      "dateTime": timestamp aanmaak ticket (date),
      "articles": [
        {
          "plu": productcode (string),
          "description": benaming van product in kassa (string),
          "articleGroup": artikelgroep, als ingesteld (long int),
          "price": normale prijs (float),
          "priceInclDiscount": prijs inclusief kortingen (float)
        }
      ],
    }

indien de service niet bereikbaar is wordt er een code 500 als antwoord gegeven, bij andere fouten een code 4**, enkel bij code 200 is de transactie gelukt. Als antwoord wordt er een leeg ticket met enkel ticketId en ticketStatus teruggegeven:

    {
      "ticketStatus": "S10",
      "id": 27,
      "registerSerialNr": null,
      "clientCardNr": null,
      "staffMember": null,
      "ticketNr": 0,
      "dateTime": null,
      "ticketProductDtos": null,
      "ticketPromotionDtos": null
    }
    
TicketStatussen met hun betekenis:

* S10(10, "wacht op registratie"),
* S15(15, "wordt geregistreerd"),
* S20(20, "wacht op identificatie klant"),
* S25(25, "identificatie klant wordt gescand"),
* S30(30, "wacht op verwerking"),
* S35(35, "wordt verwerkt"),
* S40(40, "wacht op afrekening"),
* S45(45, "wordt afgerekend"),
* S50(50, "wacht op afhandeling"),
* S55(55, "wordt afgehandeld"),
* S60(60, "afgehandeld"),
* S70(70, "cancelled");

s70 wordt enkel teruggegeven indien er iets mis liep of indien het ticket door de kassa geannuleerd werd.
Opvragen van ticket-status

    curl -X GET --header 'Accept: application/json' 'http://localhost:8090/api/register/ticket/<ticketId>'

tot en met s35 wordt er een leeg ticket met enkel ticketId en status teruggegeven. Bij status s40 is de verwerking van het ticket ten einde en wordt het volledige ticket inclusief promoties en kortingen teruggegeven.

    {
      "ticketStatus": null,
      "id": 122,
      "registerSerialNr": "001",
      "clientCardNr": "Client00001",
      "staffMember": "Joris",
      "ticketNr": 2,
      "dateTime": 1511881142508,
      "articles": [
        {
          "id": 5,
          "plu": "PLU0004",
          "description": "usefull product",
          "articleGroup": 5,
          "price": 5.22,
          "priceInclDiscount": 2.55
        }
      ],
      "promotions": [
        {
          "id": 8,
          "promotionId": 2,
          "pointsRewarded": 55,
          "totalPoints": 500,
          "giftArticles": [
            {
              "id": 1,
              "firmId": 2,
              "name": "5euro bon",
              "productPlus": [
                2,
                5
              ]
            }
          ],
          "validUntil": 1511881142481,
          "validateUntil": 1511881142481
        }
      ],
      "giftArticles": [
        {
          "giftArticleId": 13,
          "name": "2.5 euro bon",
          "productPlus": [
            2,
            5
          ],
          "quantity": 2
        }
      ]
    }

Afrekenen ticket
    
    curl -X POST --header 'Accept: application/json' 'http://localhost:8090/api/register/ticket/settle/<TicketId>'

Nadat een ticket afgerekend wordt moet dit doorgegeven worden aan de service met bovenstaande AJAX-call. Enkel het ticketnr moet doorgegeven worden. Hierna zal de service punten en promotieartikelen op basis van punten berekenen en aan de klant koppelen. er wordt enkel ticketId en status teruggegeven.
Met het opvragen van de ticketstatus kan opnieuw het verwerkt ticket bekomen worden bij status S60
Validatie beloning

    curl -X POST --header 'Accept: application/json' 'http://localhost:8090/api/register/ticket/settle/giftArticle/<clientCardNr>/<giftArticleId'

Indien de gevraagde beloning niet aanwezig is bij de opgegeven klant wordt er een status 406 (Not acceptable) teruggegeven. Bij code 200 (Ok) is de beloning met succes bij de klant weggenomen en kan deze afgegeven worden door de kassier. de response body geeft de geselecteerde beloning weer.

    {
      "giftArticleId": 13,
      "name": "2.5 euro bon",
      "productPlus": [
        2,
        5
      ]
    }


##source
* [Portal](https://bitbucket.org/qnhretail/promoqportal)
* [Service](https://bitbucket.org/qnhretail/promoqservice)

## Visual Studio Team Services
* [TSVS](https://qnhomniqstore.visualstudio.com/QNH%20Promotion%20Service)
