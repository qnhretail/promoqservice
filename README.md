|| [English](README.md) || [Nederlands](README_nl.md) || [français](README_fr.md) ||

# PromoQservice
PromoQservice is a RESTful service for the management of promotional systems. The service serves as a backend for the
management / customer site as for the service linked to cash register systems.

## Installation

### Building the jar file

To be able to build the package you need a maven installation. more information on installing and using Maven can be found on maven.apache.org.
To build the project run `mvn package` inside the project root folder. The jar file will be placed inside the `target/` folder.

### Running the service

For running the service all that needs to be done is to copy the jar file onto the server and run the following command.

`java.exe -jar -Dspring.profiles.active=staging "c:\pathTo\promoq-0.1.1.jar"`

PromoQ is equipped with different build profiles. The standard profile is intended for a local development environment. the profile can be chosen by adding `-Dspring.profiles.active = [profile]` to the  command. profile files can be found in `src/main/resources` as `.properties` files. 

Available profiles:

* standard (without addition to buid command): localhost
* staging: for on staging server (192.168.3.13).
* wouters

Profile files are used to change database properties, server port to deploy onto, ... The server does not depend on one specific type of database. It is already tested with MS SQL and MySQL.

## Link to cash register systems
### Authentication
authentication is used as authentication. For this purpose, the following format is used in the Authentication header: "Basic authenticationHash"
The authentication hash is a base64 hash of the username followed by a colon and then the password.
Once logged in, the JSESSIONID cookie can be used for authentication.
###Communication
communication is done through AJAX calls with json objects. The possible calls can be viewed and tested at http: // <service-url> /swagger-ui.html
! Note: The calls made on swagger are actually executed on the production server!

passing a ticket
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '<json string as below>' 'http://localhost:8090/api/register/ticket'

    {
       "registerSerialNr": serial number register as specified in service (string),
       "clientCardNr": customer card number (string),
       "staffMember": name staff member as logged in, for logging (string),
       "ticketNr": ticket number as registered in the cash register (long int),
       "dateTime": timestamp production ticket (date),
       "articles": [
         {
           "plu": product code (string),
           "description": name of product in cash register (string),
           "articleGroup": item group, if set (long int),
           "price": normal price (float),
           "priceInclDiscount": price including discounts (float)
         }
       ],
     }

If the service is not available, a code 500 is given as a reply, with other errors a code 4**, only with code 200 the transaction is successful. In response, an empty ticket with only ticketId and ticketStatus is returned:

    {
      "ticketStatus": "S10",
      "id": 27,
      "registerSerialNr": null,
      "clientCardNr": null,
      "staffMember": null,
      "ticketNr": 0,
      "dateTime": null,
      "ticketProductDtos": null,
      "ticketPromotionDtos": null
    }
    
TicketStatuses with their meaning:

* S10 (10, "waiting for registration")
* S15 (15, "is recorded")
* S20 (20, "waiting for customer identification")
* S25 (25, "customer identification is scanned")
* S30 (30, "waiting for processing")
* S35 (35, "is being processed")
* S40 (40, "waiting for settlement")
* S45 (45, "is settled")
* S46 (46, "Waiting for client card")
* S47 (47, "Registered to client card")
* S50 (50, "waiting for completion")
* S55 (55, "is handled")
* S60 (60, "handled")
* S70 (70, "canceled")

s70 is only returned if something went wrong or if the ticket was canceled by the box office.
Requesting ticket status

    curl -X GET - header 'Accept: application/json' 'http://localhost:8090/api/register/ticket /<ticketId>'

up to and including s35 an empty ticket with only ticketId and status is returned. At status s40 the processing of the ticket ends and the entire ticket including promotions and discounts is returned.

    {
      "ticketStatus": null,
      "id": 122,
      "registerSerialNr": "001",
      "clientCardNr": "Client00001",
      "staffMember": "Joris",
      "ticketNr": 2,
      "dateTime": 1511881142508,
      "articles": [
        {
          "id": 5,
          "plu": "PLU0004",
          "description": "usefull product",
          "articleGroup": 5,
          "price": 5.22,
          "priceInclDiscount": 2.55
        }
      ],
      "promotions": [
        {
          "id": 8,
          "promotionId": 2,
          "pointsRewarded": 55,
          "totalPoints": 500,
          "giftArticles": [
            {
              "id": 1,
              "firmId": 2,
              "name": "5euro bon",
              "productPlus": [
                2,
                5
              ]
            }
          ],
          "validUntil": 1511881142481,
          "validateUntil": 1511881142481
        }
      ],
      "giftArticles": [
        {
          "giftArticleId": 13,
          "name": "2.5 euro bon",
          "productPlus": [
            2,
            5
          ],
          "quantity": 2
        }
      ]
    }

Checkout ticket
    
    curl -X POST - header 'Accept: application/json' 'http://localhost:8090/api/register/ ticket/settle/<TicketId>'

After a ticket has been settled, this must be passed on to the service with the above-mentioned AJAX call. Only the ticket number must be passed on. After this, the service points and promotional items will be calculated on the basis of points and linked to the customer. only ticketId and status are returned.
With the retrieval of the ticket status the processed ticket can again be obtained at status S60
Validation reward

    curl -X POST - header 'Accept: application/json' 'http://localhost:8090/api/register/ticket/settle/giftArticle/<clientCardNr>/<giftArticleId'

If the requested reward is not present at the specified customer, a status 406 (Not acceptable) is returned. With code 200 (Ok) the reward has been successfully removed from the customer and can be delivered by the cashier. the response body displays the selected reward.

    {
      "giftArticleId": 13,
      "name": "2.5 euro bon",
      "productPlus": [
        2,
        5
      ]
    }

#### Communication from site

##### Ptomotion Json

type:
* 0: Value Promotion

valuePromoType:
* 0: Gift Article
* 1: Direct Discount

## source
* [Portal](https://bitbucket.org/qnhretail/promoqportal)
* [Service](https://bitbucket.org/qnhretail/promoqservice)

## Visual Studio Team Services
* [TSVS](https://qnhomniqstore.visualstudio.com/QNH%20Promotion%20Service)
